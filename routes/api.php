<?php

use Illuminate\Support\Facades\Route;


Route::middleware(['apiVersion'])->group(function () {
    Route::prefix('v1')->name('api.v1.')->namespace('Api\V1')->group(function () {
        Route::post('login', 'LoginController@login');
        Route::post('verifyOtp', 'LoginController@verifyOtp')->name('verifyOtp');
        Route::post('contactUs', 'ContactUsController@getRecentViewItems')->name('getRecentViewItems');
        Route::get('page/{id}', 'HomeController@page');
        Route::get('notification', 'NotificationController@index');
        Route::get('galleries', 'GalleryController@index')->name('galleries');
        Route::get('diksarthi', 'DiksarthiController@index')->name('diksarthi');
        Route::get('circulars', 'HomeController@circulars')->name('circulars');
        Route::get('yojna', 'HomeController@yojna')->name('yojna');
        Route::get('tapsaviSanman', 'HomeController@tapsaviSanman')->name('tapsaviSanman');
        Route::get('parshwanath', 'HomeController@parshwanath')->name('parshwanath');
        Route::get('villageDerasar', 'HomeController@villageDerasar')->name('villageDerasar');
        Route::get('ourAncestor', 'HomeController@ourAncestor')->name('ourAncestor');
        Route::get('ourGyanti', 'HomeController@ourGyanti')->name('ourGyanti');
        Route::get('banners', 'HomeController@banners')->name('banners');
        Route::get('karyakar', 'HomeController@karyakar')->name('karyakar');
        Route::get('ourCommitte', 'HomeController@ourCommitte')->name('ourCommitte');
        Route::get('village', 'HomeController@village')->name('village');
        Route::get('president', 'HomeController@president')->name('president');
        Route::post('forgotPassword', 'LoginController@forgotPassword')->name('forgotPassword');
        Route::post('verifyResetPasswordOtp', 'LoginController@verifyResetPasswordOtp')->name('verifyResetPasswordOtp');
        Route::post('resetPassword', 'LoginController@resetPassword')->name('resetPassword');

        Route::middleware(['auth:sanctum'])->group(function () {
            Route::get('getProfile', 'UserController@index')->name('getProfile');
            Route::post('updateProfile', 'UserController@updateProfile')->name('updateProfile');
            Route::post('changePassword', 'UserController@changePassword')->name('changePassword');
            Route::post('addFeeback', 'HomeController@addFeeback')->name('addFeeback');
            Route::post('memberList', 'UserController@memberList')->name('memberList');
        });
    });
});
