<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function(){
    return redirect('admin/login');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'HomeController@search')->name('search');
Route::get('/privacy-policy', 'HomeController@page')->name('privacy-policy');
Route::get('reset-password/{token}', 'ResetPasswordController@resetPasswordForm')->name('reset-password');
Route::post('resetPasswordSubmit', 'ResetPasswordController@resetPasswordSubmit')->name('resetPasswordSubmit');
