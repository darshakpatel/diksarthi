<?php


use Illuminate\Support\Facades\Route;


Route::get('/', 'LoginController@index')->name('login');
Route::get('login', 'LoginController@index')->name('login');

Route::post('login', 'LoginController@loginCheck')->name('loginCheck');

Route::group(['middleware' => ['auth:admin', 'adminLanguage']], function () {

    Route::get('dashboard', 'HomeController@index')->name('dashboard');


    Route::resource('user', 'UserController');
    Route::get('userDetails/{id}', 'UserController@userDetails')->name('userDetails');

    Route::get('user/status/{id}/{status}', 'UserController@changeStatus')->name('user.status.change');


    Route::get('profile', 'AdminController@profile')->name('profile');
    Route::post('updateProfile', 'AdminController@updateProfile')->name('updateProfile');


    Route::post('editProfile', 'AdminController@editProfile')->name('editProfile');
    Route::post('logout', 'LoginController@logout')->name('logout');
    Route::get('password', 'AdminController@password')->name('password');
    Route::post('updatePassword', 'AdminController@updatePassword')->name('updatePassword');

    Route::resource('village', 'VillageController');
    Route::resource('relation', 'RelationController');

    Route::resource('notification', 'NotificationController');
    Route::resource('diksarthi', 'DiksarthiController');
    Route::get('setting', 'SettingController@index')->name('setting');
    Route::post('updateSetting', 'SettingController@updateSetting');
    Route::get('import', 'UserController@import');
    Route::post('fileImport', 'UserController@fileImport');
    Route::get('familyMember/{id}', 'UserController@familyMember')->name('familyMember');
    Route::get('familyMemberList', 'UserController@familyMemberList')->name('familyMemberList');
    Route::get('familyUser/{id}', 'UserController@familyUser')->name('familyUser');
    Route::resource('circular', 'CircularController');
    Route::resource('feedback', 'FeedbackController');
    Route::resource('yojna', 'YojnaController');
    Route::resource('gallery', 'GalleryController');
    Route::post('imageUpload', 'GalleryController@imageUpload')->name('imageUpload');
    Route::post('addVideoLink', 'GalleryController@addVideoLink')->name('addVideoLink');
    Route::delete('imageDelete/{id}', 'GalleryController@imageDelete')->name('imageDelete');
    Route::post('getGalleryImage', 'GalleryController@getGalleryImage')->name('getGalleryImage');
    Route::get('deleteGalleryImage/{id}', 'GalleryController@deleteGalleryImage')->name('deleteGalleryImage');

    Route::resource('tapasviSamman', 'TapasviSammanController');
    Route::resource('parshwanath', 'ParshwanathController');
    Route::resource('villageDerasar', 'VillageDerasarController');
    Route::resource('ourAncestor', 'OurAncestorController');
    Route::resource('ourGyanti', 'OurGyantiController');
    Route::resource('banner', 'BannerController');
    Route::resource('template', 'TemplateController');
    Route::resource('president', 'PresidentController');
    Route::resource('ourCommitte', 'OurCommitteController');
    Route::resource('karyakar', 'KaryakarController');
    Route::resource('page', 'PageController');

    Route::get('sendEmail', 'CommunicationController@sendEmail')->name('sendEmail');
    Route::get('sendSms', 'CommunicationController@sendSms')->name('sendSms');
    Route::get('sendNotification', 'CommunicationController@sendNotification')->name('sendNotification');
    Route::post('sendEmailProcess', 'CommunicationController@sendEmailProcess')->name('sendEmailProcess');
    Route::post('sendNotificationProcess', 'CommunicationController@sendNotificationProcess')->name('sendNotificationProcess');
    Route::get('ownershipTransfer/{id}', 'UserController@ownershipTransfer')->name('ownershipTransfer');
    Route::post('ownershipTransfer', 'UserController@ownershipTransferSubmit')->name('ownershipTransferSubmit');
    Route::get('todayBirthday', 'BirthdayController@todayBirthday')->name('todayBirthday');
    Route::get('todayBirthdaySend', 'BirthdayController@todayBirthdaySend')->name('todayBirthdaySend');
    Route::get('todayAnniversary', 'BirthdayController@todayAnniversary')->name('todayAnniversary');
    Route::get('todayAnniversarySend', 'BirthdayController@todayAnniversarySend')->name('todayAnniversarySend');
    Route::get('exportUser', 'UserController@export')->name('exportUser');
});
