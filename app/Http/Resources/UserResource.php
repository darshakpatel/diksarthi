<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'main_member_id' => $this->main_member_id,
            'member_type' => $this->member_type,
            'member_code' => $this->member_code,
            'name' => $this->name,
            'mobile_no' => $this->mobile_no,
            'email' => $this->email,
            'village_id' => $this->village_id,
            'village' => $this->village->name,
            'p_no' => $this->p_no,
            'family_no' => $this->family_no,
            'relation_id' => $this->relation_id,
            'relation' => $this->relation?$this->relation->name:"",
            'petron' => $this->petron,
            'piyar_paksh' => $this->piyar_paksh,
            'anniversary_date' => $this->anniversary_date,
            'date_of_birth' => $this->date_of_birth,
            'education' => $this->education,
            'marital_status' => $this->marital_status,
            'blood_group' => $this->blood_group,
            'residency_no' => $this->residency_no,
            'address_1' => $this->address_1,
            'address_2' => $this->address_2,
            'address_3' => $this->address_3,
            'city' => $this->city,
            'pincode' => $this->pincode,
            'special' => $this->special,
            'office_no' => $this->office_no,
            'business' => $this->business,
            'image' => $this->image,
        ];
    }
}
