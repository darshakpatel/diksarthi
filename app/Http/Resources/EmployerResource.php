<?php

namespace App\Http\Resources;

use App\Models\Subscription;
use Illuminate\Http\Resources\Json\JsonResource;

class EmployerResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'social_insurance_number' => $this->social_insurance_number,
            'date_of_birth' => $this->date_of_birth,
            'date_of_hire' => $this->date_of_hire,
            'employee_name' => $this->employee_name,
            'hire_date' => $this->hire_date,
            'start_date' => $this->start_date,
            'employee_contact_form' => $this->employee_contact_form,
            'employee_course_name' => $this->employee_course_name,
            'employee_course_date' => $this->employee_course_date,
            'employee_certificate_of_course' => $this->employee_certificate_of_course,
            'diciplinary_action_note' => $this->diciplinary_action_note,
            'diciplinary_form' => $this->diciplinary_form,
            'asset_on_hand' => $this->asset_on_hand,
        ];
    }
}
