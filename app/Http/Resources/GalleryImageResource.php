<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GalleryImageResource extends JsonResource
{

    public function toArray( $request )
    {
        return [
            'id'    => $this->id,
            'type'  => $this->type,
            'image' => $this->image,
        ];
    }
}
