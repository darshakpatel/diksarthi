<?php

namespace App\Http\Resources;

use App\Models\Subscription;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\GalleryImageResource;


class GalleryResource extends JsonResource
{

    public function toArray( $request )
    {
        return [
            'id'          => $this->id,
            'village'     => $this->village ? $this->village->name : "",
            'village_id'  => $this->village_id,
            'title'       => $this->title,
            'description' => $this->description,
            'images'      => GalleryImageResource::collection($this->galleryImage),
        ];
    }
}
