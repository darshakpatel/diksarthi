<?php

namespace App\Http\Resources;

use App\Models\Subscription;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\GalleryImageResource;


class DiksarthiResource extends JsonResource
{

    public function toArray( $request )
    {
        return [
            'id'          => $this->id,
            'village'     => $this->village_name,
            'village_id'  => $this->village_id,
            'title'       => $this->title,
            'description' => $this->description,
            'image'       => $this->image,
        ];
    }
}
