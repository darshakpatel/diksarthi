<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Template;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function search()
    {
        $templates = Template::where('status', 'Active')->get();
        $data = [];
        foreach ($templates as $key => $template) {
            $data[] = $template->title;
        }
        \File::put(public_path('/search.json'), json_encode(['templates' => $data]));
        return redirect()->to(url('/search.json'));
    }

    public function page()
    {
        $page = Page::find(4);
        return view('privacy-policy', ['page' => $page]);
    }
}
