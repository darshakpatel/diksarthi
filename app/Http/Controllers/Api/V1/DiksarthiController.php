<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\DiksarthiResource;
use App\Models\Diksarthi;
use DB;
use Illuminate\Http\Request;

class DiksarthiController extends Controller
{

    public function index()
    {

        $villageId = DB::table('diksarthi')->where('diksarthi.id', '!=', 1)
            ->pluck('village_id')->toArray();

        $villages = DB::table('villages')->whereIn('id', $villageId)->orderBy('name', 'asc')->get();

        $array = [];
        foreach ($villages as $village) {
            $diksarthi = DB::table('diksarthi')
                ->where('village_id', $village->id)
                ->leftjoin('villages', 'villages.id', 'diksarthi.village_id')
                ->select('diksarthi.*', 'villages.name as village_name')
                ->orderBy('diksarthi.title', 'asc')
                ->get();
            $diksarthi = DiksarthiResource::collection($diksarthi);
            $array[] = [
                'village'   => $village->name,
                'diksarthi' => $diksarthi
            ];
        }
        return response()->json(['data' => $array]);
    }
}