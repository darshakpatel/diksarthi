<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationResource;
use App\Models\Notification;

use Illuminate\Http\Request;


class NotificationController extends Controller
{

    public function index(Request $request): \Illuminate\Http\JsonResponse
    {
        $user = $request->user();
        $query = Notification::get();
        if (count($query) > 0) {
            $notifications = NotificationResource::collection($query);
            return response()->json([
                'success' => true,
                'data' => $notifications,
            ]);
        } else {
            return response()->json([
                'success' => false,
                'message' => trans('messages.api.notification.notification_not_found'),
            ]);
        }
    }
}
