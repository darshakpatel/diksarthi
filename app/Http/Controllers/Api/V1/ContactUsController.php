<?php

namespace App\Http\Controllers\Api\V1;


use App\Http\Requests\API\V1\ContactUsStoreRequest;
use App\Models\ContactUs;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller
{
    public function store(ContactUsStoreRequest $request): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();

        $contact_us = new ContactUs();
        $contact_us->name = $validated['name'];
        $contact_us->country_code = $validated['country_code'];
        $contact_us->mobile_no = $validated['mobile_no'];
        $contact_us->title = $validated['title'];
        $contact_us->message = $validated['message'];
        $contact_us->email = $validated['email'];
        $contact_us->save();

        return response()->json([
            'message' => trans('messages.api.contactUs.thank_you_for_contact_us'),
        ]);
    }

}
