<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\LoginWithEmailRequest;
use App\Http\Requests\API\V1\MobileVerifyRequest;
use App\Models\User;
use App\Models\UserDevice;
use DB;
use App\Helpers\SmsHelper;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\OtpMail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{

    public function login( Request $request )
    {
        if (!empty($request['mobile_no'])) {
            $user = User::where('mobile_no', $request->input('mobile_no'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Mobile no or Password', 'success' => false]);
            }

            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Invalid Mobile no or Password', 'success' => false]);
            }
            if ($user) {
                if ($user->is_mobile_verified == 1) {
                    $tokenResult = $user->createToken('authToken')->plainTextToken;
                    return response()->json([
                        'message'            => 'Login Successfully !',
                        'is_mobile_verified' => 1,
                        'token'              => $tokenResult
                    ]);
                } else {
                    $otp = 123456;
                    if (config('sms_notification') == 1) {
                        $otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
                    }
                    $user->otp = $otp;
                    $user->save();
                    return response()->json([
                        'message'            => 'OTP Sent Successfully !',
                        'is_mobile_verified' => 0
                    ]);
                }

            }
        } else {
            $user = User::where('email', $request->input('email'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Email or Password', 'success' => false]);
            }

            if (!Hash::check($request->input('password'), $user->password)) {
                return response()->json(['message' => 'Invalid Email or Password', 'success' => false]);
            }
            if ($user) {
                if ($user->is_email_verified == 1) {
                    $tokenResult = $user->createToken('authToken')->plainTextToken;
                    return response()->json([
                        'message'           => 'Login Successfully !',
                        'is_email_verified' => 1,
                        'token'             => $tokenResult
                    ]);
                } else {
                    $otp = 123456;
                    if (config('email_notification') == 1) {
                        $otp = rand(111111, 999999);
                        Mail::to($request['email'])->send(new OtpMail([
                            'name' => $user->name,
                            'otp'  => $otp
                        ]));
                    }
                    $user->email_otp = $otp;
                    $user->save();
                    return response()->json([
                        'message'           => 'Email successfully sent for verification',
                        'is_email_verified' => 0,
                    ]);
                }
            }
        }
    }

    public function forgotPassword( Request $request )
    {
        if (!empty($request['mobile_no'])) {
            $user = User::where('mobile_no', $request->input('mobile_no'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Mobile no', 'success' => false]);
            }
            $otp = 123456;
            if (config('sms_notification') == 1) {
                $otp = SmsHelper::sendSms(['mobile_no' => $user->mobile_no]);
            }
            $user->otp = $otp;
            $user->save();
            return response()->json([
                'message' => 'OTP Sent Successfully !',
            ]);
        } else {
            $user = User::where('email', $request->input('email'))
                ->first();
            if (!$user) {
                return response()->json(['message' => 'Invalid Email', 'success' => false]);
            }
            $otp = 123456;

            if (config('email_notification') == 1) {
                $otp = rand(111111, 999999);
                Mail::to($request['email'])->send(new OtpMail([
                    'name' => $user->name,
                    'otp'  => $otp
                ]));
            }
            $user->email_otp = $otp;
            $user->save();

            return response()->json([
                'message' => 'Email successfully sent for verification',
            ]);
        }
    }

    public function verifyOtp( MobileVerifyRequest $request )
    {
        if ($request['is_mobile'] == 1) {
            $user = User::where('mobile_no', $request['mobile_no'])
                ->first();
        } else {
            $user = User::where('email', $request['email'])
                ->first();
        }
        if ($user) {
            if ($request['is_mobile'] == 1) {
                if ($user->otp == $request['otp']) {
                    $user->is_mobile_verified = 1;
                    $user->save();
                    $tokenResult = $user->createToken('authToken')->plainTextToken;
                    return response()->json([
                        'success' => true,
                        'token'   => $tokenResult,
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => "Invalid OTP",
                    ]);
                }
            } elseif ($user->email_otp == $request['otp']) {
                $user->is_email_verified = 1;
                $user->save();
                $tokenResult = $user->createToken('authToken')->plainTextToken;
                return response()->json([
                    'success' => true,
                    'token'   => $tokenResult,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Invalid OTP",
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => "This mobile no or email is not registered",
            ]);
        }
    }

    public function verifyResetPasswordOtp( Request $request )
    {
        if ($request['mobile_no']) {
            $user = User::where('mobile_no', $request['mobile_no'])
                ->first();
        } else {
            $user = User::where('email', $request['email'])
                ->first();
        }
        if ($user) {
            if ($request['mobile_no']) {
                if ($user->otp == $request['otp']) {
                    return response()->json([
                        'success' => true,
                        'user_id' => $user->id,
                        'message' => "Otp Successfully Verified",
                    ]);
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => "Invalid OTP",
                    ]);
                }
            } elseif ($user->email_otp == $request['otp']) {
                return response()->json([
                    'success' => true,
                    'user_id' => $user->id,
                    'message' => "Otp Successfully Verified",
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Invalid OTP",
                ]);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => "This mobile no or email is not registered",
            ]);
        }
    }

    public function resetPassword( Request $request )
    {
        $user = User::find($request['user_id']);
        if(!$user){
            return response()->json([
                'success' => false,
                'message' => "This mobile no or email is not registered",
            ]);
        }
        $user->password = Hash::make($request['password']);
        $user->save();

        return response()->json([
            'success' => true,
            'message' => "Password Updated Successfully",
        ]);
    }

    public function addDeviceToken( $user_id, $device_type, $device_token ): bool
    {
        $this->deleteToken($device_token);
        $employers = new UserDevice();
        $employers->user_id = $user_id;
        $employers->device_type = $device_type;
        $employers->device_token = $device_token;
        $employers->save();
        return true;
    }

    public function deleteToken( $device_token ): bool
    {
        DB::table('user_devices')->where('device_token', $device_token)->delete();
        return true;
    }
}

