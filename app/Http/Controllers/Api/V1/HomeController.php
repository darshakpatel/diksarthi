<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Circular;
use App\Helpers\ImageUploadHelper;
use App\Models\Feedback;
use App\Http\Resources\VillageDerasarResource;
use App\Http\Resources\OurAncestorResource;
use App\Models\Yojna;
use App\Models\TapasviSamman;
use App\Models\Village;
use App\Models\Parshwanath;
use App\Models\VillageDerasar;
use App\Models\OurAncestor;
use App\Models\OurGyanti;
use App\Models\Banner;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function circulars(): \Illuminate\Http\JsonResponse
    {
        $result = Circular::select('id', 'title', 'date', 'description', 'image', 'video_link')->get();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);

    }

    public function addFeeback( Request $request ): \Illuminate\Http\JsonResponse
    {
        $user = $request->user();
        $image = null;
        if ($request->hasFile('image')) {
            $image = ImageUploadHelper::ImageUpload($request['image']);
        }
        $feedback = new Feedback();
        $feedback->user_id = $user->id;
        $feedback->subject = $request['subject'];
        $feedback->description = $request['description'];
        $feedback->image = $image;
        $feedback->save();

        return response()->json([
            'success' => true,
            'message' => "Feedback Successfully Submitted",
        ]);

    }

    public function yojna(): \Illuminate\Http\JsonResponse
    {
        $result = Yojna::select('id', 'start_date', 'end_date', 'name', 'description', 'benefit', 'image', 'video_link')->get();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }

    public function tapsaviSanman(): \Illuminate\Http\JsonResponse
    {
        $result = TapasviSamman::orderBy('order_by', 'asc')->select('id', 'title', 'image')->orderBy('title', 'asc')->get();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }

    public function parshwanath(): \Illuminate\Http\JsonResponse
    {
        $result = Parshwanath::orderBy('order_by', 'asc')->select('id', 'title', 'description', 'image')->orderBy('title', 'asc')->get();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }

    public function ourGyanti(): \Illuminate\Http\JsonResponse
    {
        $result = OurGyanti::select('id', 'description')->first();
        return response()->json([
            'success' => true,
            'data'    => $result->description,
        ]);
    }

    public function villageDerasar( Request $request ): \Illuminate\Http\JsonResponse
    {
        if ($request['village_id'] != null) {
            $villageIds = VillageDerasar::where('village_id', $request['village_id'])->pluck('village_id')->toArray();
        } else {
            $villageIds = VillageDerasar::pluck('village_id')->toArray();
        }
        $array = [];
        $villages = Village::whereIn('id', $villageIds)->get();
        foreach ($villages as $village) {
            $VillageDerasar = VillageDerasar::where('village_id', $village->id)->get();
            $array[] = [
                'village'         => $village->name,
                'village_derasar' => VillageDerasarResource::collection($VillageDerasar),
            ];
        }
        return response()->json([
            'success' => true,
            'data'    => $array,
        ]);
    }

    public function ourAncestor(): \Illuminate\Http\JsonResponse
    {
        $result = OurAncestor::get();
        $result = OurAncestorResource::collection($result);
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }

    public function banners(): \Illuminate\Http\JsonResponse
    {
        $result = Banner::select('id', 'image')->get();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }

    public function karyakar(): \Illuminate\Http\JsonResponse
    {
        $result = DB::table('karyakar')->select('description')->first();
        return response()->json([
            'success' => true,
            'data'    => $result->description,
        ]);
    }

    public function ourCommitte(): \Illuminate\Http\JsonResponse
    {
        $result = DB::table('our_committe')->select('description')->first();
        return response()->json([
            'success' => true,
            'data'    => $result->description,
        ]);
    }

    public function president(): \Illuminate\Http\JsonResponse
    {
        $result = DB::table('presidents')->select('description')->first();
        return response()->json([
            'success' => true,
            'data'    => $result->description,
        ]);
    }

    public function village(): \Illuminate\Http\JsonResponse
    {
        $result = DB::table('villages')->select('id', 'name')->orderBy('name', 'asc')->get();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }

    public function page( $id ): \Illuminate\Http\JsonResponse
    {
        $result = DB::table('pages')->where('page_slug', $id)->select('name','description')->first();
        return response()->json([
            'success' => true,
            'data'    => $result,
        ]);
    }
}
