<?php

namespace App\Http\Controllers\Api\V1;

use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\API\V1\UserChangePasswordRequest;
use App\Http\Requests\API\V1\UserProfileChangeRequest;
use App\Http\Requests\API\V1\UserProfileUpdateRequest;
use App\Http\Resources\UserResource;
use App\Mail\ResetPasswordEmail;
use App\Models\User;
use App\Models\UserDevice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index( Request $request ): \Illuminate\Http\JsonResponse
    {
        $user = $request->user();

        return response()->json([
            'data' => new UserResource($user),
        ]);
    }

    public function memberList( Request $request )
    {
        $users = User::with('village');
        if ($request->input('family') != null) {
            //$user = User::where('id', $request->input('family'))->first();
//            if ($user && $user->member_type == 2) {
//                $users = $users->where('main_member_id', $user->main_member_id);
//            } else {
            $users = $users->where('main_member_id', $request->input('family'));
//            }

        }
        if ($request->input('member_type') != null) {
            $users = $users->where('member_type', $request['member_type']);
        }
        if ($request->input('search') != null) {
            $users = $users->where(function ( $query ) use ( $request ) {
                $query->where('name', 'LIKE', '%' . $request['search'] . '%')
                    ->orwhere('mobile_no', 'LIKE', '%' . $request['search'] . '%')
                    ->orwhere('email', 'LIKE', '%' . $request['search'] . '%');
            });
        }
        if ($request->input('village_id') != null) {
            $users = $users->where('village_id', $request->input('village_id'));
        }
        $users = $users->paginate(10);

        return response()->json([
            'data'        => UserResource::collection($users),
            'total_pages' => ceil($users->total() / 10)
        ]);
    }

    public function updateProfile( UserProfileUpdateRequest $request ): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();

        try {
            $user = User::findorFail($request->user()->id);
            $user->name = $validated['name'];
            $user->mobile_no = $validated['mobile_no'];
            $user->email = $validated['email'];
            $user->village_id = $request['village_id'];
            $user->petron = $request['petron'];
            $user->piyar_paksh = $request['piyar_paksh'];
            $user->anniversary_date = $request['anniversary_date'];
            $user->date_of_birth = $request['date_of_birth'];
            $user->education = $request['education'];
            $user->marital_status = $request['marital_status'];
            $user->blood_group = $request['blood_group'];
            $user->residency_no = $request['residency_no'];
            $user->address_1 = $request['address_1'];
            $user->address_2 = $request['address_2'];
            $user->address_3 = $request['address_3'];
            $user->city = $request['city'];
            $user->pincode = $request['pincode'];
            $user->special = $request['special'];
            $user->office_no = $request['office_no'];
            $user->business = $request['business'];

            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $user->image = $image;
            }

            $user->save();
            return response()->json([
                'success' => true,
                'message' => trans('messages.api.user.profile_updated'),
            ]);
        } catch (\Exception $error) {
            return response()->json([
                'success' => false,
                'message' => "API Error.",
                'error'   => $error->getMessage() . '.line-' . $error->getLine(),
            ]);
        }
    }

    public function changePassword( UserChangePasswordRequest $request ): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();

        try {
            $user = User::findorFail($request->user()->id);
            if (Hash::check($validated['old_password'], $user->passwod)) {
                $user->passwod = $validated['new_password'];
                $user->save();
                return response()->json([
                    'message' => trans('messages.api.user.password_updated'),
                ]);
            } else {
                return response()->json([
                    'message' => trans('messages.api.user.old_password_is_wrong'),
                ], 422);
            }
        } catch (\Exception $error) {
            return response()->json([
                'message' => trans('messages.api.error'),
                'error'   => $error->getMessage(),
            ], 500);
        }
    }

    public function forgotPassword( Request $request ): \Illuminate\Http\JsonResponse
    {
        $rules = [
            'email' => 'required|email',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
            ], 403);
        }
        $user = User::where(['email' => $request->input('email')])->first();
        if ($user) {
            $token = Password::getRepository()->create($user);
            $array = [
                'name'                   => $user->name,
                'actionUrl'              => route('reset-password', [$token]),
                'mail_title'             => trans('messages.title_password_reset'),
                'reset_password_subject' => trans('messages.reset_password_subject'),
                'main_title_text'        => trans('messages.title_password_reset'),
            ];
            Mail::to($request->input('email'))->send(new ResetPasswordEmail($array));
            return response()->json([
                'success' => true,
                'message' => trans('messages.please_check_your_mail'),
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => trans('messages.email_not_found'),
            ], 200);
        }
    }
}
