<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\OurCommitte;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OurCommitteController extends Controller
{
    public function index()
    {
        $ourCommitte = OurCommitte::first();
        return view('admin.ourCommitte.index', [
            'ourCommitte' => $ourCommitte
        ]);
    }

    public function store( Request $request )
    {
        $ourCommitte =OurCommitte::first();
        $ourCommitte->description = $request['description'];
        $ourCommitte->save();
        return response()->json(['message' => "Our Committe Updated"]);
    }
}
