<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Models\Karyakar;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class KaryakarController extends Controller
{
    public function index()
    {
        $karyakar = Karyakar::first();
        return view('admin.karyakar.index', [
            'karyakar' => $karyakar
        ]);
    }

    public function store( Request $request )
    {
        $karyakar =Karyakar::first();
        $karyakar->description = $request['description'];
        $karyakar->save();
        return response()->json(['message' => "Karyakar Updated"]);
    }
}
