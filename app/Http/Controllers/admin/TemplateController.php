<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyStoreRequest;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;

class TemplateController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $templates = Template::select('templates.*');
            return DataTables::of($templates)
                ->addColumn('action', function ($templates) {
                    return '<a href="' . route('admin.template.edit', [$templates->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . Config::get('languageString.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
        return view('admin.template.index');
    }

    public function store(Request $request)
    {
        $template = Template::find($request['edit_value']);
        $template->title = $request['title'];
        $template->subject = $request['subject'];
        $template->description = $request['description'];
        if($request['api_key']){
            $template->api_key = $request['api_key'];
        }
        $template->save();
        return response()->json(['success' => true, 'message' => "Template Updated"], 200);
    }

    public function edit(int $id)
    {
        $tags = [];
        $template = Template::findorfail($id);
        if ($template->name_key == 'otp_sms' || $template->name_key == 'otp_email') {
            $tags = ['{{otp}}'];
        }
        return view('admin.template.edit', [
            "tags"     => $tags,
            "template" => $template,
        ]);
    }
}
