<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\Banner;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class BannerController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $banners = Banner::select('banners.*');
            return DataTables::of($banners)
                ->addColumn('action', function ( $parshwanath ) {
                    $edit_button = '<a href="' . route('admin.banner.edit', [$parshwanath->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $parshwanath->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('image', function ( $parshwanath ) {
                    if ($parshwanath->image) {
                        return '<img src="' . asset($parshwanath->image) . '" style="max-width:200px" />';
                    }
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('admin.banner.index');
    }


    public function create()
    {
        return view('admin.banner.create');
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $circular = new Banner();
            $circular->image = $image;
            $circular->save();
            return response()->json(['message' => "Banner Added"]);
        } else {
            $circular = Banner::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $circular->image = $image;
            }
            $circular->save();
            return response()->json(['message' => "Banner Updated"]);
        }
    }


    public function edit( int $id )
    {
        $banner = Banner::findorfail($id);
        return view('admin.banner.edit', ["banner" => $banner]);
    }


    public function destroy( $id )
    {
        Banner::where('id', $id)->delete();
        return response()->json(['message' => 'Banner Deleted']);
    }
}
