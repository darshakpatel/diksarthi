<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\RelationStoreRequest;
use App\Helpers\ImageUploadHelper;
use App\Models\Circular;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class CircularController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $circulars = Circular::select('circulars.*');
            return DataTables::of($circulars)
                ->addColumn('action', function ( $circulars ) {
                    $edit_button = '<a href="' . route('admin.circular.edit', [$circulars->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $circulars->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.circulars.index');
    }


    public function create()
    {
        return view('admin.circulars.create');
    }


    public function store(Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $circular = new Circular();
            $circular->date = $request['date'];
            $circular->title = $request['title'];
            $circular->description = $request['description'];
            $circular->video_link = $request['video_link'];
            $circular->image = $image;
            $circular->save();
            return response()->json(['message' => "Circular Added"]);
        } else {
            $circular = Circular::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $circular->image = $image;
            }
            $circular->date = $request['date'];
            $circular->title = $request['title'];
            $circular->description = $request['description'];
            $circular->video_link = $request['video_link'];
            $circular->save();
            return response()->json(['message' => "Circular Updated"]);
        }
    }


    public function edit( int $id )
    {
        $circular = Circular::findorfail($id);
        return view('admin.circulars.edit', ["circular" => $circular]);
    }


    public function destroy( $id )
    {
        Circular::where('id', $id)->delete();
        return response()->json(['message' => 'Circular Deleted']);
    }
}
