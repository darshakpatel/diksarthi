<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class FeedbackController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $feedbacks = Feedback::orderBy('id','desc')->select('feedback.*');
            return DataTables::of($feedbacks)
                ->addColumn('action', function ( $feedbacks ) {
                    $delete_button = '<button data-id="' . $feedbacks->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $delete_button;
                })
                ->addColumn('image', function ( $feedbacks ) {
                    if ($feedbacks->image) {
                        return '<img src="' . asset($feedbacks->image) . '" style="max-width:150px" />';
                    }
                })
                ->addColumn('user', function ( $feedbacks ) {
                    return $feedbacks->user ? $feedbacks->user->name : "";
                })
                ->addColumn('description', function ( $feedbacks ) {
                    return implode(PHP_EOL, str_split($feedbacks->description, 25));
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.feedback.index');
    }


    public function destroy( $id )
    {
        Feedback::where('id', $id)->delete();
        return response()->json(['message' => 'Feedback Deleted']);
    }
}
