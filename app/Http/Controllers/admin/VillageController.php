<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\VillageStoreRequest;
use App\Models\Village;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class VillageController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $villages = Village::select('villages.*');
            return DataTables::of($villages)
                ->addColumn('action', function ( $villages ) {
                    $edit_button = '<a href="' . route('admin.village.edit', [$villages->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $villages->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button.' '.$delete_button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.village.index');
    }


    public function create()
    {
        return view('admin.village.create');
    }


    public function store( VillageStoreRequest $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $village = new Village();
            $village->name = $request['name'];
            $village->save();
            return response()->json(['message' => "Village Added"]);
        } else {
            $village = Village::find($id);
            $village->name = $request['name'];
            $village->save();
            return response()->json(['message' => "Village Updated"]);
        }
    }


    public function edit( int $id )
    {
        $village = Village::findorfail($id);
        return view('admin.village.edit', ["village" => $village]);
    }


    public function destroy( $id )
    {
        Village::where('id', $id)->delete();
        return response()->json(['message' => 'Village Deleted']);
    }
}
