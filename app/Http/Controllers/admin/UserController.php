<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Helpers\ImageDeleteHelper;
use App\Models\Relation;
use App\Models\User;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\MemberImport;
use App\Exports\UserExport;

class UserController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {

            $users = User::with([
                'village',
                'relation',
                'familyMember' => function ( $q ) {
                    $q->with('relation');
                }
            ]);
            if ($request->input('village_id') != null) {
                $users->where('village_id', $request->input('village_id'));
            }
            if ($request->input('member_type') != null) {
                $users->where('member_type', $request->input('member_type'));
            } else {
                $users->where('member_type', 1)
                    ->where('is_transferred', 0);
            }
            $users = $users->select('users.*');

            return Datatables::of($users)
                ->addColumn('action', function ( $users ) {
                    $ownership_transfer = $family_member = $detail_button = '';
                    $delete_button = '<button data-id="' . $users->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-trash-can"></i></button>';
                    $detail_button = '<a href="' . route('admin.userDetails', ['id' => $users->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.view') . '"><i class="mdi mdi-eye"></i></a>';
                    $edit_button = '<a href="' . route('admin.user.edit', [$users->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="fa fa-edit"></i></a>';
                    if ($users->member_type == 1) {
                        $family_member = '<a href="' . route('admin.familyMember', [$users->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Family Member"><i class="fa fa-user"></i></a>';
                        $ownership_transfer = '<a href="' . route('admin.ownershipTransfer', [$users->id]) . '" class="btn btn-info waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="Ownership Transfer"><i class="fa fa-user-tie"></i></a>';
                    }
                    if ($users->status == 'active') {
                        $status_button = '<button data-id="' . $users->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                    } else {
                        $status_button = '<button data-id="' . $users->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
                    }
                    return '<div class="btn-group-list">' . $detail_button . ' ' . $family_member . ' ' . $edit_button . ' ' . $delete_button . ' ' . $status_button . ' ' . $ownership_transfer . '</div>';
                })
                ->addColumn('date', function ( $users ) {
                    $date = $users->date_of_birth;
                    return $date;
                })
                ->addColumn('mobile_no', function ( $users ) {
                    return $users->country_code . ' ' . $users->mobile_no;
                })
                ->addColumn('member_type', function ( $users ) {
                    return $users->member_type == 1 ? 'Registered Member' : 'Family Member';
                })
                ->addColumn('village', function ( $users ) {
                    return $users->village ? $users->village->name : '';
                })
                ->addColumn('relation', function ( $users ) {
                    return $users->relation ? $users->relation->name : '';
                })
                ->addColumn('status', function ( $users ) {
                    if ($users->status == 'active') {
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
                    } else {
                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
                    }
                    return $status;
                })
                ->rawColumns(['action', 'name', 'status'])
                ->make(true);
        }
        $villages = Village::all();
        return view('admin.user.index', ['villages' => $villages]);

    }

    public function create()
    {
        $villages = Village::all();
        $relations = Relation::all();
        $register_members = DB::table('users')->where('member_type', 1)->select('member_type', 'main_member_id', 'family_no', 'name', 'id')->get();

        return view('admin.user.create', [
            'register_members' => $register_members,
            'relations'        => $relations,
            'villages'         => $villages,
        ]);
    }

    public function familyUser( $id )
    {
        $members = User::where('main_member_id', $id)->get();
        foreach ($members as $member) {
            $array[] = [
                'name'          => $member->name,
                'mobile_no'     => $member->mobile_no,
                'relation'      => $member->relation->name,
                'date_of_birth' => $member->date_of_birth,
            ];
        }
        return response()->json([
            'data' => $members,
        ]);
    }

    public function store( Request $request )
    {
        if ($request['edit_value'] == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            do {
                $member_code = strtoupper($this->generateRandomString(10));
            } while (User::where('member_code', $member_code)->exists());
            $user = new User();
            $user->village_id = $request['village_id'];
            $user->member_type = $request['member_type'];
            $user->member_code = $member_code;
            $user->main_member_id = $request['main_member_id'];
            $user->p_no = $request['p_no'];
            $user->family_no = $request['family_no'];
            $user->per_no = $request['per_no'];
            $user->name = $request['name'];
            if ($request['member_type'] == 1) {
                $user->relation_id = 17;
            } else {
                $user->relation_id = $request['relation_id'];
            }
            $user->petron = $request['petron'];
            $user->piyar_paksh = $request['piyar_paksh'];
            $user->anniversary_date = $request['anniversary_date'];
            $user->date_of_birth = $request['date_of_birth'];
            $user->education = $request['education'];
            $user->marital_status = $request['marital_status'];
            $user->blood_group = $request['blood_group'];
            $user->mobile_no = $request['mobile_no'];
            $user->residency_no = $request['residency_no'];
            $user->office_no = $request['office_no'];
            $user->business = $request['business'];
            $user->address_1 = $request['address_1'];
            $user->address_2 = $request['address_2'];
            $user->address_3 = $request['address_3'];
            $user->city = $request['city'];
            $user->pincode = $request['pincode'];
            $user->email = $request['email'];
            $user->special = $request['special'];
            $user->image = $image;
            if (!empty($request['password'])) {
                $user->password = \Hash::make($request['password']);
            }
            $user->save();

            return response()->json(['message' => 'User added']);
        } else {

            $user = User::find($request['edit_value']);
            $user->village_id = $request['village_id'];
            $user->p_no = $request['p_no'];
            $user->family_no = $request['family_no'];
            $user->per_no = $request['per_no'];
            $user->name = $request['name'];
            if ($request['member_type'] == 1) {
                $user->relation_id = 17;
            } else {
                $user->relation_id = $request['relation_id'];
            }
            $user->petron = $request['petron'];
            $user->piyar_paksh = $request['piyar_paksh'];
            $user->anniversary_date = $request['anniversary_date'];
            $user->date_of_birth = $request['date_of_birth'];
            $user->education = $request['education'];
            $user->marital_status = $request['marital_status'];
            $user->blood_group = $request['blood_group'];
            $user->mobile_no = $request['mobile_no'];
            $user->residency_no = $request['residency_no'];
            $user->office_no = $request['office_no'];
            $user->business = $request['business'];
            $user->address_1 = $request['address_1'];
            $user->address_2 = $request['address_2'];
            $user->address_3 = $request['address_3'];
            $user->city = $request['city'];
            $user->pincode = $request['pincode'];
            $user->email = $request['email'];
            $user->special = $request['special'];
            if ($request->hasFile('image')) {
                if ($user->image) {
                    ImageDeleteHelper::deleteImage($user->image);
                }
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $user->image = $image;
            }
            if (!empty($request['password'])) {
                $user->password = \Hash::make($request['password']);
            }
            $user->save();

            return response()->json(['message' => 'User updated']);
        }
    }

    public function edit( $id )
    {
        $user = User::find($id);
        $villages = Village::all();
        $relations = Relation::all();
        $register_members = DB::table('users')->where('member_type', 1)->select('member_type', 'main_member_id', 'family_no', 'name', 'id')->get();
        return view('admin.user.edit', [
            'user'             => $user,
            'register_members' => $register_members,
            'relations'        => $relations,
            'villages'         => $villages,
        ]);
    }


    public function destroy( $id )
    {
        User::where('id', $id)->delete();
        return response()->json([
            'success' => true, 'status_code' => 200, 'message' => trans('messages.admin.user.user_deleted')
        ]);
    }

    public function changeStatus( $id, $status )
    {
        User::where('id', $id)->update(['status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    function generateRandomString( $length = 6 )
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function userDetails( $id )
    {
        $user = User::find($id);
        return view('admin.user.details', ['user' => $user]);
    }

    public function import()
    {
        return view('admin.user.import');
    }

    public function fileImport( Request $request )
    {
        DB::beginTransaction();
        $per_no = null;
        try {
            $members = Excel::toArray(new MemberImport, $request->file('excel'));
            foreach ($members[0] as $row) {
                if (!empty($row['name'])) {
                    $village = $relation_id = null;
                    if (!empty($row['village'])) {
                        $village = Village::where('name', $row['village'])->first();
                    }
                    if (!empty($row['relationship'])) {
                        $relation = Relation::where('name', $row['relationship'])->first();
                        if ($relation) {
                            $relation_id = $relation->id;
                        }
                        if ($row['relationship'] == 'SELF' || $row['relationship'] == 'Self') {
                            $member_type = 1;
                        } else {
                            $member_type = 2;
                        }
                    }
                    $anniversary = $dob = null;

                    if (!empty($row['anniversary']) && is_integer($row['anniversary'])) {
                        $anniversary = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['anniversary'])
                            ->format('Y-m-d');

                    }
                    if (!empty($row['dob']) && is_integer($row['dob'])) {
                        $dob = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['dob'])
                            ->format('Y-m-d');
                    }
                    $user = new User();
                    if ($member_type == 2) {
                        $per_no = substr($row["per_no"], 0, -2);
                        $mainMember = User::where('family_no', 'LIKE', '%' . $per_no . '%')->first();
                        $user->main_member_id = $mainMember ? $mainMember->id : null;
                        $family_no = $mainMember->family_no;
                    } else {
                        $family_no = $row['fam_no'];
                    }
                    $user->member_type = $member_type;
                    $user->date_of_birth = $dob;
                    $user->anniversary_date = $anniversary;
                    $user->village_id = $village ? $village->id : null;
                    $user->p_no = $row['p_no'];
                    $user->family_no = $family_no;
                    $user->per_no = $row['per_no'];
                    $user->name = $row['name'];
                    $user->relation_id = $relation_id;
                    $user->petron = $row['pl'];
                    $user->piyar_paksh = $row['piyar_paksh'];
                    $user->education = $row['education'];
                    $user->marital_status = $row['married'];
                    $user->blood_group = $row['blood'];
                    $user->mobile_no = $row['mob_no'];
                    $user->residency_no = $row['res_no1'];
                    $user->office_no = $row['office_no1'];
                    $user->business = $row['business'];
                    $user->address_1 = $row['adrl1'];
                    $user->address_2 = $row['adrl2'];
                    $user->address_3 = $row['adrl3'];
                    $user->city = $row['city'];
                    $user->pincode = $row['pin'];
                    $user->email = $row['e_mail'];
                    $user->special = $row['special'];
                    $user->save();
                }
            }
            DB::commit();
            return response()->json(['message' => 'User added']);
        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json(['message' => $per_no . '/' . $row["per_no"] . '   ' . $exception->getMessage() . ' ' . $exception->getLine() . ' ' . $exception->getFile()]);
        }

    }

    function isDate( $value )
    {
        if (!$value) {
            return false;
        }
        try {
            new \DateTime($value);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function familyMember( $id )
    {
        $mainMember = User::find($id);
        return view('admin.user.familyMember', [
            'main_member_id' => $id,
            'mainMember'     => $mainMember
        ]);
    }

    public function ownershipTransfer( $id )
    {
        $mainMember = User::find($id);
        $users = User::where('member_type', 2)->where('main_member_id', $id)->get();
        return view('admin.user.ownershipTransfer', [
            'mainMember' => $mainMember,
            'users'      => $users
        ]);
    }

    public function ownershipTransferSubmit( Request $request )
    {
        $mainMember = User::find($request['main_member_id']);
        $mainMember->is_transferred = 1;
        $mainMember->main_member_id = $request['transfer_id'];
        $mainMember->save();

        $transfer = User::find($request['transfer_id']);
        $transfer->member_type = 1;
        $transfer->old_member_id = $request['main_member_id'];
        $transfer->is_ownership_transfer = 1;
        $transfer->save();

        DB::table('users')->where('id', '!=', $transfer->id)->where('main_member_id', $request['main_member_id'])
            ->update([
                'main_member_id' => $transfer->id,
                'old_member_id'  => $request['main_member_id'],
                'updated_at'     => date('Y-m-d H:i:s'),
            ]);

        DB::table('ownership_transfers')
            ->insert([
                'main_member_id' => $request['main_member_id'],
                'new_member_id'  => $transfer->id,
                'created_at'     => date('Y-m-d H:i:s'),
                'updated_at'     => date('Y-m-d H:i:s'),
            ]);

        return response()->json(['message' => 'Transferred Successfully']);
    }

    public function familyMemberList( Request $request )
    {
        if ($request->ajax()) {
            $users = User::select('users.*');
            $users->where('main_member_id', $request->input('main_member_id'));
            return Datatables::of($users)
                ->addColumn('action', function ( $users ) {
                    $detail_button = '';
                    $delete_button = '<button data-id = "' . $users->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle = "tooltip" data-placement = "top" title = "' . trans('messages.edit') . '" ><i class="mdi mdi-trash-can" ></i ></button>';
                    $detail_button = '<a href = "' . route('admin.userDetails', ['id' => $users->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle = "tooltip" data-placement = "top" title = "' . trans('messages.view') . '" ><i class="mdi mdi-eye" ></i ></a>';
                    $edit_button = '<a href = "' . route('admin.user.edit', [$users->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle = "tooltip" data-placement = "top" title = "' . trans('messages.edit') . '" ><i class="fa fa-edit" ></i ></a > ';
                    if ($users->status == 'active') {
                        $status_button = ' <button data-id = "' . $users->id . '" data-status = "inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle = "tooltip" data-placement = "top" title = "' . trans('messages.inactive') . '" ><i class="fas fa-ban" ></i ></button>';
                    } else {
                        $status_button = '<button data-id = "' . $users->id . '" data-status = "active" class="status-change btn btn-primary waves-effect waves-light" data-toggle = "tooltip" data-placement = "top" title = "' . trans('messages.active') . '" ><i class="fas fa-check-square" ></i ></button>';
                    }
                    return '<div class="btn-group-list" > ' . $detail_button . ' ' . $edit_button . ' ' . $delete_button . ' ' . $status_button . ' </div > ';
                })
                ->addColumn('date', function ( $users ) {
                    $date = $users->date_of_birth;
                    return $date;
                })
                ->addColumn('mobile_no', function ( $users ) {
                    return $users->country_code . ' ' . $users->mobile_no;
                })
                ->addColumn('village', function ( $users ) {
                    return $users->village ? $users->village->name : '';
                })
                ->addColumn('relation', function ( $users ) {
                    return $users->relation ? $users->relation->name : '';
                })
                ->addColumn('status', function ( $users ) {
                    if ($users->status == 'active') {
                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . ' </span >';
                    } else {
                        $status = '<span class="badge bg-soft-danger text-danger p-1" > ' . trans('messages.inactive') . ' </span > ';
                    }
                    return $status;
                })
                ->rawColumns(['action', 'name', 'status'])
                ->make(true);
        }
    }
    public function export( Request $request )
    {
        $users = User::with('relation');
        if ($request->input('village_id') != null) {
            $users->where('village_id', $request->input('village_id'));
        }
        if ($request->input('member_type') != null) {
            $users->where('member_type', $request->input('member_type'));
        } else {
            $users->where('is_transferred', 0);
        }
        $users = $users->select('users.*')->get();
        return Excel::download(new UserExport($users), 'UserExport.xlsx');
    }
}
