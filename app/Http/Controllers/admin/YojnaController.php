<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\RelationStoreRequest;
use App\Helpers\ImageUploadHelper;
use App\Models\Yojna;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class YojnaController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $yojnas = Yojna::select('yojnas.*');
            return DataTables::of($yojnas)
                ->addColumn('action', function ( $yojnas ) {
                    $edit_button = '<a href="' . route('admin.yojna.edit', [$yojnas->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $yojnas->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.yojna.index');
    }


    public function create()
    {
        return view('admin.yojna.create');
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $yojna = new Yojna();
            $yojna->start_date = $request['start_date'];
            $yojna->end_date = $request['end_date'];
            $yojna->name = $request['name'];
            $yojna->benefit = $request['benefit'];
            $yojna->description = $request['description'];
            $yojna->video_link = $request['video_link'];
            $yojna->image = $image;
            $yojna->save();
            return response()->json(['message' => "Yojna Added"]);
        } else {
            $yojna = Yojna::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $yojna->image = $image;
            }
            $yojna->start_date = $request['start_date'];
            $yojna->end_date = $request['end_date'];
            $yojna->name = $request['name'];
            $yojna->benefit = $request['benefit'];
            $yojna->description = $request['description'];
            $yojna->video_link = $request['video_link'];
            $yojna->save();
            return response()->json(['message' => "Yojna Updated"]);
        }
    }


    public function edit( int $id )
    {
        $yojna = Yojna::findorfail($id);
        return view('admin.yojna.edit', ["yojna" => $yojna]);
    }


    public function destroy( $id )
    {
        Yojna::where('id', $id)->delete();
        return response()->json(['message' => 'Yojna Deleted']);
    }
}
