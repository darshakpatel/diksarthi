<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Cache;

class SettingController extends Controller
{
    public function index()
    {
        $settings = Setting::all();
        return view('admin.setting.index',
            [
                "settings" => $settings,
            ]);
    }

    public function updateSetting(Request $request): \Illuminate\Http\JsonResponse
    {
        $settings = Setting::all();
        foreach($settings as $setting){
            Setting::where('meta_key', $setting->meta_key)->update([
                'meta_value' => $request->input($setting->meta_key),
            ]);
        }
//        Cache::forget('currency');
        return response()->json(['success' => true, 'message' => 'Setting Successfully updated']);
    }

//    function set_env(string $key, string $value, $env_path = null)
//    {
//        $value = preg_replace('/\s+/', '', $value); //replace special ch
//        $key = strtoupper($key); //force upper for security
//        $env = file_get_contents(isset($env_path) ? $env_path : base_path('.env')); //fet .env file
//        $env = str_replace("$key=" . env($key), "$key=" . $value, $env); //replace value
//        /** Save file eith new content */
//        $env = file_put_contents(isset($env_path) ? $env_path : base_path('.env'), $env);
//        //Artisan::call('config:clear');
//    }


}
