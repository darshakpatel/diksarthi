<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageDeleteHelper;
use App\Helpers\ImageUploadHelper;
use App\Models\Village;
use App\Models\Diksarthi;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class DiksarthiController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $diksarthi = Diksarthi::where('id', '!=', 1)->select('diksarthi.*');
            return DataTables::of($diksarthi)
                ->addColumn('action', function ( $galleries ) {
                    $edit_button = '';
                    $edit_button = '<a href="' . route('admin.diksarthi.edit', [$galleries->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $galleries->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('date', function ( $diksarthi ) {
                    return $diksarthi->created_at->format('d-m-Y');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.diksarthi.index');
    }

    public function create()
    {
        $villages = Village::all();
        return view('admin.diksarthi.create', [
            'villages' => $villages
        ]);
    }

    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $diksarthi = new Diksarthi();
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $diksarthi->image = $image;
            }
            $diksarthi->village_id = $request['village_id'];
            $diksarthi->title = $request['title'];
            $diksarthi->description = $request['description'];
            $diksarthi->save();
        } else {
            $diksarthi = Diksarthi::find($id);
            if ($request->hasFile('image')) {
                ImageDeleteHelper::deleteImage($diksarthi->image);
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $diksarthi->image = $image;
            }
            $diksarthi->village_id = $request['village_id'];
            $diksarthi->title = $request['title'];
            $diksarthi->description = $request['description'];
            $diksarthi->save();
        }
        return response()->json(['message' => "Diksarthi Updated"]);
    }

    public function edit( int $id )
    {
        $diksarthi = Diksarthi::findorfail($id);
        $villages = Village::all();
        return view('admin.diksarthi.edit', [
            'villages'  => $villages,
            "diksarthi" => $diksarthi
        ]);
    }


    public function destroy( $id )
    {
        $Diksarthi = Diksarthi::where('id', $id)->first();
        if ($Diksarthi->image) {
            ImageDeleteHelper::deleteImage($Diksarthi->image);
        }
        Diksarthi::where('id', $id)->delete();
        return response()->json(['message' => 'Diksarthi Deleted']);
    }
}
