<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\TemplateResource;
use App\Http\Requests\NotificationStoreRequest;
use App\Models\UserDevice;
use App\Models\Template;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $templates = Template::where('status', 'Active')->get();
        return view('admin.notification.create', [
            'templates' => $templates
        ]);
    }

    public function store( NotificationStoreRequest $request ): \Illuminate\Http\JsonResponse
    {
        $validated = $request->validated();
        $array['title'] = $request['title'];
        $array['message'] = $request['message'];
        $template = Template::find($request['template_id']);
        $array['template'] = new TemplateResource($template);
        $device = new UserDevice();
        $device->sendNotification($array);

        return response()->json(['message' => 'Notification Sent Successfully'], 200);
    }
}
