<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\OurAncestor;
use App\Models\President;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Village;

class PresidentController extends Controller
{
    public function index()
    {
        $president = President::first();
        return view('admin.president.index', [
            'president' => $president
        ]);
    }

    public function store( Request $request )
    {
        $president =President::first();
        $president->description = $request['description'];
        $president->save();
        return response()->json(['message' => "President Updated"]);
    }
}
