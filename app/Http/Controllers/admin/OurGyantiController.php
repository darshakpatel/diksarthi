<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\OurGyanti;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Village;

class OurGyantiController extends Controller
{

    public function index()
    {
        $ourGyanti = OurGyanti::first();
        return view('admin.ourGyanti.index', ['ourGyanti' => $ourGyanti]);
    }

    public function store( Request $request )
    {
        $circular = OurGyanti::first();
        $circular->description = $request['description'];
        $circular->save();
        return response()->json(['message' => "Our Ghnyati Updated"]);
    }

}
