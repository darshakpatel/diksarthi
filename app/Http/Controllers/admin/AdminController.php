<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Relation;
use App\Models\User;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\MemberImport;

class AdminController extends Controller
{

    public function profile( Request $request )
    {
        $profile = Admin::where('id', auth()->user()->id)->first();
        return view('admin.profile.profile', ['profile' => $profile]);
    }

    public function updateProfile( Request $request )
    {
        $image = null;
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'nullable|email',
            'mobile_no' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors()->first(),
            ]);
        }
        $auth_user = Auth::user();
        $input['email'] = $request->input('email');
        if ($input['email'] != NULL) {
            $duplicate_email_check = Admin::where('email', $input['email'])
                ->where('id', '!=', $auth_user->id)
                ->count();
            if ($duplicate_email_check > 0) {
                return response()->json([
                    'success' => false,
                    'message' => trans('messages.admin.user.email_is_exists'),
                ]);
            }
        }


        Admin::where('id', $auth_user->id)
            ->update([
                'name'      => $request->input('name'),
                'email'     => $request->input('email'),
                'mobile_no' => $request->input('mobile_no'),
            ]);
        return response()->json([
            'success' => true,
            'message' => trans('messages.admin.user.profile_updated'),
        ]);

    }


    public function password()
    {
        return view('admin.profile.password');
    }


    public function updatePassword( Request $request )
    {
        $validator_array = [
            'password'         => 'min:6|required|same:confirm_password',
            'confirm_password' => 'min:6',
        ];
        $validator = Validator::make($request->all(), $validator_array);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(), 'data' => $validator->errors()
            ], 422);
        }

        Admin::where('id', auth()->user()->id)
            ->update([
                'password' => Hash::make($request->input('password')),
            ]);
        return response()->json(['success' => true, 'message' => "Password Updated Successfully"]);

    }
}