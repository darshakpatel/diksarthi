<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\VillageDerasar;
use App\Models\Village;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class VillageDerasarController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $village_derasar = VillageDerasar::orderBy('order_by', 'asc')->select('village_derasar.*');
            return DataTables::of($village_derasar)
                ->addColumn('action', function ( $village_derasar ) {
                    $edit_button = '<a href="' . route('admin.villageDerasar.edit', [$village_derasar->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $village_derasar->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('image', function ( $parshwanath ) {
                    if ($parshwanath->image) {
                        return '<img src="' . asset($parshwanath->image) . '" style="max-width:200px" />';
                    }
                })
                ->addColumn('village', function ( $parshwanath ) {
                    if ($parshwanath->village) {
                        return $parshwanath->village->name;
                    }
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.villageDerasar.index');
    }


    public function create()
    {
        $villages = Village::all();
        return view('admin.villageDerasar.create', [
            'villages' => $villages
        ]);
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $circular = new VillageDerasar();
            $circular->village_id = $request['village_id'];
            $circular->title = $request['title'];
            $circular->order_by = $request['order_by'];
            $circular->description = $request['description'];
            $circular->image = $image;
            $circular->save();
            return response()->json(['message' => "Village Derasar Added"]);
        } else {
            $circular = VillageDerasar::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $circular->image = $image;
            }
            $circular->village_id = $request['village_id'];
            $circular->title = $request['title'];
            $circular->order_by = $request['order_by'];
            $circular->description = $request['description'];
            $circular->save();
            return response()->json(['message' => "Village Derasar Updated"]);
        }
    }


    public function edit( int $id )
    {
        $villageDerasar = VillageDerasar::findorfail($id);
        $villages = Village::all();
        return view('admin.villageDerasar.edit', [
            "villageDerasar" => $villageDerasar,
            'villages'       => $villages
        ]);
    }


    public function destroy( $id )
    {
        VillageDerasar::where('id', $id)->delete();
        return response()->json(['message' => 'Village Derasar Deleted']);
    }
}
