<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\OurAncestor;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Village;

class OurAncestorController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $our_ancestors = OurAncestor::select('our_ancestors.*');
            return DataTables::of($our_ancestors)
                ->addColumn('action', function ( $our_ancestors ) {
                    $edit_button = '<a href="' . route('admin.ourAncestor.edit', [$our_ancestors->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $our_ancestors->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('image', function ( $parshwanath ) {
                    if ($parshwanath->image) {
                        return '<img src="' . asset($parshwanath->image) . '" style="max-width:200px" />';
                    }
                })
                ->addColumn('village', function ( $parshwanath ) {
                    if ($parshwanath->village) {
                        return $parshwanath->village->name;
                    }
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        return view('admin.ourAncestor.index');
    }


    public function create()
    {
        $villages = Village::all();
        return view('admin.ourAncestor.create', [
            'villages' => $villages
        ]);
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $circular = new OurAncestor();
            $circular->village_id = $request['village_id'];
            $circular->title = $request['title'];
            $circular->image = $image;
            $circular->save();
            return response()->json(['message' => "Our Ancestor Added"]);
        } else {
            $circular = OurAncestor::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $circular->image = $image;
            }
            $circular->village_id = $request['village_id'];
            $circular->title = $request['title'];
            $circular->save();
            return response()->json(['message' => "Our Ancestor Updated"]);
        }
    }


    public function edit( int $id )
    {
        $ourAncestor = OurAncestor::findorfail($id);
        $villages = Village::all();
        return view('admin.ourAncestor.edit', [
            "ourAncestor" => $ourAncestor,
            "villages"    => $villages
        ]);
    }


    public function destroy( $id )
    {
        OurAncestor::where('id', $id)->delete();
        return response()->json(['message' => 'Our Ancestor Deleted']);
    }
}
