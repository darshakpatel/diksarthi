<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\RelationStoreRequest;
use App\Models\Relation;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class RelationController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $relations = Relation::select('relations.*');
            return DataTables::of($relations)
                ->addColumn('action', function ( $relations ) {
                    $edit_button = '<a href="' . route('admin.relation.edit', [$relations->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $relations->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button.' '.$delete_button;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.relation.index');
    }


    public function create()
    {
        return view('admin.relation.create');
    }


    public function store( RelationStoreRequest $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $village = new Relation();
            $village->name = $request['name'];
            $village->save();
            return response()->json(['message' => "Relation Added"]);
        } else {
            $village = Relation::find($id);
            $village->name = $request['name'];
            $village->save();
            return response()->json(['message' => "Relation Updated"]);
        }
    }


    public function edit( int $id )
    {
        $relation = Relation::findorfail($id);
        return view('admin.relation.edit', ["relation" => $relation]);
    }


    public function destroy( $id )
    {
        Relation::where('id', $id)->delete();
        return response()->json(['message' => 'Relation Deleted']);
    }
}
