<?php

namespace App\Http\Controllers\admin;

use App\Helpers\ImageDeleteHelper;
use App\Models\Order;
use App\Models\OrderCustomField;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Controller;
use App\Models\Employer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class OrderController extends Controller
{

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $orders = Order::select('orders.*');

            return DataTables::of($orders)
                ->addColumn('action', function ($orders) {
                    $status_button = '';
                    $delete_button = '<button data-id="' . $orders->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="mdi mdi-trash-can"></i></button>';
                    if ($orders->status == 'active') {
                        $status_button = '<button data-id="' . $orders->id . '" data-status="inActive" class="status-change btn btn-warning waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.inactive') . '"><i class="fas fa-ban"></i></button>';
                    }
//                    else {
//                        $status_button = '<button data-id="' . $orders->id . '" data-status="active" class="status-change btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.active') . '"><i class="fas fa-check-square"></i></button>';
//                    }
                    return $delete_button . ' ' . $status_button;
                })
//                ->addColumn('status', function ($orders) {
//                    if ($orders->status == 'active') {
//                        $status = '<span class="badge bg-soft-success text-success p-1">' . trans('messages.active') . '</span>';
//                    } else {
//                        $status = '<span class="badge bg-soft-danger text-danger p-1">' . trans('messages.inactive') . '</span>';
//                    }
//                    return $status;
//                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.order.index');
    }

    public function create()
    {
        return view('admin.order.create');
    }

    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        if ($request['edit_value'] == 0) {
//            do {
            $code = rand(1000000, 9999999);
//            } while (
//                DB::table('orders')->where('order_id', $code)->exists()
//            );

            $orders = new Order();
            $orders->service_title = $request['service_title'];
            $orders->service_schedule = $request->service_schedule;
            $orders->manage_of_work_order = $request['manage_of_work_order'];
            $orders->assigned_provider = $request['assigned_provider'];
            $orders->service_location = $request['service_location'];
            $orders->order_id = $code;
            $orders->traning_link = $request['traning_link'];
            $orders->help_desk_contact = $request['help_desk_contact'];
            $orders->manager_on_duty = $request['manager_on_duty'];
            $orders->orientation_keyword = $request['orientation_keyword'];
            $orders->save();

            foreach ($request['title'] as $key => $title) {
               $custom_field =new OrderCustomField();
                $custom_field->order_id = $orders->id;
                $custom_field->meta_key = $title;
                $custom_field->meta_value = $request['value'][$key];
                $custom_field->save();
            }

            return response()->json(['message' => "Order Added"]);
        } else {
            $orders = Order::findorfail($request['edit_value']);
            $orders->service_title = $request['service_title'];
            $orders->service_schedule = $request->service_schedule;
            $orders->manage_of_work_order = $request['manage_of_work_order'];
            $orders->assigned_provider = $request['assigned_provider'];
            $orders->service_location = $request['service_location'];
            $orders->traning_link = $request['traning_link'];
            $orders->help_desk_contact = $request['help_desk_contact'];
            $orders->manager_on_duty = $request['manager_on_duty'];
            $orders->orientation_keyword = $request['orientation_keyword'];
            $orders->save();
            foreach ($request['title'] as $key => $title) {
                $custom_field =new OrderCustomField();
                $custom_field->order_id = $orders->id;
                $custom_field->meta_key = $title;
                $custom_field->meta_value = $request['value'][$key];
                $custom_field->save();
            }
            return response()
                ->json([
                    'message' => "Order Updated",
                ]);

        }
    }

    public function edit($id)
    {
        $order = Order::findorfail($id);
        return view('admin.order.edit', ["order" => $order]);
    }

    public function changeStatus($id, $status)
    {
        Order::where('id', $id)->update(['status' => $status]);
        return response()->json(['success' => true, 'message' => trans('messages.status_changed')]);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        Order::where('id', $id)->delete();
        return response()
            ->json([
                'message' => "Order deleted successfully"
            ]);
    }

    public function addCustomField(Request $request): \Illuminate\Http\JsonResponse
    {
        $row = $request['rowNo'];
        $view = view('admin.order.addCustomField', [
            'row' => $row,
        ])->render();
        return response()->json(['data' => $view]);
    }
}
