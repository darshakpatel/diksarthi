<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\GalleryStoreRequest;
use App\Helpers\ImageUploadHelper;
use App\Helpers\ImageDeleteHelper;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\Village;
use App\Models\TempImage;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class GalleryController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $galleries = Gallery::select('galleries.*');
            return DataTables::of($galleries)
                ->addColumn('action', function ( $galleries ) {
                    $edit_button = '';
                    $edit_button = '<a href="' . route('admin.gallery.edit', [$galleries->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $galleries->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('date', function ( $galleries ) {
                    return $galleries->created_at->format('d-m-Y');
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.gallery.index');
    }


    public function create()
    {
        $villages = Village::all();
        return view('admin.gallery.create', [
            'villages' => $villages
        ]);
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $gallery = new Gallery();
            $gallery->village_id = $request['village_id'];
            $gallery->title = $request['title'];
            $gallery->description = $request['description'];
            $gallery->save();

            foreach ($request['video_link'] as $file) {
                $galleryImage = new GalleryImage();
                $galleryImage->gallery_id = $gallery->id;
                $galleryImage->type = 'video';
                $galleryImage->image = $file;
                $galleryImage->save();
            }
            $images = TempImage::where('temp_time', $request['temp_time'])->get();

            ImageUploadHelper::UploadMultipleImage($images, $gallery->id);
            return response()->json(['message' => "Gallery Added"]);
        } else {

            $gallery = Gallery::find($id);
            $gallery->village_id = $request['village_id'];
            $gallery->title = $request['title'];
            $gallery->description = $request['description'];
            $gallery->save();

            GalleryImage::where('type', 'video')->where('gallery_id', $id)->delete();

            $images = TempImage::where('temp_time', $request['temp_time'])->get();

            ImageUploadHelper::UploadMultipleImage($images, $gallery->id);

            foreach ($request['video_link'] as $file) {
                $galleryImage = new GalleryImage();
                $galleryImage->gallery_id = $gallery->id;
                $galleryImage->type = 'video';
                $galleryImage->image = $file;
                $galleryImage->save();
            }

            return response()->json(['message' => "Gallery Updated"]);
        }
    }


    public function edit( int $id )
    {
        $gallery = Gallery::findorfail($id);
        $videoLinks = GalleryImage::where('gallery_id', $id)->where('type', 'video')->get();
        $images = GalleryImage::where('gallery_id', $id)->where('type', 'image')->get();
        $villages = Village::all();
        return view('admin.gallery.edit', [
            "images"     => $images,
            "videoLinks" => $videoLinks,
            "villages"   => $villages,
            "gallery"    => $gallery
        ]);
    }


    public function destroy( $id )
    {
        $galleryImages = GalleryImage::where('type', 'image')->where('gallery_id', $id)->get();
        foreach ($galleryImages as $galleryImage) {
            ImageDeleteHelper::deleteImage($galleryImage->image);
        }
        GalleryImage::where('gallery_id', $id)->delete();
        Gallery::where('id', $id)->delete();
        return response()->json(['message' => 'Gallery Deleted']);
    }

    public function imageUpload( Request $request ): \Illuminate\Http\JsonResponse
    {

        $files = $request->qqfile;
        $image_path = 'uploads/' . date('Y') . '/' . date('m');
        $extension = $files->getClientOriginalExtension();
        $image_name = $image_path . '/' . uniqid() . '.' . $extension;
        $files->move($image_path, $image_name);

        $tmpImage = new TempImage();
        $tmpImage->name = $image_name;
        $tmpImage->temp_id = $request->qquuid;
        $tmpImage->temp_time = $request->temp_time;
        $tmpImage->save();
        return response()->json([
            'success' => true,
        ]);
    }

    function imageDelete( $id )
    {
        $image = TempImage::where('temp_id', request()->segments()[2])->first()->name;
        ImageDeleteHelper::deleteImage($image);
        TempImage::where('temp_id', request()->segments()[2])->delete();
    }

    function addVideoLink( Request $request )
    {
        $view = view('admin.gallery.videoLink', [
            'row' => $request['row']
        ])->render();
        return response()->json(['data' => $view]);
    }

    public function deleteGalleryImage( $id )
    {
        $image = GalleryImage::where('id', $id)->first();
        if ($image) {
            ImageDeleteHelper::deleteImage($image->image);
            GalleryImage::where('id', $id)->delete();
        }
        return response()->json(['success' => true, 'message' => "Image Deleted"]);

    }

    public function getGalleryImage( Request $request )
    {
        $id = $request['gallery_id'];
        $images = GalleryImage::where('gallery_id', $id)->where('type', 'image')->get();
        return view('admin.gallery.images', ['images' => $images]);
    }
}
