<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use App\Models\Parshwanath;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class ParshwanathController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $parshwanath = Parshwanath::orderBy('order_by','asc')->select('parshwanath.*');
            return DataTables::of($parshwanath)
                ->addColumn('action', function ( $parshwanath ) {
                    $edit_button = '<a href="' . route('admin.parshwanath.edit', [$parshwanath->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $parshwanath->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('image', function ( $parshwanath ) {
                    if ($parshwanath->image) {
                        return '<img src="' . asset($parshwanath->image) . '" style="max-width:200px" />';
                    }
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('admin.parshwanath.index');
    }


    public function create()
    {
        return view('admin.parshwanath.create');
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $circular = new Parshwanath();
            $circular->title = $request['title'];
            $circular->order_by = $request['order_by'];
            $circular->description = $request['description'];
            $circular->image = $image;
            $circular->save();
            return response()->json(['message' => "Parshwanath Added"]);
        } else {
            $circular = Parshwanath::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $circular->image = $image;
            }
            $circular->title = $request['title'];
            $circular->order_by = $request['order_by'];
            $circular->description = $request['description'];
            $circular->save();
            return response()->json(['message' => "Parshwanath Updated"]);
        }
    }


    public function edit( int $id )
    {
        $parshwanath = Parshwanath::findorfail($id);
        return view('admin.parshwanath.edit', ["parshwanath" => $parshwanath]);
    }


    public function destroy( $id )
    {
        Parshwanath::where('id', $id)->delete();
        return response()->json(['message' => 'Parshwanath Deleted']);
    }
}
