<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Village;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        
        $register_members = User::where('member_type', 1)->count();
        $family_members = User::where('member_type', 2)->count();
        $villages = Village::count();

        return view('admin.dashboard', [
            'members'          => $family_members + $register_members,
            'register_members' => $register_members,
            'family_members'   => $family_members,
            'villages'         => $villages,
        ]);
    }


    //show image uplad page
    public function getimage()
    {
        return view('admin.test.image');
    }

    //create product with image
    public function createProduct( Request $request )
    {
        try {
            $key = 'cfc0413d492d3ee8289aafa530e73539';
            $secret = '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';
            $domain = 'the-perfume-warehouse.myshopify.com';


            $parameters = [
                'Authorization' => 'Basic ' . base64_encode("$key:$secret"),
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'charset'       => 'utf-8',
            ];

            $body = [
                "product" => [
                    "title"        => "Burton Custom Freestyle",
                    "body_html"    => "<strong>Good snowboard!</strong>",
                    "vendor"       => "Burton",
                    "product_type" => "Snowboard",
                    "variants"     => [
                        [
                            "option1" => "Blue",
                            "option2" => "155"
                        ],
                        [
                            "option1" => "Black",
                            "option2" => "159"
                        ]
                    ],
                    "options"      => [
                        [
                            "name"   => "Color",
                            "values" => [
                                "Blue",
                                "Black"
                            ]
                        ],
                        [
                            "name"   => "Size",
                            "values" => [
                                "155",
                                "159"
                            ]
                        ]
                    ]
                ]
            ];
            $response = Http::withHeaders($parameters)->post('https://' . $key . ':' . $secret . '@' . $domain . '/admin/api/2021-07/products.json', $body);
            $id = $response['product']['id'];
            $files = $request->file('image');
            foreach ($files as $file) {
                $ibody = [
                    "image" => [
                        'attachment' => base64_encode(file_get_contents($file)),
                    ]
                ];
                $response = Http::withHeaders($parameters)->post('https://' . $key . ':' . $secret . '@' . $domain . '/admin/api/2021-07/products/' . $id . '/images.json', $ibody);
            }
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    //update product with image
    public function updateProduct( Request $request )
    {
        try {
            $key = 'cfc0413d492d3ee8289aafa530e73539';
            $secret = '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';
            $domain = 'the-perfume-warehouse.myshopify.com';


            $parameters = [
                'Authorization' => 'Basic ' . base64_encode("$key:$secret"),
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'charset'       => 'utf-8',
            ];
            $body = [
                "product" => [
                    'id'           => 6830670872749,
                    "title"        => "Burton Custom Freestyle 4444",
                    "body_html"    => "<strong>Good snowboard!</strong>",
                    "vendor"       => "Burton",
                    "product_type" => "Snowboard",
                    "variants"     => [
                        [
                            "option1" => "Blue",
                            "option2" => "155"
                        ],
                        [
                            "option1" => "Black",
                            "option2" => "159"
                        ]
                    ],
                    "options"      => [
                        [
                            "name"   => "Color",
                            "values" => [
                                "Blue",
                                "Black"
                            ]
                        ],
                        [
                            "name"   => "Size",
                            "values" => [
                                "155",
                                "159"
                            ]
                        ]
                    ]
                ]
            ];
            $response = Http::withHeaders($parameters)->put('https://' . $key . ':' . $secret . '@' . $domain . '/admin/api/2021-07/products/6830670872749.json', $body);


            $files = $request->file('image');
            foreach ($files as $file) {
                $ibody = [
                    "image" => [
                        'attachment' => base64_encode(file_get_contents($file)),
                    ]
                ];
                $response = Http::withHeaders($parameters)->post('https://' . $key . ':' . $secret . '@' . $domain . '/admin/api/2021-07/products/6830670872749/images.json', $ibody);
            }


        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function getApiKey()
    {

        $client = new \GuzzleHttp\Client();
        $secret_key = 'sk_live_f57da3213f304082b143e9dc1bb2b854';
        $consumerKey = 'ck_live_2ab02d23823d40468487fbdd7a4dd740';
        $parameters = [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept'       => 'application/json'
            ]
        ];
        $postData = [
            'form_params' => [
                'secret_key'   => $secret_key,
                'consumer_key' => $consumerKey
            ]
        ];
        if (!empty($param)) {
            $parameters['json'] = $param;
        }
        $response = $client->request("post", 'https://api.onbuy.com/v2/auth/request-token', $postData, $parameters);
        $responseHeaders = $response->getHeaders();

        //$usedLimitPercentage = (100*$rateLimit[0])/$rateLimit[1];
        //if($usedLimitPercentage > 95){sleep(5);}
        $responseBody = json_decode($response->getBody(), true);
        return $responseBody;
    }

    public function onbuyCreate()
    {
        try {
            $result = $this->getApiKey();
            $parameters = [
                'Authorization' => $result['access_token'],
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'charset'       => 'utf-8',
            ];

            $body = [
                "site_id"        => 2000,
                "category_id"    => "3407",
                "mpn"            => "EXAMPLEMPN",
                "summary_points" => [
                    "summary point 1",
                    "summary point 2"
                ],

                "brand_name"        => "Test brand",
                "product_name"      => "Red v",
                "description"       => "Comfortable fit, easy release.",
                "default_image"     => "http://www.freepngimg.com/download/lion/3-2-lion-png.png",
                "additional_images" => [
                    "http://c1.staticflickr.com/9/8381/8613405336_11caf04e28_b.jpg"
                ],
                "variant_1"         => [
                    "feature_id" => 102
                ],
                "variants"          => [
                    [
                        "variant_1"     => [
                            "option_id" => 620
                        ],
                        "default_image" => "http://west-moors.co.uk/images/red.png",
                        "rrp"           => "499.99",
                        "product_codes" => [
                            "5034982266920"
                        ],
                        "features"      => [
                            [
                                "option_id" => 93,
                                "name"      => "slime green"
                            ]
                        ],
                        "listings"      => [
                            "new" => [
                                "sku"   => "v99-5076371426621-NEW",
                                "price" => 449.99,
                                "stock" => 3
                            ]
                        ]
                    ],
                    [
                        "variant_1"     => [
                            "option_id" => 621
                        ],
                        "default_image" => "http://west-moors.co.uk/images/blue.png",
                        "rrp"           => "499.99",
                        "product_codes" => [
                            "5011232442754"
                        ],
                        "listings"      => [
                            "new" => [
                                "sku"   => "v99-51080-7B-NEW",
                                "price" => 449.99,
                                "stock" => 3
                            ]
                        ],
                        "features"      => [
                            [
                                "option_id" => 93,
                                "name"      => "puke green",
                                "hex"       => "#A3C00F"
                            ]
                        ],
                        "product_data"  => [
                            [
                                "label" => "label1",
                                "value" => "value1"
                            ],
                            [
                                "label" => "label2",
                                "value" => "value2"
                            ]
                        ]
                    ],
                    [
                        "variant_1"     => [
                            "option_id" => 622
                        ],
                        "default_image" => "http://west-moors.co.uk/images/green.png",
                        "rrp"           => "499.99",
                        "product_codes" => [
                            "5031459073043"
                        ],
                        "features"      => [
                            [
                                "option_id" => 93,
                                "name"      => "putting green"
                            ]
                        ],
                        "listings"      => [
                            "new" => [
                                "sku"   => "v3-94462-7B-NEW",
                                "price" => 449.99,
                                "stock" => 3
                            ]
                        ],
                        "product_data"  => [
                            [
                                "label" => "label1",
                                "value" => "value1"
                            ],
                            [
                                "label" => "label2",
                                "value" => "value2"
                            ]
                        ]
                    ]
                ]
            ];
            $response = Http::withHeaders($parameters)->post('https://api.onbuy.com/v2/products', $body);

            dd($response['success']);

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function categories()
    {
        try {
            $result = $this->getApiKey();
            $parameters = [
                'Authorization' => $result['access_token'],
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'charset'       => 'utf-8',
            ];
            $response = Http::withHeaders($parameters)->get('https://api.onbuy.com/v2/categories?site_id=2000');
            dd($response['results']);

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function products()
    {
        try {
            $result = $this->getApiKey();
            $parameters = [
                'Authorization'                => $result['access_token'],
                'Content-Type'                 => 'application/json',
                'Accept'                       => 'application/json',
                'charset'                      => 'utf-8',
                'Access-Control-Allow-Methods' => '*',
                'Access-Control-Allow-Origin'  => '*',

            ];
            $body = [
                "site_id"  => 2000,
                "listings" => [
                    "opc"       => 1234512345123,
                    "condition" => [
                        'sku'       => 44 - 87654,
                        'group_sku' => "hhaa",
                    ],
                ],
            ];

            $response = Http::withHeaders($parameters)->post('https://api.onbuy.com/v2/products/1234512345123/listings', $body);
            dd($response['error']);

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function brands()
    {
        try {
            $result = $this->getApiKey();
            $parameters = [
                'Authorization' => $result['access_token'],
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'charset'       => 'utf-8',
            ];
            $response = Http::withHeaders($parameters)->get('https://api.onbuy.com/v2/brands');
            dd($response['results']);

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function myProduct()
    {
        try {
            $parameters = [
                'headers' => [
                    'X-EBAY-API-SITEID'              => 0,
                    'X-EBAY-API-COMPATIBILITY-LEVEL' => 967,
                    'X-EBAY-API-CALL-NAME'           => 'GetMyeBaySelling',
                    'X-EBAY-API-IAF-TOKEN'           => 'v^1.1#i^1#r^0#p^3#I^3#f^0#t^H4sIAAAAAAAAAOVYa4gcVRaenlcczEQ2ilEjO225iiZb3fdWVVdV1063dqY7mdZ5dKY7EzOsTm5V3Zoup7qqqHsrk2ZRZkc2+NqNIEtYiBjUSH4Ikh+iKIoQQWE3YaOgrhCj4AvUdV8iYRd3q3oe6YxOMjMdSMP2j+6uc8/rO/fcU+dcMN3ZtWlf/77vuiNrWg9Ng+nWSAReDro6Ozava2u9rqMF1DFEDk3/bLp9pu2LXoIqlquMYOI6NsHRvRXLJkqNmGJ8z1YcREyi2KiCiUI1pZgZHFC4GFBcz6GO5lhMNJ9NMVLCSMq8noAazwuCkQyo9rzOkpNiDAySCSGhigLSOVnSg3VCfJy3CUU2TTEc4CALJJYDJSgrHK9wYgwIYIyJjmKPmI4dsMQAk665q9RkvTpfz+8qIgR7NFDCpPOZrcXhTD6bGyr1xut0pefiUKSI+uTcpz5Hx9FRZPn4/GZIjVsp+pqGCWHi6VkL5ypVMvPOrML9WqgTvMpjJGFsqCLHQfWihHKr41UQPb8fIcXUWaPGqmCbmrR6oYgG0VDvxRqdexoKVOSz0fBnu48s0zCxl2JyWzK7dhRzI0y0WCh4zh5Tx3qIFELICaLIA4FJ0zJ2sWf4FTyFPFx2fII1x5+cMzmrdy7gi2z2ObZuhuEj0SGHbsGB/3hxlGBdlAKmYXvYyxg09K2OjwPz0eT5sXB7Z/fTp2U73GFcCUISrT1eeC/mk+NsOlys9IAYqiJCCcgFfwRVm0uP8Kw3lCLpcJcyhUI89AWrqMpWkDeJqWshDbNaEN5gbzxTV/iEwfGygVldTBqskDQMVk3oIgsNjAHGqqol5f/PTKHUM1Wf4oVsWbxQg5tiiprj4oJjmVqVWcxSq0NzubGXpJgypa4Sj09NTcWm+JjjTcQ5AGD8rsGBolbGFcQs8JoXZmbNWoZoOJAipkKrbuDN3iAJA+P2BJPmPb2APFotYssKCPMpfI5v6cXUJUD2WWYQgVJgorkw9juEYr0haJYzYdqDmJYd/ZJgC8/6kvhyg5n8QEPwMq6br1R8ilQL5y8NwiXRCQInSVJD8MLappjIUKgzie3mS9CR3NaRXLF/vDR8Z26oIaRFrHmYNhe6zB07RsfyldH+hLtlSII7h6i4efOQX962i5aJIJcyRaLv3azuzE3KqYbAD06YTZa7HJCgIMgCJwIgLhdbeNZ/FF9uwm82gFKCM4DKQyghgIQE5g0NJ2UIDUMUoYwTDVelJsMLYbEvaIsH2CyuOGxhJMuKkmGoHJQBK3FQM0RJbQgzCRuF5sIcypNAAXLNWFhJY5pTiTso6IpD0njN4+hymOIkaDJisz1moDnmYaQ7tlVdjfAKZEx7T9CWOF51NQYXhFcgg7SgO7XpcsyFZ30J8RUYNHzLMC0r7D9Xg7FOfCUobWRVqamRVZk07TDjyApEXFStAdRN4obnZVmSAS0YYTQcC8aK2nS7QmcX5G2HBgOLhsJZIkZ8lWie6daGuoukZ8GxhsqHh3XTC8afcd8zm6uKQDheK53jA/nBfCnHLqqkLHE0Ry1bRkPww6hfwi59SfCFTLG4s32mlRseyTYEMIv3NNsrUeQ1iCWYYAUsh1+iwcoyRKyuaViXBJ5HWrIhzCZqsoYWipwog4QAhOXiWkSouy34wZVR/Nzb23RL7QNnIi+AmcjR1kgExMFN8EZwQ2fbjva2tdcRkwbVDRkxYk7YiPoejk3iqotMr7UzMrXh5cOv1t0XH7obXLNwY9zVBi+vuz4G159d6YBXbOjmIJA4AGWO58QxcOPZ1XZ4dftVn5966K+pY9duuuE3H8kHHij3jB/MPwK6F5gikY6W9plIS/S+pza8ta1l967b7/n1hv7Im19Xn/n3Sz3PkuNTN/fe/NLhjZc99MH9h17pOfb7Xvn2Y5uK/3zndz+/7B/u/sju7ZveWbPrtee/OqLe0n86W/rL/tSDieTbU8e7vY1HPzFatm778H29LfEEtn45Hn/0s/ts88gV//n60ZPuN93gw9FO49PT1xc42/nzmd9+H+vqeuD5azqO3bH9oxOntrzI/GvdrWfGzhx59fSfDr77k6z9x6MnDp5898szvWhNDxxZX/77G/tv+fbO1z/4FZfsu+1k8qdXvpwu3dsxuO9Ay3NHFf9v+uTjPYc7Ovr+u/EXp9YO/2H9kyN3X7v26er6uz7bfeS9feuq3zysnnjr4x79++Psgcci3uz2/Q++YX7uyRcAAA==',
                    'Content-Type'                   => 'text/plain',
                    'Cookie'                         => 'dp1=bu1p/QEBfX0BAX19AQA**64b42d0e^bl/US64b42d0e^; nonsession=CgADKACBktC0OYjAxYjNkOGYxN2EwYWM3MWUyYjI0N2MzZmZmZTU3YTVwr20l'
                ]

            ];
            $body = '<?xml version="1.0" encoding="utf-8"?>
<GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">    
	<ErrorLanguage>en_US</ErrorLanguage>
	<WarningLevel>High</WarningLevel>
  <ActiveList>
    <Sort>TimeLeft</Sort>
    <Pagination>
      <EntriesPerPage>25</EntriesPerPage>
      <PageNumber>1</PageNumber>
    </Pagination>
  </ActiveList>
</GetMyeBaySellingRequest>';

            $client = new Client(['base_uri' => 'https://api.ebay.com/ws/']);
            $response = $client->request('POST', 'api.dll', $parameters, [
                'form_params' => $body
            ]);
//            $response = Http::withHeaders($parameters)->post('https://api.ebay.com/ws/api.dll',['body'=>$body]);
            $xml = simplexml_load_string($response->getBody(), 'SimpleXMLElement', LIBXML_NOCDATA);
            echo "<pre>";
            $json = json_encode($xml);
            $jsonResultToArray = json_decode($json, TRUE);
            print_r($jsonResultToArray);
//            print_r($response);
//            $xml = simplexml_load_string($response);

//            $json = json_encode($xml);
//
//            $jsonResultToArray = json_decode($json, TRUE);
//            echo "<pre>";
//            print_r($jsonResultToArray);
//            exit;

        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }
}
