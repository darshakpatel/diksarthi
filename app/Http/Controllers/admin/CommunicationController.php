<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\TemplateResource;
use App\Http\Requests\NotificationStoreRequest;
use App\Models\UserDevice;
use App\Models\Notification;
use App\Models\Template;
use App\Mail\SendEmail;
use Mail;
use DB;
use Illuminate\Http\Request;

class CommunicationController extends Controller
{
    public function sendEmail()
    {
        return view('admin.communication.sendEmail');
    }

    public function sendSms()
    {
        return view('admin.communication.sendSms');
    }

    public function sendNotification()
    {
        return view('admin.communication.sendNotification');
    }

    public function sendEmailProcess( Request $request )
    {
        if ($request['member_type'] == 0) {
            $members = DB::table('users')->where('email', '!=', null)->get();
        } elseif ($request['member_type'] == 1) {
            $members = DB::table('users')->where('member_type', 1)->where('email', '!=', null)->get();
        } else {
            $members = DB::table('users')->where('member_type', 2)->where('email', '!=', null)->get();
        }
        foreach ($members as $member) {
            if (config('email_notification') == 1) {
                Mail::to($member->email)->send(new SendEmail([
                    'name'    => $member->name,
                    'subject' => $request['subject'],
                    'message' => $request['message'],
                ]));
            }
        }

        return response()->json(['message' => 'Email Sent Successfully'], 200);
    }

    public function sendNotificationProcess( Request $request )
    {
        if ($request['member_type'] == 0) {
            $members = DB::table('users')->pluck('id')->toArray();
            $devices = UserDevice::whereIn('user_id', $members)->get();
        } elseif ($request['member_type'] == 1) {
            $members = DB::table('users')->where('member_type', 1)->pluck('id')->toArray();
            $devices = UserDevice::whereIn('user_id', $members)->get();
        } else {
            $members = DB::table('users')->where('member_type', 2)->pluck('id')->toArray();
            $devices = UserDevice::whereIn('user_id', $members)->get();
        }
        $ios_array = $android_array = [];
        foreach ($devices as $device) {
            if (config('push_notification') == 1) {
                $notification = new Notification();
                $notification->user_id = $device->user_id;
                $notification->title = $request['title'];
                $notification->message = $request['message'];
                $notification->is_sent = 0;
                $notification->save();

                if ($device->device_type == 1) {
                    $android_array[] = $device->device_token;
                }
                if ($device->device_type == 0) {
                    $ios_array[] = $device->device_token;
                }

            }
        }
        $array['title'] = $request['title'];
        $array['message'] = $request['message'];
        $array['android_array'] = $android_array;
        $device = new UserDevice();
        $device->sendNotification($array);

        return response()->json(['message' => 'Notification Sent Successfully'], 200);
    }
}
