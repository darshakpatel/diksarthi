<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\TapasviSammanStoreRequest;
use App\Helpers\ImageUploadHelper;
use App\Models\TapasviSamman;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;


class TapasviSammanController extends Controller
{

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $tapasvi_samman = TapasviSamman::orderBy('order_by','asc')->select('tapasvi_samman.*');
            return DataTables::of($tapasvi_samman)
                ->addColumn('action', function ( $tapasvi_samman ) {
                    $edit_button = '<a href="' . route('admin.tapasviSamman.edit', [$tapasvi_samman->id]) . '" class="btn btn-blue waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.edit') . '"><i class="mdi mdi-pencil"></i></a>';
                    $delete_button = '<button data-id="' . $tapasvi_samman->id . '" class="delete-single btn btn-danger waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="' . trans('messages.delete') . '"><i class="fa fa-trash"></i></a>';
                    return $edit_button . ' ' . $delete_button;
                })
                ->addColumn('image', function ( $tapasvi_samman ) {
                    if ($tapasvi_samman->image) {
                        return '<img src="' . asset($tapasvi_samman->image) . '" style="max-width:200px" />';
                    }
                })
                ->rawColumns(['action','image'])
                ->make(true);
        }
        return view('admin.tapasviSamman.index');
    }


    public function create()
    {
        return view('admin.tapasviSamman.create');
    }


    public function store( Request $request )
    {
        $id = $request['edit_value'];
        if ($id == 0) {
            $image = null;
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
            }
            $circular = new TapasviSamman();
            $circular->title = $request['title'];
            $circular->order_by = $request['order_by'];
            $circular->image = $image;
            $circular->save();
            return response()->json(['message' => "Tapasvi Samman Added"]);
        } else {
            $circular = TapasviSamman::find($id);
            if ($request->hasFile('image')) {
                $image = ImageUploadHelper::ImageUpload($request['image']);
                $circular->image = $image;
            }
            $circular->title = $request['title'];
            $circular->order_by = $request['order_by'];
            $circular->save();
            return response()->json(['message' => "Tapasvi Samman Updated"]);
        }
    }


    public function edit( int $id )
    {
        $tapasviSamman = TapasviSamman::findorfail($id);
        return view('admin.tapasviSamman.edit', ["tapasviSamman" => $tapasviSamman]);
    }


    public function destroy( $id )
    {
        TapasviSamman::where('id', $id)->delete();
        return response()->json(['message' => 'Tapasvi Samman Deleted']);
    }
}
