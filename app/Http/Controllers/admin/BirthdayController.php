<?php

namespace App\Http\Controllers\admin;


use App\Http\Controllers\Controller;
use App\Helpers\ImageUploadHelper;
use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Models\Template;
use App\Models\Notification;
use App\Models\UserDevice;
use App\Mail\SendEmail;
use App\Helpers\SmsHelper;
use Mail;
use Yajra\DataTables\Facades\DataTables;


class BirthdayController extends Controller
{
    public function todayBirthday( Request $request )
    {
        if ($request->ajax()) {
            $users = DB::table('users')
                ->leftjoin('villages', 'villages.id', 'users.village_id')
                ->whereMonth('users.date_of_birth', date('m'))
                ->whereDay('users.date_of_birth', date('d'))
                ->select('users.*', 'villages.name as village');
            return DataTables::of($users)
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        $is_sent = DB::table('today_sms')->where('type', 'birthday')->where('sent_date', date('Y-m-d'))->exists();
        return view('admin.todaySms.birthday', [
            'is_sent' => $is_sent
        ]);
    }

    public function todayAnniversary( Request $request )
    {
        if ($request->ajax()) {
            $users = DB::table('users')
                ->leftjoin('villages', 'villages.id', 'users.village_id')
                ->whereMonth('users.anniversary_date', date('m'))
                ->whereDay('users.anniversary_date', date('d'))
                ->select('users.*', 'villages.name as village');
            return DataTables::of($users)
                ->rawColumns(['action', 'image'])
                ->make(true);
        }
        $is_sent = DB::table('today_sms')->where('type', 'anniversary')->where('sent_date', date('Y-m-d'))->exists();
        return view('admin.todaySms.anniversary', [
            'is_sent' => $is_sent
        ]);
    }

    public function todayBirthdaySend()
    {
        DB::table('today_sms')->where('type', 'birthday')->where('sent_date', '!=', date('Y-m-d'))->delete();
        if (!DB::table('today_sms')->where('type', 'birthday')->where('sent_date', date('Y-m-d'))->exists()) {
            $users = User::whereMonth('date_of_birth', date('m'))->whereDay('date_of_birth', date('d'))->get();
            foreach ($users as $user) {
                if (config('email_notification') == 1 && $user->email) {
                    $template = Template::where('name_key', 'happy_birthday_mail')->first();
                    $subject = $template->subject;
                    $body = str_replace(array('{{name}}', '{#var#}'), array(
                        $user->name, $user->name
                    ), $template->description);
                    Mail::to($user->email)->send(new SendEmail([
                        'subject' => $subject,
                        'message' => $body,
                    ]));
                }
                if (config('sms_notification') == 1 && $user->mobile_no) {
                    $template = Template::where('id', 14)->first();
                    $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                    SmsHelper::sendDynamicSms([
                        'mobile_no' => $user->mobile_no,
                        'message'   => $template['description'],
                        'api'       => $template->api_key
                    ]);
                }
                $android_array = [];
                if (config('push_notification') == 1) {
                    $devices = DB::table('user_devices')->where('user_id', $user->id)->get();
                    foreach ($devices as $device) {
                        if ($device->device_type == 0) {
                            $android_array[] = $device->device_token;
                        }
                    }
                    $template = Template::where('id', 12)->first();
                    $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                    $notification = new Notification();
                    $notification->user_id = $user->id;
                    $notification->title = $template['title'];
                    $notification->message = $template['description'];
                    $notification->is_sent = 1;
                    $notification->save();

                    $array['title'] = $template['title'];
                    $array['message'] = $template['description'];
                    $array['android_array'] = $android_array;
                    $device = new UserDevice();
                    $device->sendNotification($array);
                }
            }
            DB::table('today_sms')->insert([
                'type'      => 'birthday',
                'sent_date' => date('Y-m-d')
            ]);
        }
        return redirect()->back();
    }

    public function todayAnniversarySend()
    {
        DB::table('today_sms')->where('type', 'anniversary')->where('sent_date', '!=', date('Y-m-d'))->delete();
        if (!DB::table('today_sms')->where('type', 'anniversary')->where('sent_date', date('Y-m-d'))->exists()) {
            $ann_users = User::whereMonth('anniversary_date', date('m'))->whereDay('anniversary_date', date('d'))->get();
            foreach ($ann_users as $user) {
                if (config('email_notification') == 1 && $user->email) {
                    $template = Template::where('name_key', 'happy_anniversary_mail')->first();
                    $subject = $template->subject;
                    $body = str_replace(array('{{name}}', '{#var#}'), array(
                        $user->name, $user->name
                    ), $template->description);
                    Mail::to($user->email)->send(new SendEmail([
                        'subject' => $subject,
                        'message' => $body,
                    ]));
                }
                if (config('sms_notification') == 1 && $user->mobile_no) {
                    $template = Template::where('id', 15)->first();
                    $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                    SmsHelper::sendDynamicSms([
                        'mobile_no' => $user->mobile_no,
                        'message'   => $template['description'],
                        'api'       => $template->api_key
                    ]);
                }
                $android_array = [];
                if (config('push_notification') == 1) {
                    $devices = DB::table('user_devices')->where('user_id', $user->id)->get();
                    foreach ($devices as $device) {
                        if ($device->device_type == 0) {
                            $android_array[] = $device->device_token;
                        }
                    }
                    $template = Template::where('id', 13)->first();
                    $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                    $notification = new Notification();
                    $notification->user_id = $user->id;
                    $notification->title = $template['title'];
                    $notification->message = $template['description'];
                    $notification->is_sent = 1;
                    $notification->save();

                    $array['title'] = $template['title'];
                    $array['message'] = $template['description'];
                    $array['android_array'] = $android_array;
                    $device = new UserDevice();
                    $device->sendNotification($array);
                }
            }
            DB::table('today_sms')->insert([
                'type'      => 'anniversary',
                'sent_date' => date('Y-m-d')
            ]);
        }
        return redirect()->back();
    }
}
