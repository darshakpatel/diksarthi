<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;

class ApiVersion
{
    public function handle( $request, Closure $next )
    {
        $domain = explode('/', $request->url())[2];
        if ($domain == 'dharmadeep.com') {
//            return response()->json(['success' => false, 'message' => 'New version is available on play store please update the app.']);
        }
        return $next($request);
    }
}
