<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use App\Models\Setting;
use App\Models\Template;

class SmsHelper
{
    public static function sendSms( $array )
    {
        $number = $array['mobile_no'];
        if (config('sms_notification') == 0) {
            $otp = 123456;
            return $otp;
        } else {
            $otp = rand(111111, 999999);
            $template = Template::where('name_key', 'otp_sms')->first();
            $text = $template['description'];
            $text = str_replace('{{otp}}', $otp, $text);
            $text = urlencode(utf8_encode($text));
            $api = Setting::where('meta_key', 'sms_api')->first()->meta_value;
            $sms = str_replace('%1', $number, $api);
            $sms = str_replace('%2', $text, $sms);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $sms);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            $headers = [];
            $headers[] = "Accept: application/json";
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($ch);
            $res = explode('|', $result);
            if (curl_errno($ch)) {
                \Log::info('Error:' . curl_error($ch));
            }
            curl_close($ch);
            return $otp;
        }
    }

    public static function sendDynamicSms( $array )
    {
        $number = $array['mobile_no'];
        $text = $array['message'];
        $text = urlencode(utf8_encode($text));
        $api = $array['api'];
        $sms = str_replace(array('%1', '%2'), array($number, $text), $api);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $headers = [];
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        $res = explode('|', $result);
        \Log::info($sms);
        \Log::info(print_r($res,true));
        if (curl_errno($ch)) {
            \Log::info('Error:' . curl_error($ch));
        }
        curl_close($ch);
    }
}
