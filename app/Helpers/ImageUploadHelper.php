<?php

namespace App\Helpers;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use App\Models\GalleryImage;

class ImageUploadHelper
{
    public static function ImageUpload( $image ): string
    {
        $path = 'uploads/' . date('Y') . '/' . date('m');
        if (!File::exists(public_path() . "/" . $path)) {
            File::makeDirectory(public_path() . "/" . $path, 0777, true);
        }
        $files = $image;
        $extension = $image->getClientOriginalExtension();
        $destination_path = public_path() . '/' . $path;
        $image_name = uniqid() . '.' . $extension;
        $files->move($destination_path, $image_name);
        return $path . '/' . $image_name;
    }

    public static function UploadMultipleImage( $images, $id )
    {
        foreach ($images as $val) {
            $image = new GalleryImage();
            $image->gallery_id = $id;
            $image->type = 'image';
            $image->image = $val->name;
            $image->save();
        }
    }
}
