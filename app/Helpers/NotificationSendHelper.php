<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;

class NotificationSendHelper
{
    public static function sendNotificationDirect($array)
    {
        $fields = [
            'registration_ids' => $array['token'],
            'notification'     =>
                [
                    'title'    => $array['title'],
                    'body'     => $array['message'],
                    'vibrate'  => "1",
                    'sound'    => "mySound",
                    "priority" => "high",
                ],
            'data'             => ['message' => $array['message'], 'title' => $array['title']],
        ];

        $response = Http::withHeaders([
            'Authorization:key' => 'AAAA7bi4koM:APA91bGXmtdVpOQmg8Gxbqy1PN0pb6eAivZU6K-kkwKuG6SwxNTlA_M7RHEJ1Qp0zUkQkd0YEIOCqaCI3CwAOeQAXmy8TSkGdXznXf9v1F3HnCdVIXs78m_eH7lVQ7oCfTos2VhYIMSo',
            'Content-Type'      => 'application/json'
        ])->post('https://fcm.googleapis.com/fcm/send', $fields);
    }

}
