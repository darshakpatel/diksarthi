<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UserExport implements FromCollection, WithHeadings, WithEvents
{

    private $data;

    public function __construct( $data )
    {
        $this->data = $data;
    }

    public function collection()
    {
        $array = [];
        foreach ($this->data as $value) {

            $array[] = [
                'family_no'     => $value->family_no,
                'name'          => $value->name,
                'village'       => $value->village->name ?? '',
                'type'          => $value->member_type == 1 ? 'Registered Member' : 'Family Member',
                'mobile_no'     => $value->mobile_no,
                'date_of_birth' => $value->date_of_birth,
                'relation'      => $value->relation->name??'',
                'email'         => $value->email,
                'address_1'     => $value->address_1,
                'address_2'     => $value->address_2,
                'address_3'     => $value->address_3,
                'city'          => $value->city,
                'pincode'       => $value->pincode,
            ];
        }
        return collect($array);
    }

    public function headings(): array
    {
        return [
            'Family ID',
            'Name',
            'Village',
            'Type',
            'Mobile No',
            'Birth Date',
            'Relation',
            'Email',
            'Address 1',
            'Address 2',
            'Address 3',
            'City',
            'Pincode',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function ( AfterSheet $event ) {
                $border_cellRange = 'A1:M' . $event->sheet->getDelegate()->getHighestRow();
                $event->sheet->getDelegate()->getStyle('A1:P1')->getFont()->setBold(true);
                $event->sheet->getStyle($border_cellRange)->applyFromArray([
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => Border::BORDER_THIN, 'color' => ['argb' => '000000']
                        ]
                    ]
                ]);
                $event->sheet->getDelegate()->getColumnDimension('A')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('B')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('C')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('D')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('E')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('F')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('G')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('H')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('I')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('J')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('K')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('L')->setAutoSize(true);
                $event->sheet->getDelegate()->getColumnDimension('M')->setAutoSize(true);
            },
        ];
    }
}
