<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $sms_notification = DB::table('settings')->where('meta_key', 'sms_notification')->first()->meta_value;
        $email_notification = DB::table('settings')->where('meta_key', 'email_notification')->first()->meta_value;
        $push_notification = DB::table('settings')->where('meta_key', 'push_notification')->first()->meta_value;
        \Config::set('sms_notification', $sms_notification);
        \Config::set('email_notification', $email_notification);
        \Config::set('push_notification', $push_notification);
    }
}
