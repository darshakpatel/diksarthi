<?php

namespace App\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class Subscription extends Command
{

    protected $signature = 'subscription';
    protected $description = 'subscription';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $templates = \App\Models\Subscription::where('end_date', '>', date('Y-m-d'))->update(['status' => 'expired']);
    }
}



