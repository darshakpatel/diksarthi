<?php

namespace App\Console;

use App\Models\Notification;
use App\Models\PendingNotification;
use App\Models\UserDevice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Mockery\Matcher\Not;

class SendNotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $device = new UserDevice();

        $array = [];
        $notifications = Notification::where('is_sent', 0)->get();
        foreach ($notifications as $notification) {
            $array['title'] = $notification->title;
            $array['send_type'] = 1;
            $array['message'] = $notification->message;
            $array['is_admin'] = $notification->is_admin;

            $pending_notification_query = PendingNotification::
            where('notification_id', $notification->id)
                ->select('device_token', 'device_type', 'id')
                ->limit(995)
                ->get();
            $array['ios_array'] = $android_array = $array['android_array'] = $ids_array = $ios_array = [];


            foreach ($pending_notification_query as $token_array) {
                $android_array[] = $token_array->device_token;
                $ids_array[] = $token_array->id;
            }

            PendingNotification::whereIn('id', $ids_array)->delete();

            $array['android_array'] = $android_array;
            $array['ios_array'] = $ios_array;

            $device->sendNotification($array);
            if (PendingNotification::where('notification_id', $notification->id)->count() == 0) {
                Notification::where('id', $notification->id)
                    ->update([
                        'is_sent' => 1
                    ]);
            }

        }
    }
}
