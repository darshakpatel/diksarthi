<?php

namespace App\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\Template;
use App\Models\Notification;
use App\Models\UserDevice;
use App\Mail\SendEmail;
use App\Helpers\SmsHelper;
use Mail;
use DB;

class Communication extends Command
{

    protected $signature = 'communication';
    protected $description = 'communication';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $users = User::whereMonth('date_of_birth', date('m'))->whereDay('date_of_birth', date('d'))->get();

        foreach ($users as $user) {
            if (config('email_notification') == 1 && $user->email) {
                $template = Template::where('name_key', 'happy_birthday_mail')->first();
                $subject = $template->subject;
                $body = str_replace(array('{{name}}', '{#var#}'), array(
                    $user->name, $user->name
                ), $template->description);
                Mail::to($user->email)->send(new SendEmail([
                    'subject' => $subject,
                    'message' => $body,
                ]));
            }
            if (config('sms_notification') == 1 && $user->mobile_no) {
                $template = Template::where('id', 14)->first();
                $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                SmsHelper::sendDynamicSms([
                    'mobile_no' => $user->mobile_no,
                    'message'   => $template['description'],
                    'api'       => $template->api_key
                ]);
            }
            $android_array = [];
            if (config('push_notification') == 1) {
                $devices = DB::table('user_devices')->where('user_id', $user->id)->get();
                foreach ($devices as $device) {
                    if ($device->device_type == 0) {
                        $android_array[] = $device->device_token;
                    }
                }
                $template = Template::where('id', 12)->first();
                $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->title = $template['title'];
                $notification->message = $template['description'];
                $notification->is_sent = 1;
                $notification->save();

                $array['title'] = $template['title'];
                $array['message'] = $template['description'];
                $array['android_array'] = $android_array;
                $device = new UserDevice();
                $device->sendNotification($array);
            }
        }

        DB::table('today_sms')->insert([
            'type'      => 'birthday',
            'sent_date' => date('Y-m-d')
        ]);

        $ann_users = User::whereMonth('anniversary_date', date('m'))->whereDay('anniversary_date', date('d'))->get();

        foreach ($ann_users as $user) {
            if (config('email_notification') == 1 && $user->email) {
                $template = Template::where('name_key', 'happy_anniversary_mail')->first();
                $subject = $template->subject;
                $body = str_replace(array('{{name}}', '{#var#}'), array(
                    $user->name, $user->name
                ), $template->description);
                Mail::to($user->email)->send(new SendEmail([
                    'subject' => $subject,
                    'message' => $body,
                ]));
            }
            if (config('sms_notification') == 1 && $user->mobile_no) {
                $template = Template::where('id', 15)->first();
                $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                SmsHelper::sendDynamicSms([
                    'mobile_no' => $user->mobile_no,
                    'message'   => $template['description'],
                    'api'       => $template->api_key
                ]);
            }
            $android_array = [];
            if (config('push_notification') == 1) {
                $devices = DB::table('user_devices')->where('user_id', $user->id)->get();
                foreach ($devices as $device) {
                    if ($device->device_type == 0) {
                        $android_array[] = $device->device_token;
                    }
                }
                $template = Template::where('id', 13)->first();
                $template['description'] = str_replace('{#var#}', $user->name, $template['description']);
                $notification = new Notification();
                $notification->user_id = $user->id;
                $notification->title = $template['title'];
                $notification->message = $template['description'];
                $notification->is_sent = 1;
                $notification->save();

                $array['title'] = $template['title'];
                $array['message'] = $template['description'];
                $array['android_array'] = $android_array;
                $device = new UserDevice();
                $device->sendNotification($array);
            }
        }
        DB::table('today_sms')->insert([
            'type'      => 'anniversary',
            'sent_date' => date('Y-m-d')
        ]);
    }
}



