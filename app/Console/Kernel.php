<?php

namespace App\Console;

use App\Console\Subscription;
use App\Console\SendNotificationCommand;
use App\Console\Communication;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    protected $commands = [
        Subscription::class,
        SendNotificationCommand::class,
        Communication::class,
    ];

    protected function schedule( Schedule $schedule )
    {
        $schedule->command('send_notification')
            ->everyMinute();
        $schedule->command('communication')
            ->dailyAt('06:00');
    }

    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
