<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class MemberImport implements ToModel, WithHeadingRow
{
    public function model( array $row )
    {
        return [
            'village'          => $row['Village'],
            'p_no'             => $row['P No.'],
            'family_no'        => $row['Fam No:'],
            'per_no'           => $row['Per No:'],
            'name'             => $row['Name'],
            'relation'         => $row['Relationship'],
            'petron'           => $row['P/L'],
            'piyar_paksh'      => $row['Piyar Paksh'],
            'anniversary_date' => $row['Anniversary'],
            'date_of_birth'    => $row['DOB'],
            'education'        => $row['Education'],
            'marital_status'   => $row['Married'],
            'blood_group'      => $row['Blood'],
            'mobile_no'        => $row['Mob . No'],
            'residency_no'     => $row['Res. No.1'],
            'office_no'        => $row['Office No.1'],
            'business'         => $row['Business'],
            'address_1'        => $row['ADRL1'],
            'address_2'        => $row['ADRL2'],
            'address_3'        => $row['ADRL3'],
            'city'             => $row['CITY'],
            'pincode'          => $row['PIN'],
            'email'            => $row['E-MAIL'],
            'special'          => $row['SPECIAL'],
        ];
    }
}
