<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Models\Template;
use Illuminate\Queue\SerializesModels;

class OtpMail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct( $details )
    {
        $this->details = $details;
    }

    public function build()
    {
        $template = Template::where('name_key', 'otp_email')->first();
        $title = $template->title;
        $subject = $template->subject;
        $body = str_replace('{{otp}}', $this->details['otp'], $template->description);
        $body = str_replace('{{name}}', $this->details['name'], $body);
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->with([
                'body'            => $body,
                'main_title_text' => $title,
            ])
            ->subject($subject)
            ->view('emails.otpEmail');

    }
}
