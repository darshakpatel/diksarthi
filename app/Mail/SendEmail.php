<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use App\Models\Template;
use Illuminate\Queue\SerializesModels;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;

    public function __construct( $details )
    {
        $this->details = $details;
    }

    public function build()
    {
        $subject = $this->details['subject'];
        $body = $this->details['message'];
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->with([
                'body' => $body,
            ])
            ->subject($subject)
            ->view('emails.sendEmail');

    }
}
