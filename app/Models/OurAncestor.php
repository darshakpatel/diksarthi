<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurAncestor extends Model
{
    protected $guarded = [];

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
