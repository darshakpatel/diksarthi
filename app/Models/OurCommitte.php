<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurCommitte extends Model
{
    protected $table='our_committe';
    protected $guarded = [];
}
