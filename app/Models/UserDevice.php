<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    protected $guarded = [];

    public function sendNotification($array)
    {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = [];
        if (isset($array['ios_array']) && count($array['ios_array']) > 0) {
            $fields = [
                'registration_ids' => $array['ios_array'],
                'notification' =>
                    [
                        'title' => $array['title'],
                        'body' => $array['message'],
                        'vibrate' => "1",
                        'sound' => "mySound",
                        "priority" => "high",
                    ],
                'data' =>
                    [
                        'message' => $array['message'],
                    ],
            ];
        }
        if (isset($array['android_array']) && count($array['android_array']) > 0) {
            $fields = [
                'registration_ids' => $array['android_array'],
                'priority' => 'high',
                'android' => [
                    'priority' => 'high',
                ],
                'data' => [
                    'title' => $array['title'],
                    'message' => $array['message'],
                ],

            ];
        }
        //  dd(json_encode($fields));


        $headers = [
            'Authorization:key=AAAAnnuqWUg:APA91bEZ8D9xcT-l0KeYgv17CcEYe1q56c91pLfuPmDgzuwWOaAj6RWMKrzPVBhwBhX6HKjtRxj-iL5vINkTTOWtGyqRz0TBDwPK3VkNY0lZ9w_-8_AQ7EqxTvh0iDI2ervSjC7L4cLK',
            'Content-Type:application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        //dd($result);
        curl_close($ch);
        //    \Log::info(json_decode($result,true));
        // \Log::info($array);
        return $result;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
