<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TapasviSamman   extends Model
{
    protected $guarded = [];
    protected $table='tapasvi_samman';
}
