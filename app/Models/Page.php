<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [];
    public function sluggable()
    {
        return [
            'page_slug' => [
                'source' => 'name',
            ],
        ];
    }
}
