<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
    protected $guarded = [];

    public function galleryImage()
    {
        return $this->hasMany(GalleryImage::class);
    }

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
