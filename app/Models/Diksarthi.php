<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Diksarthi extends Model
{
    public $table = 'diksarthi';
    protected $guarded = [];

    public function village()
    {
        return $this->belongsTo(Village::class)->orderBy('name', 'asc');
    }
}
