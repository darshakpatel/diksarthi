<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VillageDerasar extends Model
{
    protected $guarded = [];
    protected $table = 'village_derasar';

    public function village()
    {
        return $this->belongsTo(Village::class);
    }
}
