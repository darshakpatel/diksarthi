$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/gallery',
            type: 'GET',
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'date', name: 'date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        drawCallback: function () {
            funDataTableCheck('datable_check')
            funDataTableUnCheck('datable_check')
            funTooltip()
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[1, 'ASC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    })

    $(document).on('click', '#bulk-option-btn', function () {
        table.draw()
    })

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data('id')
        Swal.fire({
            title: 'Delete Gallery',
            text: 'Are you sure you want to delete',
            type: 'warning',
            showCancelButton: !0,
            confirmButtonColor: '#556ee6',
            cancelButtonColor: '#f46a6a'
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id)
            }
        })
    })

    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault()
            loaderView()
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/gallery', formData)
                .then(function (response) {
                    $form.removeClass('was-validated')
                    setTimeout(function () {
                        window.location.href = APP_URL + '/gallery'
                    }, 1000)
                    successToast(response.data.message, 'success')
                })
                .catch(function (error) {
                    successToast(error.response.data.message, 'warning')
                })

            loaderHide()
        }
    })

    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/gallery/' + value_id)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide()
                successToast(response.data.message, 'success')
            })
            .catch(function (error) {
                funTooltipHide()
                successToast(error.response.data.message, 'warning')
            })
    }

    $('#description').summernote({
        placeholder: 'Write something...',
        height: 230,
        focus: true,
        required: true,
        callbacks: {
            // fix broken checkbox on link modal
        }
    });
    $(document).on('click', '.delete-gallery-image', function () {
        const image_id = $(this).data('id');
        const gallery_id = $(this).data('gallery-id');

        Swal.fire({
            title: "Delete Image",
            text: "Are you sure want to delete this image?",
            type: 'warning',
            showCancelButton: !0,
            confirmButtonColor: '#556ee6',
            cancelButtonColor: '#f46a6a'
        }).then(function (t) {
            if (t.value) {
                deleteGalleryImage(image_id, gallery_id);
            }
        })

    })

    function deleteGalleryImage(image_id, gallery_id) {
        $.ajax({
            type: 'GET',
            url: APP_URL + '/deleteGalleryImage' + '/' + image_id,
            success: function (data) {
                successToast(data.message, 'success');
                getGalleryImage(gallery_id);

            }, error: function (data) {
                console.log('Error:', data)
            }
        })
    }

    function getGalleryImage(gallery_id) {
        loaderView();
        $.ajax({
            type: 'POST',
            data: {gallery_id: gallery_id},
            url: APP_URL + '/getGalleryImage',
            dataType: 'html',
            success: function (data) {
                $("#images").html(data);
                loaderHide();

            }, error: function (data) {
                console.log('Error:', data)
            }
        })

    }
})

function addVideoLink() {
    axios
        .post(APP_URL + '/addVideoLink', {row: rowNo})
        .then(function (response) {
            $("#gallery").append(response.data.data)
        })
        .catch(function (error) {
            console.log(error)
        })
    rowNo = rowNo + 1;
}

function removeVideoLink(row) {
    $(".row-" + row).remove()
}



