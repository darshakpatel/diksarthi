$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    const table = $('#addrows').DataTable({
        processing: true,
        serverSide: true,
        // responsive: true,
        ajax: {
            url: APP_URL + '/user',
            type: 'GET',
            data: function (d) {
                d.village_id = $("#village_id").val()
                d.member_type = $("#member_type").val()
            }
        },
        columns: [
            {
                data: null,
                name: '',
                className: 'details-control',
                searchable: false,
                orderable: false,
                defaultContent: ''
            },
            {data: 'id', name: 'id'},
            {data: 'member_type', name: 'member_type'},
            {data: 'village', name: 'village'},
            {data: 'family_no', name: 'family_no'},
            {data: 'name', name: 'name'},
            {data: 'mobile_no', name: 'mobile_no'},
            {data: 'date', name: 'date'},
            {data: 'relation', name: 'relation'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, width: "20%"},
        ],
        drawCallback: function () {
            funTooltip();
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[0, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    });

    // $('#addrows tbody').on('click', 'td.details-control', function () {
    //     var tr = $(this).closest('tr');
    //     var $nextTD = $(this).closest('td').next();
    //     var row = table.row(tr); // variable name of the datatable
    //     if (row.child.isShown()) {
    //         row.child.hide();
    //         tr.removeClass('shown');
    //     } else {
    //         user_id=$nextTD.text();
    //         var result = '';
    //         $.get('/admin/familyUser/' + user_id, function (response) {
    //             $.each(response.data, function (i, d) { // loop through data returned from ajax
    //                 result += '' + d.name + '' + d.date_of_birth + '' + d.mobile_no;
    //             });
    //             row.child($(result)).show();	// use selector $() for result to align child rows with main table column headings
    //             tr.addClass('shown');
    //         }, 'json');
    //     }
    // });
    // Add event listener for opening and closing details
    $('#addrows tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    function format(d) {

        // `d` is the original data object for the row
        var abc = '<table class="table table-bordered">';
        $.each(d.family_member, function (index, value) {
            abc += '<tr>' +
                '<td>Name:</td>' +
                '<td>' + value.name + '</td>' +
                '<td>Date of Birth:</td>' +
                '<td>' + value.date_of_birth + '</td>' +
                '<td>Mobile No:</td>' +
                '<td>' + value.mobile_no + '</td>' +
                '<td>Relation:</td>' +
                '<td>' + value.relation.name + '</td>' +
                '</tr>';
        })
        abc += '</table>';
        return abc;
    }

    $("#filter").on('click', function () {
        table.draw()
    })
    $(document).on('click', '#bulk-option-btn', function () {
        table.draw();
    });

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data("id");

        Swal.fire({
            title: "Delete User",
            text: delete_message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#556ee6",
            cancelButtonColor: "#f46a6a"
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id);
            }
        });
    });
    $(document).on('click', '.status-change', function () {
        const value_id = $(this).data('id')
        const status = $(this).data('status')
        if (status === 'inActive') {
            Swal.fire({
                title: inactive_user,
                text: status_message,
                type: 'warning',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        } else {
            Swal.fire({
                title: active_user,
                text: status_message,
                type: 'success',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        }
    })


    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/user/' + value_id)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    function statusChangeRecord(value_id, status) {
        axios
            .get(APP_URL + '/user/status/' + value_id + '/' + status)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                loaderHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    $("#anniversary_date").datepicker({
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        language: 'en',
    })
    $("#date_of_birth").datepicker({
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        language: 'en',
        maxDate: new Date()
    })

    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault()
            loaderView()
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/user', formData)
                .then(function (response) {
                    loaderHide()
                    $form.removeClass('was-validated')
                    setTimeout(function () {
                        window.location.href = APP_URL + '/user'
                    }, 1000)
                    successToast(response.data.message, 'success')
                })
                .catch(function (error) {
                    loaderHide()
                    successToast(error.response.data.message, 'warning')
                })
        }
    })
    $('#member_type').on('change', function () {
        const type = $(this).val()
        if (type == 2) {
            $('#main_member_id').attr('required', true)
            $('.register_member').show()
            $("#relation_id").val('');
            $("#relation_id").trigger('change');
            $("#relation_id").select2({disabled: ''});
        } else {
            $("#relation_id").val('17');
            $("#relation_id").trigger('change');
            $("#relation_id").select2({disabled: 'readonly'});
            $('#main_member_id').attr('required', false)
            $('.register_member').hide()
            $("#family_no").val('')
            $("#family_no").attr('readonly', false).removeClass('bg-light')
        }
    })

    $('#main_member_id').on('change', function () {
        const family_no = $(this).find(':selected').attr('data-family-no')
        if (family_no != '') {
            $("#family_no").val(family_no)
            $("#family_no").attr('readonly', true).addClass('bg-light')
        } else {
            $("#family_no").val('')
            $("#family_no").attr('readonly', false).removeClass('bg-light')
        }

    })
    $("#export").on('click', function () {
        const village_id = $("#village_id").val()
        const member_type = $("#member_type").val()
        window.location.href = APP_URL + '/exportUser?village_id=' + village_id + '&member_type=' + member_type
    })
});



