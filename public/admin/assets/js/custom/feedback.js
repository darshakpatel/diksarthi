$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })

    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/feedback',
            type: 'GET',
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'user', name: 'user'},
            {data: 'subject', name: 'subject'},
            {data: 'description', name: 'description'},
            {data: 'image', name: 'image'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        drawCallback: function () {
            funDataTableCheck('datable_check')
            funDataTableUnCheck('datable_check')
            funTooltip()
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[1, 'ASC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    })

    $(document).on('click', '#bulk-option-btn', function () {
        table.draw()
    })

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data('id')
        Swal.fire({
            title: 'Delete Feedback',
            text: 'Are you sure you want to delete',
            type: 'warning',
            showCancelButton: !0,
            confirmButtonColor: '#556ee6',
            cancelButtonColor: '#f46a6a'
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id)
            }
        })
    })


    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/feedback/' + value_id)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide()
                successToast(response.data.message, 'success')
            })
            .catch(function (error) {
                funTooltipHide()
                successToast(error.response.data.message, 'warning')
            })
    }
})



