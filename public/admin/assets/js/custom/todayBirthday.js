$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/todayBirthday',
            type: 'GET',
            data: function (d) {
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'village', name: 'villages.name'},
            {data: 'family_no', name: 'family_no'},
            {data: 'name', name: 'name'},
            {data: 'mobile_no', name: 'mobile_no'},
        ],
        drawCallback: function () {
            funTooltip();
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[0, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    });
});



