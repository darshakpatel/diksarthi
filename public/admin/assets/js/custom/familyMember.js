$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    const table = $('#addrows').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: APP_URL + '/familyMemberList',
            type: 'GET',
            data: function (d) {
                d.main_member_id = $("#main_member_id").val()
            }
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'village', name: 'village'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'mobile_no', name: 'mobile_no'},
            {data: 'date', name: 'date'},
            {data: 'relation', name: 'relation'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false, width: "20%"},
        ],
        drawCallback: function () {
            funTooltip();
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[0, 'DESC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    });

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data("id");

        Swal.fire({
            title: "Delete Family Member",
            text: delete_message,
            type: "warning",
            showCancelButton: !0,
            confirmButtonColor: "#556ee6",
            cancelButtonColor: "#f46a6a"
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id);
            }
        });
    });
    $(document).on('click', '.status-change', function () {
        const value_id = $(this).data('id')
        const status = $(this).data('status')
        if (status === 'inActive') {
            Swal.fire({
                title: inactive_user,
                text: status_message,
                type: 'warning',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        } else {
            Swal.fire({
                title: active_user,
                text: status_message,
                type: 'success',
                showCancelButton: !0,
                confirmButtonColor: '#556ee6',
                cancelButtonColor: '#f46a6a'
            }).then(function (t) {
                if (t.value) {
                    statusChangeRecord(value_id, status)
                }
            })
        }
    })


    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/user/' + value_id)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    function statusChangeRecord(value_id, status) {
        axios
            .get(APP_URL + '/user/status/' + value_id + '/' + status)
            .then(function (response) {
                funDataTableUnCheck('datable_check')
                table.draw()
                loaderHide();
                funTooltipHide();
                successToast(response.data.message, 'success');
            })
            .catch(function (error) {
                funTooltipHide();
                loaderHide();
                successToast(error.response.data.message, 'warning')
            });
    }

    $("#anniversary_date").datepicker({
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        language: 'en',
    })
    $("#date_of_birth").datepicker({
        dateFormat: 'yyyy-mm-dd',
        autoClose: true,
        language: 'en',
        maxDate: new Date()
    })

    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault()
            loaderView()
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/user', formData)
                .then(function (response) {
                    $form.removeClass('was-validated')
                    setTimeout(function () {
                        window.location.href = APP_URL + '/user'
                    }, 1000)
                    successToast(response.data.message, 'success')
                })
                .catch(function (error) {
                    successToast(error.response.data.message, 'warning')
                })

            loaderHide()
        }
    })
});



