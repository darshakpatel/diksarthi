$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            loaderView();
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/notification', formData)
                .then(function (response) {
                    if ($("#form-method").val() === 'add') {
                        $form[0].reset();
                        $form.removeClass('was-validated');
                    }
                    window.location.href = APP_URL + '/dashboard'
                    successToast(response.data.message, 'success');
                })
                .catch(function (error) {
                    successToast(error.response.data.message, 'warning')
                });

            loaderHide();
        }
    })
    $(document).on('click', '.reset', function () {
        $("#addEditForm")[0].reset();
    });
})
