$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    let $form = $("#addEditForm");
    $form.on("submit", function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            let formData = new FormData($("#addEditForm")[0]);
            $.ajax({
                url: APP_URL + '/updateSetting',
                type: 'POST',
                dataType: 'json',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    successToast(data.message, 'success');
                    window.location.href = APP_URL + '/setting'
                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        }
    });


});
