$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })


    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault()
            loaderView()
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/updateProfile', formData)
                .then(function (response) {
                    $form.removeClass('was-validated')
                    setTimeout(function () {
                        window.location.href = APP_URL + '/profile'
                    }, 1000)
                    successToast(response.data.message, 'success')
                })
                .catch(function (error) {
                    successToast(error.response.data.message, 'warning')
                })

            loaderHide()
        }
    })
})



