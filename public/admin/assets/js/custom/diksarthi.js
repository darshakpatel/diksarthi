$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    const table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: APP_URL + '/diksarthi',
            type: 'GET',
        },
        columns: [
            {data: 'id', name: 'id'},
            {data: 'title', name: 'title'},
            {data: 'date', name: 'date'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ],
        drawCallback: function () {
            funDataTableCheck('datable_check')
            funDataTableUnCheck('datable_check')
            funTooltip()
        },
        language: {
            processing: '<div class="spinner-border text-primary m-1" role="status"><span class="sr-only">Loading...</span></div>'
        },
        order: [[1, 'ASC']],
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']]
    })

    $(document).on('click', '#bulk-option-btn', function () {
        table.draw()
    })

    $(document).on('click', '.delete-single', function () {
        const value_id = $(this).data('id')
        Swal.fire({
            title: 'Delete Diksarthi',
            text: 'Are you sure you want to delete',
            type: 'warning',
            showCancelButton: !0,
            confirmButtonColor: '#556ee6',
            cancelButtonColor: '#f46a6a'
        }).then(function (t) {
            if (t.value) {
                deleteRecord(value_id)
            }
        })
    })
    let $form = $('#addEditForm')
    $form.on('submit', function (e) {
        if (!e.isDefaultPrevented()) {
            e.preventDefault()
            loaderView()
            let formData = new FormData($form[0])
            axios
                .post(APP_URL + '/diksarthi', formData)
                .then(function (response) {
                    loaderHide()
                    $form.removeClass('was-validated')
                    setTimeout(function () {
                        window.location.href = APP_URL + '/diksarthi'
                    }, 1000)
                    successToast(response.data.message, 'success')
                })
                .catch(function (error) {
                    loaderHide()
                    successToast(error.response.data.message, 'warning')
                })
        }
    })
    $('#description').summernote({
        placeholder: 'Write something...',
        height: 230,
        focus: true,
        required: true,
        callbacks: {
            // fix broken checkbox on link modal
        }
    });

    function deleteRecord(value_id) {
        axios
            .delete(APP_URL + '/diksarthi/' + value_id)
            .then(function (response) {
                table.draw()
                loaderHide()
                successToast(response.data.message, 'success')
            })
            .catch(function (error) {
                funTooltipHide()
                successToast(error.response.data.message, 'warning')
            })
    }
})



