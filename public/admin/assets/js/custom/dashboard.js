$('.shawCalRanges').daterangepicker({
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
});


//getTodayGraphData();

function getTodayGraphData() {
    loaderView();
    $("#today-bar-chart").html("");
    var request = $.ajax({
        url: site_url + "admin/getTodayGraphData",
        method: "POST",
        dataType: "json"
    });
    request.done(function (data, textStatus, jqXHR) {
        loaderHide();
        //console.log(data);
        ShowTodayGraph(data);
    });
    request.fail(function (jqXHR, textStatus) {
        loaderHide();
        console.log("Request failed: " + textStatus);
        // errorMessage("Request failed: " + textStatus);
    });
}

function ShowTodayGraph(data) {
    new Morris.Bar({
        element: 'today-bar-chart',
        data: data,
        xkey: 'date',
        ykeys: ['Pending', 'Confirmed', 'Delivered', 'Rejected', 'Canceled'],
        labels: ['Pending', 'Confirmed', 'Delivered', 'Rejected', 'Canceled'],
        barColors: ['#FFB22B', '#745AF2', '#6C757D', '#06D79C', '#EF5350'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
    });

}

function getOrderGraphData() {
    var year = $("#order_year").val();
    loaderView();
    $("#order-overview").html("");
    var request = $.ajax({
        url: site_url + "admin/getOrderGraphData",
        method: "POST",
        data: {year: year},
        dataType: "json"
    });
    request.done(function (data, textStatus, jqXHR) {
        loaderHide();
        //console.log(data);
        showOrderGraph(data);
    });
    request.fail(function (jqXHR, textStatus) {
        loaderHide();
        console.log("Request failed: " + textStatus);
        // errorMessage("Request failed: " + textStatus);
    });
}

function showOrderGraph(data) {
    new Morris.Bar({
        element: "order-overview",
        data: data,
        xkey: "y",
        ykeys: ["a"],
        labels: ["Total Order"],
        barColors: ['#06D79C'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
    });
}

function getRegisterGraphData() {
    var year = $("#merchant_user_year").val();
    loaderView();
    $("#register-overview").html("");
    var request = $.ajax({
        url: site_url + "admin/getRegisterGraphData",
        method: "POST",
        data: {year: year},
        dataType: "json"
    });
    request.done(function (data, textStatus, jqXHR) {
        loaderHide();
        //console.log(data);
        showRegisterGraph(data);
    });
    request.fail(function (jqXHR, textStatus) {
        loaderHide();
        console.log("Request failed: " + textStatus);
        // errorMessage("Request failed: " + textStatus);
    });
}

function showRegisterGraph(data) {
    new Morris.Bar({
        element: "register-overview",
        data: data,
        xkey: "y",
        ykeys: ["merchant", "user"],
        labels: ["Merchant", 'User'],
        barColors: ['#745AF2', '#398BF7'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
    });
}

function getAverageData() {
    var average_date = $("#average_date").val();
    loaderView();

    var request = $.ajax({
        url: site_url + "admin/getAverageData",
        method: "POST",
        data: {average_date: average_date},
        dataType: "json"
    });
    request.done(function (data, textStatus, jqXHR) {
        loaderHide();
        $("#average_income").html(data.average_income);
        $("#average_order").html(data.average_order);
        $("#average_merchant").html(data.average_merchant);
        $("#average_user").html(data.average_user);
    });
    request.fail(function (jqXHR, textStatus) {
        loaderHide();
        console.log("Request failed: " + textStatus);
        //errorMessage("Request failed: " + textStatus);
    });
}


$(function () {
    getTodayGraphData();
    getOrderGraphData();
    getDashboardData();
    getAverageData();
    getNotificationData();
    getRegisterGraphData();
});

function reloadDashBoardData() {
    getTodayGraphData();
    getOrderGraphData();
    getDashboardData();
    getAverageData();
    getNotificationData();
    getRegisterGraphData();
}

function viewOrderDetails(id) {
    loaderView();

    var request = $.ajax({
        url: site_url + "order/getOrderDetails",
        method: "POST",
        data: {id: id},
        dataType: "json"
    });
    request.done(function (data, textStatus, jqXHR) {
        $("#orderDetailsModal").modal("show");
        loaderHide();
        $("#myModalLabel").html(data.modal_title);
        $("#modalContent").html(data.modal_content);
        $("#delivery_boy_id").select2();
    });
    request.fail(function (jqXHR, textStatus) {
        console.log("Request failed: " + textStatus);
        errorMessage("Request failed: " + textStatus);
        loaderHide();
    });
}


function reloadDashBoardData() {
    getTodayGraphData();
    getOrderGraphData();
    getDashboardData();
    getAverageData();
    getNotificationData();
}

setInterval(function () {
    getDashboardData();
    getNotificationData();
}, 120000);

