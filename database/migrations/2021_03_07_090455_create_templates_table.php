<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('height');
            $table->string('width');
            $table->string('zip');
            $table->unsignedTinyInteger('is_hot');
            $table->unsignedTinyInteger('is_premium');
            $table->string('video');
            $table->string('video_webm');
            $table->string('video_url');
            $table->string('video_url_webm');
            $table->string('download_url');
            $table->string('watermark_image');
            $table->string('thumb_url');
            $table->string('watermark_position');
            $table->unsignedTinyInteger('is_new');
            $table->unsignedTinyInteger('has_text');
            $table->unsignedBigInteger('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates');
    }
}
