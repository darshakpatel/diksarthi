<?php

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create([
            'name'          => 'English',
            'language_code' => 'en',
            'is_rtl'        => 0,
        ]);
    }
}
