<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Master Panel Language Lines
    |--------------------------------------------------------------------------
    */

    'admin.sidebar.dashboard' => 'Dashboard',
    'admin.sidebar.users' => 'Users',
    'admin.sidebar.categories' => 'Categories',
    'admin.sidebar.add_category' => 'Add Category',
    'admin.sidebar.categories_list' => 'Categories',
    'admin.sidebar.vendors' => 'Vendors',
    'admin.sidebar.add_vendor' => 'Add Vendor',
    'admin.sidebar.vendor_list' => 'Vendors',
    'admin.sidebar.sliders' => 'Sliders',
    'admin.sidebar.add_slider' => 'Add Slider',
    'admin.sidebar.add_option' => 'Add Option',
    'admin.sidebar.option' => 'Options',
    'admin.sidebar.slider_list' => 'Sliders',
    'admin.sidebar.pages' => 'Pages',
    'admin.sidebar.send_notification' => 'Send Notification',
    'admin.sidebar.orders' => 'Orders',
    'admin.sidebar.items' => 'Items',
    'admin.sidebar.business_request' => 'Business Request',
    'admin.sidebar.shops' => 'Shops',
    'admin.sidebar.setting' => 'Settings',
    'admin.sidebar.drivers' => 'Drivers',

    /*
       |--------------------------------------------------------------------------
       | Master Panel Common Lines
       |--------------------------------------------------------------------------
    */

    'id' => 'ID',
    'name' => 'Name',
    'login' => 'Login',
    'remember_me' => 'Remember me',
    'submit' => 'Submit',
    'cancel' => 'Cancel',
    'apply' => 'Apply',
    'status' => 'Status',
    'pending' => 'Pending',
    'completed' => 'Completed',
    'cancelled' => 'Cancelled',
    'rejected' => 'Rejected',
    'in_progress' => 'In Progress',
    'on_way' => 'On Way',
    'approve' => 'Approve',
    'reject' => 'Reject',
    'action' => 'Action',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'details' => 'Details',
    'save' => 'Save',
    'active' => 'Active',
    'inactive' => 'InActive',
    'email' => 'Email',
    'mobile_no' => 'Mobile No',
    'password' => 'Password',
    'address' => 'Address',
    'date' => 'Date',
    'close' => 'Close',
    'image' => 'Image',
    'price' => 'Price',
    'view' => 'View',
    'add' => 'Add',
    'remove' => 'Remove',
    'shop' => 'Shop',
    'description' => 'Description',
    'country_code' => 'Country Code',
    'status_changed' => 'Status Changed Successfully',
    'status_message' => 'Are you sure want to change status?',
    'delete_message' => 'Are you sure want to delete this record?',
    'name_is_taken' => 'This name is already exists',
    'business_name' => 'Business Name',
    'reject_reason' => 'Reject Reason',
    'cancel_reason' => 'Cancel Reason',
    'select_action' => 'Please Select Action',
    'select_record' => 'Please Select Record',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Category Lines
    |--------------------------------------------------------------------------
    */

    'admin.category.add_category' => 'Add Category',
    'admin.category.category' => 'Categories',
    'admin.category.edit_category' => 'Edit Category',
    'admin.category.active_category' => 'Active Category?',
    'admin.category.inactive_category' => 'Deactivate Category?',
    'admin.category.destroy_category' => 'Destroy Category?',
    'admin.category.image' => 'Image',
    'admin.category.category_added' => 'Category Added Successfully',
    'admin.category.category_updated' => 'Category Updated Successfully',
    'admin.category.category_deleted' => 'Category Successfully Deleted',


    /*
        |--------------------------------------------------------------------------
        | Master Panel Option Lines
        |--------------------------------------------------------------------------
    */

    'admin.option.add_option' => 'Add Option',
    'admin.option.option' => 'Options',
    'admin.option.edit_option' => 'Edit Option',
    'admin.option.destroy_option' => 'Destroy Option?',
    'admin.option.option_added' => 'Option Added Successfully',
    'admin.option.option_updated' => 'Option Updated Successfully',
    'admin.option.option_deleted' => 'Option Successfully Deleted',
    'admin.option.add_term' => 'Terms',


    'admin.option.add_option_value' => 'Add :name Option Value',
    'admin.option.option_value' => 'Option Values',
    'admin.option.edit_option_value' => 'Edit :name Option Value',
    'admin.option.destroy_option_value' => 'Destroy Option Value?',
    'admin.option.option_value_added' => 'Option Value Added Successfully',
    'admin.option.option_value_updated' => 'Option Value Updated Successfully',
    'admin.option.option_value_deleted' => 'Option Value Successfully Deleted',
    /*
    |--------------------------------------------------------------------------
    | Master Panel User Lines
    |--------------------------------------------------------------------------
    */

    'admin.user.user' => 'Users',
    'admin.user.profile' => 'Profile',
    'admin.user.active_user' => 'Active User?',
    'admin.user.destroy_user' => 'Destroy User?',
    'admin.user.inactive_user' => 'Deactivate User?',
    'admin.user.password_changed' => 'Password Updated Successfully',
    'admin.user.old_password_wrong' => 'Old Password is incorrect',
    'admin.user.profile_updated' => 'Profile Updated Successfully',
    'admin.user.user_deleted' => 'User Successfully Deleted',
    'admin.user.email_is_exists' => 'This email is already exists',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Item Lines
    |--------------------------------------------------------------------------
    */

    'admin.item.item' => 'Items',
    'admin.item.item_attribute' => 'Item',
    'admin.item.add_item' => 'Add Item',
    'admin.item.edit_item' => 'Edit Item',
    'admin.item.is_customization' => 'Is Customization',
    'admin.item.is_multiple' => 'Is Multiple',
    'admin.item.item_deleted' => 'Item Deleted Successfully',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Vendor Lines
    |--------------------------------------------------------------------------
    */

    'admin.vendor.vendor' => 'Vendors',
    'admin.vendor.pending_vendor' => 'Pending Vendors',
    'admin.vendor.approved_vendor' => 'Approved Vendors',
    'admin.vendor.rejected_vendor' => 'Rejected Vendors',
    'admin.vendor.commission' => 'Commission',
    'admin.vendor.order' => 'Orders',
    'admin.vendor.category' => 'Category',
    'admin.vendor.add_vendor' => 'Add Vendor',
    'admin.vendor.edit_vendor' => 'Edit Vendor',
    'admin.vendor.shop_details' => 'Shop Details',
    'admin.vendor.item_details' => 'Item List',
    'admin.vendor.shop_name' => 'Shop Name',
    'admin.vendor.commission_percentage' => 'Commission Percentage',
    'admin.vendor.vendor_details' => 'Vendor Details',
    'admin.vendor.header_image' => 'Header Image',
    'admin.vendor.tag_line' => 'Tag Line',
    'admin.vendor.shop_logo' => 'Shop Logo',
    'admin.vendor.order_list' => 'Order List',

    'admin.vendor.active_vendor' => 'Active Vendor?',
    'admin.vendor.destroy_vendor' => 'Destroy Vendor?',
    'admin.vendor.inactive_vendor' => 'Deactivate Vendor?',
    'admin.vendor.vendor_added' => 'Vendor Added Successfully',
    'admin.vendor.vendor_updated' => 'Vendor Updated Successfully',
    'admin.vendor.vendor_deleted' => 'Vendor Deleted Successfully',
    'admin.vendor.vendor_successfully_approved' => 'Vendor Successfully Approved',
    'admin.vendor.vendor_successfully_rejected' => 'Vendor Successfully Rejected',


    /*
    |--------------------------------------------------------------------------
    | Master Panel Slider Lines
    |--------------------------------------------------------------------------
    */

    'admin.slider.slider' => 'Sliders',
    'admin.slider.add_slider' => 'Add Slider',
    'admin.slider.edit_slider' => 'Edit Slider',
    'admin.slider.no_shop_found' => 'No Shop Found',
    'admin.slider.destroy_slider' => 'Destroy Slider?',
    'admin.slider.slider_added' => 'Slider Added Successfully',
    'admin.slider.slider_updated' => 'Slider Updated Successfully',
    'admin.slider.slider_deleted' => 'Slider Deleted Successfully',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Page Lines
    |--------------------------------------------------------------------------
    */

    'admin.page.page' => 'Pages',
    'admin.page.add_page' => 'Add Page',
    'admin.page.edit_page' => 'Edit Page',

    'admin.page.page_added' => 'Page Added Successfully',
    'admin.page.page_updated' => 'Page Updated Successfully',
    'admin.page.page_deleted' => 'Page Deleted Successfully',

    /*
   |--------------------------------------------------------------------------
   | Master Panel Orders Lines
   |--------------------------------------------------------------------------
   */

    'admin.orders.orders' => 'Orders',
    'admin.orders.user' => 'User',
    'admin.orders.total' => 'Total',
    'admin.orders.transaction_id' => 'Transaction ID',
    'admin.orders.shop' => 'Shop',
    'admin.orders.destroy_orders' => 'Destroy Orders?',
    'admin.orders.orders_deleted' => 'Order Deleted Successfully',
    'admin.orders.payment_method' => 'Payment Method',

    /*
   |--------------------------------------------------------------------------
   | Master Panel Business Lines
   |--------------------------------------------------------------------------
   */

    'admin.business.business_request' => 'Business Request',
    'admin.business.name' => 'Businesses',
    'admin.business.destroy_business_request' => 'Destroy Business Request?',
    'admin.business.business_request_deleted' => 'Business Request Deleted Successfully',

    /*
   |--------------------------------------------------------------------------
   | Master Panel Shop Lines
   |--------------------------------------------------------------------------
   */

    'admin.shop.shop' => 'Shop',
    'admin.shop.destroy_shop' => 'Destroy Shop?',
    'admin.shop.shop_deleted' => 'Shop Deleted Successfully',
    'admin.shop.shop_details' => 'Shop Details',
    'admin.shop.vendor_details' => 'Vendor Details',
    'admin.shop.tag_line' => 'Tag Line',
    'admin.shop.location' => 'Location',
    'admin.shop.image' => 'Image',
    'admin.shop.commission_percentage' => 'Commission Percentage',
    'admin.shop.logo' => 'Logo',


    /*
   |--------------------------------------------------------------------------
   | Master Panel Setting Lines
   |--------------------------------------------------------------------------
   */

    'admin.setting.commission' => 'Commission',
    'admin.setting.smtp_host' => 'SMTP Host',
    'admin.setting.smtp_username' => 'SMTP Username',
    'admin.setting.smtp_password' => 'SMTP Password',
    'admin.setting.smtp_encryption' => 'SMTP Encryption',
    'admin.setting.smtp_port' => 'SMTP Port',
    'admin.setting.smtp_from_email' => 'SMTP From Mail',
    'admin.setting.tax_name' => 'Tax Name',
    'admin.setting.tax_percentage' => 'Tax Percentage',
    'admin.setting.currency' => 'Currency',
    'admin.setting.currency_placement' => 'Currency Placement',
    'admin.setting.after' => 'After',
    'admin.setting.before' => 'Before',

    /*
   |--------------------------------------------------------------------------
   | Reset Password Lines
   |--------------------------------------------------------------------------
   */
    'reset' => 'Reset Password',
    'confirm_password' => 'Confirm Password',
    'email_not_found' => 'Email not found',
    'title_password_reset' => 'Reset your Password',
    'reset_password_subject' => 'Reset your Password',
    'please_check_your_mail' => 'Email successfully sent please check your mail',
    'reset_password_body' => 'Lost your keys ??? Someone, hopefully you, requested a password reset for this account. Just click the below button to get started.',

    /*
    |--------------------------------------------------------------------------
    | Master Panel Notification Lines
    |--------------------------------------------------------------------------
    */


    'admin.notification.send_notification' => 'Send Notification',
    'admin.notification.title' => 'Title',
    'admin.notification.message' => 'Message',

    /*
    |--------------------------------------------------------------------------
    | Vendor Panel Item Lines
    |--------------------------------------------------------------------------
    */
    'vendor.dashboard.orders' => 'Orders',
    'vendor.dashboard.today_orders' => 'Today Orders',
    'vendor.dashboard.earning' => 'Earning',
    'vendor.item.item' => 'Items',
    'vendor.item.add' => 'Add',
    'vendor.item.category' => 'Category',
    'vendor.item.please_category' => 'Please select Category',
    'vendor.item.please_select_attribute_term' => 'Please Select Option Value',
    'vendor.item.item_attribute' => 'Item',
    'vendor.item.add_item' => 'Add Item',
    'vendor.item.create_variation' => 'Create Variation',
    'vendor.item.options' => 'Options',
    'vendor.item.add_variation' => 'Add Variation',
    'vendor.item.attribute' => 'Option',
    'vendor.item.edit_item' => 'Edit Item',
    'vendor.item.variable_product' => 'Variable Product',
    'vendor.item.yes' => 'Yes',
    'vendor.item.no' => 'No',
    'vendor.item.is_multiple' => 'Is Multiple?',
    'vendor.item.remove' => 'Remove',
    'vendor.item.min' => 'Min',
    'vendor.item.max' => 'Max',
    'vendor.item.destroy_item' => 'Destroy Item?',
    'vendor.item.add_customization' => 'Add Customization',
    'vendor.item.item_added' => 'Item Added Successfully',
    'vendor.item.item_updated' => 'Item Updated Successfully',
    'vendor.item.item_deleted' => 'Item Deleted Successfully',
    'vendor.item.please_create_variation' => 'Please create variation',
    'vendor.item.variation_delete_success' => 'Variation delete successfully',
    'vendor.item.option_delete_success' => 'Option delete successfully',
    'vendor.item.delete_option' => 'Delete Option?',
    'vendor.item.delete_variation' => 'Delete Variation?',
    'vendor.item.delete_variation_message' => 'Are you sure? want to delete this Variation?',
    'vendor.item.delete_option_message' => 'Are you sure? want to delete this option?',
    'vendor.item.destroy_item_image' => 'Destroy Item Image?',
    'vendor.item.confirm_message' => 'Are you sure? want to delete this image?',
    'vendor.item.image_deleted' => 'Image Deleted Successfully',

    /*
   |--------------------------------------------------------------------------
   | Vendor Panel Orders Lines
   |--------------------------------------------------------------------------
   */

    'vendor.orders.destroy_orders' => 'Destroy Orders?',
    'vendor.orders.orders_deleted' => 'Order Deleted Successfully',

    'vendor.sidebar.orders' => 'Orders',
    'vendor.orders.orders' => 'Orders',
    'vendor.orders.user' => 'User',
    'vendor.orders.total' => 'Total',
    'vendor.orders.transaction_id' => 'Transaction ID',
    'vendor.orders.payment_method' => 'Payment Method',
    'vendor.orders.tax' => 'Tax',
    'vendor.orders.total_tax' => 'Total Tax',
    'vendor.orders.quantity' => 'Quantity',
    'vendor.orders.order_details' => 'Order Details',
    'vendor.orders.sub_total' => 'Sub Total',
    'vendor.orders.view_on_map' => 'View On Map',
    'vendor.orders.driver_assign' => 'Driver Assign',
    'vendor.orders.assign' => 'Assign',
    'vendor.orders.driver_assigned' => 'Driver Assigned',
    /*
    |--------------------------------------------------------------------------
    | Vendor Panel Shop Lines
    |--------------------------------------------------------------------------
    */
    'vendor.shop.profile' => 'Shop Profile',
    'vendor.shop.profile_updated' => 'Shop Profile Updated Successfully',

    /*
    |--------------------------------------------------------------------------
    | Common Message Api Lines
    |--------------------------------------------------------------------------
   */

    'api.error' => 'Something Went wrong.Please try again after sometime.',

    /*
    |--------------------------------------------------------------------------
    | Api Message Lines
    |--------------------------------------------------------------------------
    */

    'api.login.account_in_activated' => 'Your account is deactivated by admin. Please contact admin.',
    'api.login.invalid_email_or_password' => 'Invalid email & password.',
    'api.user.no_user_found' => 'User Not found.',
    'api.user.profile_updated' => 'Profile update successfully.',
    'api.user.profile_image_updated' => 'Profile image update successfully.',
    'api.user.password_updated' => 'Password update successfully.',
    'api.user.old_password_is_wrong' => 'You have enter old password is wrong.',

    'api.cart.more_than_one_shop' => 'Replace cart item? \n  Your cart contains item from :old . Do you want to discard the selection and add item from :new?.',
    'api.cart.shop_is_not_available' => 'Sorry this shop is not available.',
    'api.cart.cart_item_deleted' => 'Your cart item deleted successfully.',
    'api.cart.cart_empty' => 'Your cart is empty.',

    'api.cart.cart_update' => 'Card is updated.',
    'api.cart.cart_item_added' => 'Item added to your cart.',
    'api.cart.item_not_available' => 'This item is not available.',

    'api.register.register_success' => 'Invalid email & password.',

    'api.category.category_not_found' => 'Category not found.',
    'api.vendor.order_not_found' => 'Order not found.',
    'api.option.option_not_found' => 'Option not found.',
    'api.option_value.option_value_not_found' => 'Option Value not found.',

    'api.country.country_not_found' => 'Country not found.',

    'api.item.items_not_found' => 'Item not found.',

    'api.order.order_placed' => 'Order Place successfully.',

    'api.address.address_empty' => 'You have no any address added.',
    'api.address.address_save' => 'Address added successfully.',
    'api.address.address_update' => 'Address updated successfully.',
    'api.address.address_deleted' => 'Address deleted successfully.',

    'api.contactUs.thank_you_for_contact_us' => 'Thank you for contact us we contact very soon.',

    'api.business.thank_you_for_add_business_request' => 'Thank you for add your business we contact very soon.',

    'api.vendor.account_is_under_approval' => 'Your account is under approval',
    'api.vendor.register_success' => 'Registered Successfully',
    'api.favourite.item_favourite_added' => 'Item added to your favourite.',
    'api.favourite.favourite_deleted' => 'Removed from your favourite.',
    /*
   |--------------------------------------------------------------------------
   | Api Driver Message Lines
   |--------------------------------------------------------------------------
   */
    'api.driver.register_success' => 'Registered Successfully',
    'api.driver.account_is_under_approval' => 'Your account is under approval',
    'messages.api.login.invalid_email_or_password' => 'Invalid your email or password',
    'messages.api.driver.account_is_under_approval' => 'Your account is under approval',
    'messages.api.login.account_in_activated' => 'Your account was In activated',
    'api.driver.rejected' => 'Your Account was Rejected',
    'api.item.item_not_found' => 'Item not found',
    'api.notification.notification_not_found' => 'Notification not found',

    /*
   |--------------------------------------------------------------------------
   | Driver Panel Item Lines
   |--------------------------------------------------------------------------
   */
    'admin.driver.pending_driver' => 'Pending Drivers',
    'admin.driver.approved_driver' => 'Approved Drivers',
    'admin.driver.rejected_driver' => 'Rejected Drivers',
    'admin.sidebar.add_driver' => 'Add Driver',
    'admin.driver.add_driver' => 'Add Driver',
    'admin.driver.driver_details' => 'Driver Details',
    'admin.driver.shop_details' => 'Shop Details',
    'admin.driver.driver_deleted' => 'Delete Successfully',
    'admin.driver.driver_successfully_approved' => 'Driver Successfully Approved',
    'admin.driver.driver_successfully_rejected' => 'Driver Successfully Rejected',
    'admin.driver.active_driver' => 'Active Driver?',
    'admin.driver.destroy_driver' => 'Destroy Driver?',
    'admin.driver.inactive_driver' => 'InActive Driver?',
    'admin.driver.approvedDriver_details' => 'Approved Driver Details',
    'admin.driver.edit_driver' => 'Edit Driver',
    'admin.driver.driver_updated' => 'Driver are successfully updated',
    'admin.driver.driver_added' => 'Driver are successfully added',
    'driverDetails' => 'View driver Details',
    'api.driver.password_updated' => 'Password Successfully Updated',
    'api.driver.old_password_is_wrong' => 'Your Old Password is wrong!',
    'api.driver.no_driver_found' => 'Sorry No Driver Found!',
];
