@extends('admin.layouts.vertical', ['title' =>'Send Notification'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Send Notification</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="">
                            <input type="hidden" id="form-method" value="edit_value">
                            @csrf

                            <div class="form-group mb-3">
                                <label>Member Type</label>
                                <select name="member_type" class="form-control select2" id="member_type" required>
                                    <option value="">Select Type</option>
                                    <option value="0">All Member</option>
                                    <option value="1">Registered Member</option>
                                    <option value="2">Family Member</option>
                                </select>
                            </div>
                            <div class="form-group mb-3">
                                <label for="title">Title</label>
                                <input type="text" class="form-control"
                                       name="title" id="title"
                                       placeholder="Title" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="message">Message</label>
                                <textarea class="form-control"
                                          name="message"
                                          placeholder="Message"
                                          required></textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.save')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.dashboard')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/sendNotification.js') }}?v={{ time() }}"></script>
@endsection
