@extends('admin.layouts.vertical', ['title' =>"Today Anniversary"])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        @if(!$is_sent)
                            <a href="{{route('admin.todayAnniversarySend')}}" class="btn btn-primary">Send SMS</a>
                        @else
                            <h4 class="text-danger">Today Messages Already Sent</h4>
                        @endif
                    </div>
                    <h4 class="page-title">Today Anniversary</h4>
                </div>
            </div>
        </div>
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>ID</th>
                                            <th>Village</th>
                                            <th>Family ID</th>
                                            <th>Name</th>
                                            <th>Mobile No</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>ID</th>
                                            <th>Village</th>
                                            <th>Family ID</th>
                                            <th>Name</th>
                                            <th>Mobile No</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/todayAnniversary.js') }}?v={{ time() }}"></script>
@endsection
