@extends('admin.layouts.vertical', ['title' => 'Family Members'])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Family Members of {{$mainMember->family_no}}</h4>
                    <input type="hidden" value="{{$mainMember->id}}" id="main_member_id">
                </div>
            </div>
        </div>

        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="addrows">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>Village</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.email')}}</th>
                                            <th>{{trans('messages.mobile_no')}}</th>
                                            <th>Birth Date</th>
                                            <th>Relation</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>Village</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.email')}}</th>
                                            <th>{{trans('messages.mobile_no')}}</th>
                                            <th>Birth Date</th>
                                            <th>Relation</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const destroy_user = '{{trans('messages.admin.user.destroy_user')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
        const status_message = '{{trans('messages.status_message')}}'
        const active_user = '{{trans('messages.admin.user.active_user')}}'
        const inactive_user = '{{trans('messages.admin.user.inactive_user')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/familyMember.js') }}?v={{ time() }}"></script>

@endsection
