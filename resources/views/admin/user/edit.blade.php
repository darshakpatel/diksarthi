@extends('admin.layouts.vertical', ['title' =>"Update Member"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Update Member</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$user->id}}">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group mb-3">
                                    <label>Member Type</label>
                                    <select name="member_type" class="form-control select2" id="member_type" required>
                                        <option value="">Select Type</option>
                                        <option value="1" @if($user->member_type==1) selected @endif>Registered Member
                                        </option>
                                        <option value="2" @if($user->member_type==2) selected @endif>Family Member
                                        </option>
                                    </select>
                                </div>
                                <div class="col-12 form-group mb-3 register_member"
                                     @if($user->member_type==1) style="display: none" @endif>
                                    <label>Select Main Member</label>
                                    <select name="main_member_id" class="form-control select2" id="main_member_id"
                                            @if($user->member_type==2) required @endif>
                                        <option value="">Select Member</option>
                                        @foreach($register_members as $register_member)
                                            <option value="{{$register_member->id}}"
                                                    @if($user->main_member_id==$register_member->id) selected @endif
                                                    data-family-no="{{$register_member->family_no}}">{{$register_member->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" name="name"
                                           value="{{$user->name}}"
                                           placeholder="Name" required>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Village</label>
                                    <select name="village_id" id="village_id" class="form-control">
                                        <option value="">Select Village</option>
                                        @foreach($villages as $village)
                                            <option value="{{$village->id}}"
                                                    @if($user->village_id==$village->id) selected @endif>{{$village->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                                <div class="col-6 form-group mb-3">
                                    <label>Family No</label>
                                    <input type="text" class="form-control @if($user->member_type==2) bg-light @endif"
                                           name="family_no" id="family_no"
                                           value="{{$user->family_no}}"
                                           placeholder="Family No" @if($user->member_type==2) readonly @endif required>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Person No</label>
                                    <input type="text" class="form-control" name="per_no"
                                           value="{{$user->per_no}}"
                                           placeholder="Person No" required>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Relation</label>
                                    <select name="relation_id" id="relation_id" class="form-control">
                                        <option value="">Select Relation</option>
                                        @foreach($relations as $relation)
                                            <option value="{{$relation->id}}"
                                                    @if($user->relation_id==$relation->id) selected @endif>{{$relation->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Petron or Life</label>
                                    <select name="petron" id="petron" class="form-control">
                                        <option value="">Select Type</option>
                                        <option value="P" @if($user->petron=="P") selected @endif>Petron</option>
                                        <option value="L" @if($user->petron=="L") selected @endif>Life</option>
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Piyar Paksh</label>
                                    <input type="text" class="form-control" name="piyar_paksh"
                                           value="{{$user->piyar_paksh}}"
                                           placeholder="Piyar Paksh">
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Anniversary Date</label>
                                    <input type="text" id="anniversary_date" class="form-control"
                                           name="anniversary_date"
                                           value="{{$user->anniversary_date}}"
                                           placeholder="Anniversary Date">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Birth Date</label>
                                    <input type="text" id="date_of_birth" class="form-control"
                                           name="date_of_birth"
                                           value="{{$user->date_of_birth}}"
                                           placeholder="Birth Date">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Education</label>
                                    <input type="text" class="form-control"
                                           name="education"
                                           value="{{$user->education}}"
                                           placeholder="Education">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Marital Status</label>
                                    <select name="marital_status" id="marital_status" class="form-control">
                                        <option value="">Select Type</option>
                                        <option value="Married" @if($user->marital_status=="Married") selected @endif>
                                            Married
                                        </option>
                                        <option value="Unmarried"
                                                @if($user->marital_status=="Unmarried") selected @endif>Unmarried
                                        </option>
                                        <option value="Engaged" @if($user->marital_status=="Engaged") selected @endif>
                                            Engaged
                                        </option>
                                    </select>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Mobile No</label>
                                    <input type="text" class="form-control" name="mobile_no"
                                           value="{{$user->mobile_no}}"
                                           placeholder="Mobile No"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email"
                                           value="{{$user->email}}"
                                           placeholder="Email"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Blood Group</label>
                                    <input type="text" class="form-control"
                                           name="blood_group"
                                           value="{{$user->blood_group}}"
                                           placeholder="Blood Group">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Residency No</label>
                                    <input type="text" class="form-control" name="residency_no"
                                           value="{{$user->residency_no}}"
                                           placeholder="Residency No"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Office No</label>
                                    <input type="text" class="form-control" name="office_no"
                                           value="{{$user->office_no}}"
                                           placeholder="Office No"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Business</label>
                                    <textarea class="form-control" name="business"
                                              placeholder="Business"
                                    >{{$user->business}}</textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Address 1</label>
                                    <textarea class="form-control" name="address_1"
                                              placeholder="Address 1"
                                    >{{$user->address_1}}</textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Address 2</label>
                                    <textarea class="form-control" name="address_2"
                                              placeholder="Address 2"
                                    >{{$user->address_2}}</textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Address 3</label>
                                    <textarea class="form-control" name="address_3"
                                              placeholder="Address 3"
                                    >{{$user->address_3}}</textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city"
                                           value="{{$user->city}}"
                                           placeholder="City"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Pincode</label>
                                    <input type="text" class="form-control" name="pincode"
                                           value="{{$user->pincode}}"
                                           placeholder="pincode"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Special</label>
                                    <textarea class="form-control" name="special"
                                              value="{{$user->special}}"
                                              placeholder="Special"
                                    ></textarea>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Image</label>
                                    <input type="file" name="image" data-default-file="{{asset($user->image)}}"
                                           class="dropify"/>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password"
                                           placeholder="Password">
                                </div>
                            </div>

                            <button class="btn btn-primary" id="update-btn"
                                    type="submit">{{trans('messages.submit')}}
                            </button>

                            <a class="btn btn-dark"
                               href="{{route('admin.user.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/user.js') }}?v={{ time() }}"></script>
@endsection

