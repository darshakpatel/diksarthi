@extends('admin.layouts.vertical', ['title' => 'Members'])

@section('content')

    <style>
        td.details-control {
            background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
        }

    </style>
    <!-- Start Content-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <a href="{{route('admin.user.create')}}" class="btn btn-primary">Add Member</a>
                    </div>
                    <h4 class="page-title">Members</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-lg-3">
                            <select name="village_id" id="village_id" class="form-control select2">
                                <option value="">Select Village</option>
                                @foreach($villages as $village)
                                    <option value="{{$village->id}}">{{$village->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <select name="member_type" id="member_type" class="form-control select2">
                                <option value="">Select Type</option>
                                <option value="1">Registered Member</option>
                                <option value="2">Family Member</option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <button class="btn btn-primary" id="filter">Filter</button>
                            <button class="btn btn-success" id="export">Export</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="addrows">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th></th>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>Member Type</th>
                                            <th>Village</th>
                                            <th>Family ID</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.mobile_no')}}</th>
                                            <th>Birth Date</th>
                                            <th>Relation</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th></th>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>Member Type</th>
                                            <th>Village</th>
                                            <th>Family ID</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>{{trans('messages.mobile_no')}}</th>
                                            <th>Birth Date</th>
                                            <th>Relation</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const destroy_user = '{{trans('messages.admin.user.destroy_user')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
        const status_message = '{{trans('messages.status_message')}}'
        const active_user = '{{trans('messages.admin.user.active_user')}}'
        const inactive_user = '{{trans('messages.admin.user.inactive_user')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/user.js') }}?v={{ time() }}"></script>

@endsection
