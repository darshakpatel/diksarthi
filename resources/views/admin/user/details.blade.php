@extends('admin.layouts.vertical', ['title' => 'Member Details'])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Member Details</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-xl-6">
                <!-- project card -->
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Name</th>
                                        <td>{{$user->name}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Email</th>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Mobile No</th>
                                        <td>{{$user->mobile_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Relationship</th>
                                        <td>{{$user->relation?$user->relation->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Village</th>
                                        <td>{{$user->village?$user->village->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Member Image</th>
                                        <td><img src="{{asset($user->image)}}" style="max-width: 200px"/></td>
                                    </tr>
                                    
                                    <tr>
                                        <th scope="row">Family No</th>
                                        <td>{{$user->family_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Person No</th>
                                        <td>{{$user->per_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Petron or Life</th>
                                        <td>{{$user->petron}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Piyar Paksh</th>
                                        <td>{{$user->piyar_paksh}}</td>
                                    </tr>

                                    <tr>
                                        <th scope="row">Special</th>
                                        <td>{{$user->special}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Office No</th>
                                        <td>{{$user->office_no}}</td>
                                    </tr>

                                    <tr>
                                        <th scope="row">Business</th>
                                        <td>{{$user->business}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>
            <div class="col-xl-6">
                <!-- project card -->
                <div class="card d-block">
                    <div class="card-body">
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Anniversary Date</th>
                                        <td>{{$user->anniversary_date}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Date of Birth</th>
                                        <td>{{$user->date_of_birth}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Education</th>
                                        <td>{{$user->education}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Marital Status</th>
                                        <td>{{$user->marital_status}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Blood Group</th>
                                        <td>{{$user->blood_group}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Residency No</th>
                                        <td>{{$user->residency_no}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address 1</th>
                                        <td>{{$user->address_1}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address 2</th>
                                        <td>{{$user->address_2}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Address 3</th>
                                        <td>{{$user->address_3}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">City</th>
                                        <td>{{$user->city}}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Pincode</th>
                                        <td>{{$user->pincode}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div>
        </div><!-- end col -->
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/userDetails.js') }}?v={{ time() }}"></script>
@endsection
