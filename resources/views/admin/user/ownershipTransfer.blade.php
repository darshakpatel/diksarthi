@extends('admin.layouts.vertical', ['title' => 'Family Members'])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Ownership transfer of {{$mainMember->name}}</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="main_member_id" value="{{$mainMember->id}}">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group mb-3">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Family Member</th>
                                            <th>Transfer</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>
                                                    {{$user->name}}
                                                </td>
                                                <td>
                                                    <input type="radio" name="transfer_id" value="{{$user->id}}" required/>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        const destroy_user = '{{trans('messages.admin.user.destroy_user')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
        const status_message = '{{trans('messages.status_message')}}'
        const active_user = '{{trans('messages.admin.user.active_user')}}'
        const inactive_user = '{{trans('messages.admin.user.inactive_user')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/ownershipTransfer.js') }}?v={{ time() }}"></script>

@endsection
