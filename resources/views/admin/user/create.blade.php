@extends('admin.layouts.vertical', ['title' =>"Add Member"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Add Member</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group mb-3">
                                    <label>Member Type</label>
                                    <select name="member_type" class="form-control select2" id="member_type" required>
                                        <option value="">Select Type</option>
                                        <option value="1">Registered Member</option>
                                        <option value="2">Family Member</option>
                                    </select>
                                </div>
                                <div class="col-12 form-group mb-3 register_member" style="display: none">
                                    <label>Select Main Member</label>
                                    <select name="main_member_id" class="form-control select2" id="main_member_id"
                                            >
                                        <option value="">Select Member</option>
                                        @foreach($register_members as $register_member)
                                            <option value="{{$register_member->id}}" data-family-no="{{$register_member->family_no}}">{{$register_member->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" name="name"
                                           placeholder="Name" required>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Village</label>
                                    <select name="village_id" id="village_id" class="form-control select2">
                                        <option value="">Select Village</option>
                                        @foreach($villages as $village)
                                            <option value="{{$village->id}}">{{$village->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                               
                                <div class="col-6 form-group mb-3">
                                    <label>Family No</label>
                                    <input type="text" class="form-control" id="family_no" name="family_no"
                                           placeholder="Family No" required>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Person No</label>
                                    <input type="text" class="form-control" name="per_no"
                                           placeholder="Person No" required>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Relation</label>
                                    <select name="relation_id" id="relation_id" class="form-control select2">
                                        <option value="">Select Relation</option>
                                        @foreach($relations as $relation)
                                            <option value="{{$relation->id}}">{{$relation->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Petron or Life</label>
                                    <select name="petron" id="petron" class="form-control select2">
                                        <option value="">Select Type</option>
                                        <option value="P">Petron</option>
                                        <option value="L">Life</option>
                                    </select>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Piyar Paksh</label>
                                    <input type="text" class="form-control" name="piyar_paksh"
                                           placeholder="Piyar Paksh">
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Anniversary Date</label>
                                    <input type="text" id="anniversary_date" class="form-control"
                                           name="anniversary_date"
                                           placeholder="Anniversary Date">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Birth Date</label>
                                    <input type="text" id="date_of_birth" class="form-control"
                                           name="date_of_birth"
                                           placeholder="Birth Date">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Education</label>
                                    <input type="text" class="form-control"
                                           name="education"
                                           placeholder="Education">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Marital Status</label>
                                    <select name="marital_status" id="marital_status" class="form-control select2">
                                        <option value="">Select Type</option>
                                        <option value="Married">Married</option>
                                        <option value="Unmarried">Unmarried</option>
                                        <option value="Engaged">Engaged</option>
                                    </select>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Mobile No</label>
                                    <input type="text" class="form-control integer" name="mobile_no"
                                           placeholder="Mobile No"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email"
                                           placeholder="Email"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Blood Group</label>
                                    <input type="text" class="form-control"
                                           name="blood_group"
                                           placeholder="Blood Group">
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Residency No</label>
                                    <input type="text" class="form-control" name="residency_no"
                                           placeholder="Residency No"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Office No</label>
                                    <input type="text" class="form-control" name="office_no"
                                           placeholder="Office No"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Business</label>
                                    <textarea class="form-control" name="business"
                                              placeholder="Business"
                                    ></textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Address 1</label>
                                    <textarea class="form-control" name="address_1"
                                              placeholder="Address 1"
                                    ></textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Address 2</label>
                                    <textarea class="form-control" name="address_2"
                                              placeholder="Address 2"
                                    ></textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Address 3</label>
                                    <textarea class="form-control" name="address_3"
                                              placeholder="Address 3"
                                    ></textarea>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city"
                                           placeholder="City"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Pincode</label>
                                    <input type="text" class="form-control" name="pincode"
                                           placeholder="pincode"
                                    >
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Special</label>
                                    <textarea class="form-control" name="special"
                                              placeholder="Special"
                                    ></textarea>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Image</label>
                                    <input type="file" name="image" class="dropify"/>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password"
                                           placeholder="Password">
                                </div>
                            </div>

                            <button class="btn btn-primary" id="update-btn"
                                    type="submit">{{trans('messages.submit')}}
                            </button>

                            <a class="btn btn-dark"
                               href="{{route('admin.user.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/user.js') }}?v={{ time() }}"></script>
@endsection

