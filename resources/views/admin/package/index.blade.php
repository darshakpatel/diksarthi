@extends('admin.layouts.vertical', ['title' =>"Packages"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <a href="{{route('admin.package.create')}}" class="btn btn-primary">Add Package</a>
                    </div>
                    <h4 class="page-title">Packages</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>Price</th>
                                            <th>Days</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>{{trans('messages.name')}}</th>
                                            <th>Price</th>
                                            <th>Days</th>
                                            <th>{{trans('messages.status')}}</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
      const status_message = '{{trans('messages.status_message')}}'
      const delete_message = '{{trans('messages.delete_message')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/village.js') }}?v={{ time() }}"></script>
@endsection
