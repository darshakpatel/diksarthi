@extends('admin.layouts.vertical', ['title' =>"Edit Package"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Package</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$package->id}}">
                            @csrf

                            <div class="form-group mb-3">
                                <label>{{trans('messages.name')}}</label>
                                <input type="text" class="form-control"
                                       name="name" value="{{$package->name}}"
                                       placeholder="{{trans('messages.name')}}" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Price</label>
                                <input type="text" class="form-control integer"
                                       name="price" value="{{$package->price}}"
                                       placeholder="Price" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>No of days</label>
                                <input type="text" class="form-control"
                                       name="days" value="{{$package->days}}"
                                       placeholder="No of Days" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.package.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/village.js') }}?v={{ time() }}"></script>

@endsection

