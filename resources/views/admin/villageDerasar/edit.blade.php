@extends('admin.layouts.vertical', ['title' =>"Edit Village Derasar"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Village Derasar</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$villageDerasar->id}}">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Village</label>
                                <select id="village_id" name="village_id" class="form-control select2">
                                    <option value="">Select Village</option>
                                    @foreach($villages as $village)
                                        <option value="{{$village->id}}"
                                                @if($village->id==$villageDerasar->village_id) selected @endif>{{$village->name}}</option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Title</label>
                                <input type="text" class="form-control"
                                       name="title" value="{{$villageDerasar->title}}"
                                       placeholder="Title" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Serial No</label>
                                <input type="text" class="form-control"
                                       name="order_by" value="{{$villageDerasar->order_by}}"
                                       placeholder="Serial No">
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Image</label>
                                <input type="file" class="form-control dropify"
                                       name="image" data-default-file="{{asset($villageDerasar->image)}}"
                                >
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Description</label>
                                <textarea name="description" id="description"
                                          class="form-control">{{$villageDerasar->description}}</textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.villageDerasar.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/villageDerasar.js') }}?v={{ time() }}"></script>

@endsection

