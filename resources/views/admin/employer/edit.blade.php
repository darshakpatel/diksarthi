@extends('admin.layouts.vertical', ['title' => "Edit Employer"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Employee</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$employer->id}}">
                            @csrf
                            <div class="row">
                                <div class="col-6 form-group mb-3">
                                    <label>Social Insurance Number</label>
                                    <textarea type="text" class="form-control"
                                              name="social_insurance_number"
                                              data-value="{{$employer->social_insurance_number}}"
                                              placeholder="Social Insurance Number" required>
                                </textarea>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label class="col-form-label">Email Address</label>
                                    <input class="form-control"
                                           type="email" name="email"
                                           value="{{$employer->email}}"
                                           required="" placeholder="email"/>
                                    <div class="invalid-tooltip">Please enter proper email.</div>
                                </div>

                                <div class="col-6 form-group">
                                    <label class="col-form-label">Password</label>
                                    <input class="form-control" type="password"
                                           name="password" id="password"

                                           placeholder="*********"/>
                                    <div class="invalid-tooltip">Please enter password.</div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Date Of Birth</label>
                                    <input type="text" class="form-control integer date-picker"
                                           name="date_of_birth"
                                           value="{{$employer->date_of_birth}}"
                                           placeholder="Date Of Birth" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Date Of Hire</label>
                                    <input type="text" class="form-control"
                                           name="date_of_hire"
                                           value="{{$employer->date_of_hire}}"
                                           placeholder="Date Of Hire" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Employee Name</label>
                                    <input type="text" class="form-control"
                                           name="employee_name"
                                           value="{{$employer->employee_name}}"
                                           placeholder="Employee Name" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Hire Date</label>
                                    <input type="text" class="form-control"
                                           name="hire_date"
                                           value="{{$employer->hire_date}}"
                                           placeholder="Hire Date" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Start Date</label>
                                    <input type="text" class="form-control"
                                           name="start_date"
                                           value="{{$employer->start_date}}"
                                           placeholder="Start Date" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Employee Contact Form</label>
                                    <input type="text" class="form-control"
                                           name="employee_contact_form"
                                           value="{{$employer->employee_contact_form}}"
                                           placeholder="Employee Contact Form" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Employee Course Name</label>
                                    <input type="text" class="form-control"
                                           name="employee_course_name"
                                           value="{{$employer->employee_course_name}}"
                                           placeholder="Employee Course Name" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">

                                    <label>Employee Course Date</label>
                                    <input type="text" class="form-control"
                                           name="employee_course_date"
                                           value="{{$employer->employee_course_date}}"
                                           placeholder="Employee Course Date" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Employee Certificate Of Course(photo or document upload)</label>
                                    <input type="text" class="form-control"
                                           name="employee_certificate_of_course"
                                           value="{{$employer->employee_certificate_of_course}}"
                                           placeholder="Employee Certificate Of Course" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Diciplinary Action Note</label>
                                    <input type="text" class="form-control"
                                           name="diciplinary_action_note"
                                           value="{{$employer->diciplinary_action_note}}"
                                           placeholder="Diciplinary Action Note" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Diciplinary Form</label>
                                    <input type="text" class="form-control"
                                           name="diciplinary_form"
                                           value="{{$employer->diciplinary_form}}"
                                           placeholder="Diciplinary Form" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-6 form-group mb-3">
                                    <label>Employee Tool List(asset on hand)</label>
                                    <input type="text" class="form-control"
                                           name="asset_on_hand"
                                           value="{{$employer->asset_on_hand}}"
                                           placeholder="Employee Tool List" required>
                                    <div class="valid-feedback"></div>
                                </div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.employer.index')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/employer.js') }}?v={{ time() }}"></script>
@endsection

