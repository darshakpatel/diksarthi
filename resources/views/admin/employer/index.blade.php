@extends('admin.layouts.vertical', ['title' =>"Employer"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <a href="{{route('admin.employer.create')}}" class="btn btn-primary">Add Employer</a>
                    </div>
                    <h4 class="page-title">Employer</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Id</th>
                                            <th>Employee Name</th>
                                            <th>Employee Course Name</th>
                                            <th>Employee Course Date</th>
                                            <th>Asset On Hand</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>Id</th>
                                            <th>Employee Name</th>
                                            <th>Employee Course Name</th>
                                            <th>Employee Course Date</th>
                                            <th>Asset On Hand</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const status_message = '{{trans('messages.status_message')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/employer.js') }}?v={{ time() }}"></script>
@endsection
