<div class="row row-{{$row}}">
    <div class="col-10 form-group mb-3">
        <label>Video Link</label>
        <input type="text" class="form-control"
               name="video_link[]"
               placeholder="Video Link">
        <div class="valid-feedback"></div>
    </div>
    <div class="col-2 form-group mb-3">
        <button type="button" class="mt-3 btn btn-danger" onclick="removeVideoLink({{$row}})"><i
                    class="fa fa-minus"></i>
        </button>
    </div>
</div>
