@extends('admin.layouts.vertical', ['title' => "Add Gallery"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Add Gallery</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            <input type="hidden" name="temp_time" id="temp_time" value="{{time()}}">
                            @csrf
                            <div class="row">

                                <div class="col-12 form-group mb-3">
                                    <label>Title</label>
                                    <input type="text" class="form-control"
                                           name="title"
                                           placeholder="Title" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Village</label>
                                    <select name="village_id" id="village_id" class="form-control select2">
                                        <option value="">Select Village</option>
                                        @foreach($villages as $village)
                                            <option value="{{$village->id}}">{{$village->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Image</label>
                                    <div id="fine-uploader"></div>
                                </div>
                                <div class="col-10 form-group mb-3">
                                    <label>Video Link</label>
                                    <input type="text" class="form-control"
                                           name="video_link[]"
                                           placeholder="Video Link">
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-2 form-group mb-3">
                                    <button type="button" class="mt-3 btn btn-primary" onclick="addVideoLink()"><i
                                                class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="gallery" style="width: 100%"></div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.gallery.index')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
    @include('admin.layouts.fineupload')
@endsection

@section('script')
    <script>
        var JS_URL = '{{url('/')}}';
        var rowNo = 1;
    </script>
    <script src="{{ asset('admin/assets/js/custom/imageUploader.js') }}?v={{ time() }}"></script>
    <script src="{{ asset('admin/assets/js/custom/gallery.js') }}?v={{ time() }}"></script>
@endsection

