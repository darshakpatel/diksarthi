<div class="row">
    @foreach($images as $image)
        <div class="col-xl-3 col-lg-3">
            <div class="img-thumbnail mb-3 image-container">

                <img src="{{url($image->image)}}" alt="thumb1" style="max-width:200px;" class="thumbimg wd-100p">
                <div class="btn btn-list pl-0 pb-0">
                    <a href="javascript:void(0)" data-id="{{$image->id}}" data-gallery-id="{{$image->gallery_id}}"
                       class="delete-gallery-image btn btn-danger btn-icon" data-toggle="tooltip" data-placement="top"><i
                                class="fa fa-trash font-size-16 align-middle"></i></a>
                </div>
            </div>
        </div>
    @endforeach

</div>
