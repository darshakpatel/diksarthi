<div class="row rows-{{$row}}" style="width: 100%">

    <div class="col-5 mt-3">
        <div class="form-group">
            <label for="option_1"> Title
                <span class="error">*</span></label>
            <input type="text" class="form-control"
                   name="title[]"
                   id="option_1_{{$row}}"
                   placeholder=" Title"
                   required/>
            <div class="help-block with-errors error"></div>
        </div>
    </div>
    <div class="col-5 mt-3">
        <div class="form-group">
            <label for="option_2">Value
                <span class="error">*</span></label>
            <input type="text" class="form-control"
                   name="value[]"
                   id="option_2_{{$row}}"
                   placeholder=" Value"
                   required/>
            <div class="help-block with-errors error"></div>
        </div>
    </div>

    <div class="form-group col-2">
        <button type="button" onclick="removeKey('{{$row}}')" style="margin-top: 60px;!important;"
                class="btn btn-danger"><i class="fa fa-minus"></i>
        </button>
    </div>

</div>