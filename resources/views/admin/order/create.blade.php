@extends('admin.layouts.vertical', ['title' => "Add Order"])
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Add Order</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="row">
                                <div class="col-6 form-group mb-3">
                                    <label>Service Title</label>
                                    <input type="text" class="form-control"
                                           name="service_title"
                                           placeholder="Service Title" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Service Schedule</label>
                                    <input type="text" class="form-control"
                                           name="service_schedule"
                                           placeholder="Service Schedule" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Manage Of Work Order</label>
                                    <input type="text" class="form-control"
                                           name="manage_of_work_order"
                                           placeholder="Manage Of Work Order" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Assigned Provider</label>
                                    <input type="text" class="form-control"
                                           name="assigned_provider"
                                           placeholder="Assigned Provider" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Service Location</label>
                                    <input type="text" class="form-control"
                                           name="service_location"
                                           placeholder="Service Location" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Traning Link</label>
                                    <input type="text" class="form-control"
                                           name="traning_link"
                                           placeholder="Traning Link" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Help Desk Contact</label>
                                    <input type="text" class="form-control"
                                           name="help_desk_contact"
                                           placeholder="Help Desk Contact" required>
                                    <div class="valid-feedback"></div>
                                </div>

                                <div class="col-6 form-group mb-3">
                                    <label>Manager On Duty</label>
                                    <input type="text" class="form-control"
                                           name="manager_on_duty"
                                           placeholder="Manager On Duty" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Orientation Keyword</label>
                                    <input type="text" class="form-control"
                                           name="orientation_keyword"
                                           placeholder="Orientation Keyword" required>
                                    <div class="valid-feedback"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-5 mt-3">
                                    <div class="form-group">
                                        <label for="option_1">Title
                                            <span class="error">*</span></label>
                                        <input type="text" class="form-control"
                                               name="title[]"
                                               placeholder="Title"
                                               required/>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                </div>
                                <div class="col-5 mt-3">
                                    <div class="form-group">
                                        <label for="option_2"> Value
                                            <span class="error">*</span></label>
                                        <input type="text" class="form-control"
                                               name="value[]"
                                               placeholder="Value"
                                               required/>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                </div>
                                <div class="form-group col-2">
                                    <button type="button" onclick="addkey()" style="margin-top: 50px;!important;"
                                            class="btn btn-success"><i class="fa fa-plus"></i>
                                    </button>
                                </div>

                            </div>
                            <div id="service_extra"></div>

                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.order.index')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/order.js') }}?v={{ time() }}"></script>
    <script>

      var rowNo = 1

      function addkey () {
        loaderView()
        axios
          .post(APP_URL + '/addCustomField', { rowNo: rowNo })
          .then(function (response) {
            $('#service_extra').append(response.data.data)
          })
        rowNo = rowNo + 1
        loaderHide()
      }

      function removeKey (row) {
        $('.rows-' + row).remove()
      }
    </script>
@endsection

