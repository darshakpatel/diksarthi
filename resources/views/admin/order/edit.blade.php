@extends('admin.layouts.vertical', ['title' => "Edit Order"])
@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Order</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$order->id}}">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Service Title</label>
                                <input type="text" class="form-control"
                                       name="service_title"
                                       value="{{$order->service_title}}"
                                       placeholder="Service Title" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Service Schedule</label>
                                <input type="text" class="form-control"
                                       name="service_schedule"
                                       value="{{$order->service_schedule}}"
                                       placeholder="Service Schedule" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Manage Of Work Order</label>
                                <input type="text" class="form-control"
                                       name="manage_of_work_order"
                                       value="{{$order->manage_of_work_order}}"
                                       placeholder="Manage Of Work Order" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Assigned Provider</label>
                                <input type="text" class="form-control"
                                       name="assigned_provider"
                                       value="{{$order->assigned_provider}}"
                                       placeholder="Assigned Provider" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Service Location</label>
                                <input type="text" class="form-control"
                                       name="service_location"
                                       value="{{$order->service_location}}"
                                       placeholder="Service Location" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Traning Link</label>
                                <input type="text" class="form-control"
                                       name="traning_link"
                                       value="{{$order->traning_link}}"
                                       placeholder="Traning Link" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Help Desk Contact</label>
                                <input type="text" class="form-control"
                                       name="help_desk_contact"
                                       value="{{$order->help_desk_contact}}"
                                       placeholder="Help Desk Contact" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>Manager On Duty</label>
                                <input type="text" class="form-control"
                                       name="manager_on_duty"
                                       value="{{$order->manager_on_duty}}"
                                       placeholder="Manager On Duty" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Orientation Keyword</label>
                                <input type="text" class="form-control"
                                       name="orientation_keyword"
                                       value="{{$order->orientation_keyword}}"
                                       placeholder="Orientation Keyword" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.order.index')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/order.js') }}?v={{ time() }}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function () {
            $('.date').datepicker();
        });
    </script>

@endsection

