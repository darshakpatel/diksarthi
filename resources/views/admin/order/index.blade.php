@extends('admin.layouts.vertical', ['title' =>"Oder"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <a href="{{route('admin.order.create')}}" class="btn btn-primary">Add Order</a>
                    </div>
                    <h4 class="page-title">Order</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Id</th>
                                            <th>Order Id</th>
                                            <th>Service Title</th>
{{--                                            <th>Status</th>--}}
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>Id</th>
                                            <th>Order Id</th>
                                            <th>Service Title</th>
{{--                                            <th>Status</th>--}}
                                            <th>Action</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const status_message = '{{trans('messages.status_message')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/order.js') }}?v={{ time() }}"></script>
@endsection
