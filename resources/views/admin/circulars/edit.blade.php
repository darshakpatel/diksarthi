@extends('admin.layouts.vertical', ['title' =>"Edit Circular"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Circular</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$circular->id}}">
                            @csrf

                            <div class="form-group mb-3">
                                <label>Date</label>
                                <input type="text" class="form-control"
                                       name="date" value="{{$circular->date}}"
                                       placeholder="Date" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Title</label>
                                <input type="text" class="form-control"
                                       name="title" value="{{$circular->title}}"
                                       placeholder="Title" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Image</label>
                                <input type="file" class="form-control dropify"
                                       name="image" data-default-file="{{asset($circular->image)}}"
                                >
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Description</label>
                                <textarea name="description" id="description" class="form-control">{{$circular->description}}</textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Video Link</label>
                                <input type="text" class="form-control"
                                       name="video_link" value="{{$circular->video_link}}"
                                       placeholder="Video Link">
                                <div class="valid-feedback"></div>
                            </div>

                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.circular.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/circular.js') }}?v={{ time() }}"></script>

@endsection

