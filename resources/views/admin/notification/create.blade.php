@extends('admin.layouts.vertical', ['title' =>trans('messages.admin.notification.send_notification')])

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">{{trans('messages.admin.notification.send_notification')}}</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" id="form-method" value="edit_value">
                            @csrf
                            <div class="form-group mb-3">
                                <label for="title">Template</label>
                                <select name="template_id" class="form-control select2" required>
                                    <option value="">Select Template</option>
                                    @foreach($templates as $template)
                                        <option value="{{$template->id}}">{{$template->title}}</option>
                                    @endforeach
                                </select>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="title">{{trans('messages.admin.notification.title')}}</label>
                                <input type="text" class="form-control"
                                       name="title" id="title"
                                       placeholder="Title" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="message">{{trans('messages.admin.notification.message')}}</label>
                                <textarea class="form-control"
                                          name="message" id="message"
                                          placeholder="{{trans('messages.admin.notification.message')}}"
                                          required></textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.save')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.dashboard')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/notification.js') }}?v={{ time() }}"></script>
@endsection
