<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">
    <div class="h-100" data-simplebar>
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul id="side-menu">
                <li class="menu-title">Navigation</li>
                <li>
                    <a href="{{route('admin.dashboard')}}">
                        <i data-feather="airplay"></i>
                        <span>{{ trans('messages.admin.sidebar.dashboard') }}</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.user.index')}}">
                        <i data-feather="user"></i>
                        <span>Members</span>
                    </a>
                </li>
                <li>
                    <a href="#village" data-toggle="collapse">
                        <i data-feather="globe"></i>
                        <span>Villages</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="village">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.village.create')}}">Add Village</a>
                            </li>
                            <li>
                                <a href="{{route('admin.village.index')}}">Villages</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#relation" data-toggle="collapse">
                        <i class="fa fa-code-branch"></i>
                        <span>Relations</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="relation">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.relation.create')}}">Add Relation</a>
                            </li>
                            <li>
                                <a href="{{route('admin.relation.index')}}">Relations</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#diksarthi" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Diksarthi</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="diksarthi">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.diksarthi.create')}}">Add Diksarthi</a>
                            </li>
                            <li>
                                <a href="{{route('admin.diksarthi.index')}}">Diksarthi</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#circular" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Circulars</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="circular">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.circular.create')}}">Add Circular</a>
                            </li>
                            <li>
                                <a href="{{route('admin.circular.index')}}">Circulars</a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li>
                    <a href="#yojna" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Yojnas</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="yojna">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.yojna.create')}}">Add Yojna</a>
                            </li>
                            <li>
                                <a href="{{route('admin.yojna.index')}}">Yojnas</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#gallery" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Galleries</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="gallery">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.gallery.create')}}">Add Gallery</a>
                            </li>
                            <li>
                                <a href="{{route('admin.gallery.index')}}">Galleries</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#tapasviSamman" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Tapasvi Sanman</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="tapasviSamman">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.tapasviSamman.create')}}">Add Tapasvi Sanman</a>
                            </li>
                            <li>
                                <a href="{{route('admin.tapasviSamman.index')}}">Tapasvi Sanman</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#parshwanath" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>108 Parshwanath</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="parshwanath">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.parshwanath.create')}}">Add 108 Parshwanath</a>
                            </li>
                            <li>
                                <a href="{{route('admin.parshwanath.index')}}">108 Parshwanath</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#villageDerasar" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Village Derasar</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="villageDerasar">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.villageDerasar.create')}}">Add Village Derasar</a>
                            </li>
                            <li>
                                <a href="{{route('admin.villageDerasar.index')}}">Village Derasar</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#ourAncestor" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Our Ancestor</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="ourAncestor">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.ourAncestor.create')}}">Add Our Ancestor</a>
                            </li>
                            <li>
                                <a href="{{route('admin.ourAncestor.index')}}">Our Ancestor</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="{{route('admin.ourGyanti.index')}}">
                        <i class="fa fa-file"></i>
                        <span>Our Ghnyati</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.president.index')}}">
                        <i class="fa fa-file"></i>
                        <span>President</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.ourCommitte.index')}}">
                        <i class="fa fa-file"></i>
                        <span>Our Committe</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.karyakar.index')}}">
                        <i class="fa fa-file"></i>
                        <span>Karyakar</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.feedback.index')}}">
                        <i class="fa fa-file"></i>
                        <span>Feedbacks</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin.page.edit',[5])}}">
                        <i class="fa fa-file"></i>
                        <span>Contact Us</span>
                    </a>
                </li>
                <li>
                    <a href="#banner" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Banners</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="banner">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.banner.create')}}">Add Banner</a>
                            </li>
                            <li>
                                <a href="{{route('admin.banner.index')}}">Banners</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#sms_setting" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Templates Settings</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sms_setting">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.template.index')}}">Templates</a>
                            </li>
                            <li>
                                <a href="{{route('admin.setting')}}">Settings</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#communication" data-toggle="collapse">
                        <i class="fa fa-file"></i>
                        <span>Communication</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="communication">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{route('admin.sendEmail')}}">Send Email</a>
                            </li>
                            <li>
                                <a href="{{route('admin.todayBirthday')}}">Today Birthday</a>
                            </li>
                            <li>
                                <a href="{{route('admin.todayAnniversary')}}">Today Anniversary</a>
                            </li>
                            <li>
                                <a href="{{route('admin.sendNotification')}}">Send Notification</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
