<meta charset="utf-8" />
<title>{{$title ?? env('APP_NAME')}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
