@yield('css')

<!-- App css -->
<link href="{{asset('admin/assets/libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"
      id="bs-default-stylesheet"/>
<link href="{{asset('admin/assets/css/app.min.css')}} " rel="stylesheet" type="text/css" id="app-default-stylesheet"/>
<link href="{{asset('admin/assets/css/bootstrap-dark.min.css')}} " rel="stylesheet" type="text/css"
      id="bs-dark-stylesheet"
      disabled/>
<link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/css/app-dark.min.css')}} " rel="stylesheet" type="text/css" id="app-dark-stylesheet"
      disabled/>
<link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/css/number-spin.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/libs/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('admin/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}"
      rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('admin/assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css')}}" rel="stylesheet"
      type="text/css"/>
<link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/libs/summernote/summernote-bs4.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/libs/flatpickr/flatpickr.min.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/assets/datepicker/css/datepicker.css')}}" rel="stylesheet" type="text/css"/>
<meta name="csrf-token" content="{{ csrf_token() }}">

