<!-- Right Sidebar -->
<div class="right-bar">
  <div data-simplebar class="h-100">



      <!-- Tab panes -->
      <div class="tab-content pt-0">
          <div class="tab-pane" id="chat-tab" role="tabpanel">

                  <!-- Topbar -->
                  <h6 class="font-weight-medium font-14 mt-4 mb-2 pb-1">Topbar</h6>

                  <div class="custom-control custom-switch mb-1">
                      <input type="radio" class="custom-control-input" name="topbar-color" value="dark" id="darktopbar-check"
                          checked />
                      <label class="custom-control-label" for="darktopbar-check">Dark</label>
                  </div>

                  <div class="custom-control custom-switch mb-1">
                      <input type="radio" class="custom-control-input" name="topbar-color" value="light" id="lighttopbar-check" />
                      <label class="custom-control-label" for="lighttopbar-check">Light</label>
                  </div>


                  <button class="btn btn-primary btn-block mt-4" id="resetBtn">Reset to Default</button>

              </div>

          </div>
      </div>

  </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
