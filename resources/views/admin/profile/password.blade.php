@extends('admin.layouts.vertical', ['title' =>'Change Password'])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Change Password</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="id" value="">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Password</label>
                                <input type="text" class="form-control" name="password"
                                       placeholder="New Password" required>
                                <div class="valid-feedback">

                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Confirm Password</label>
                                <input type="text" class="form-control" name="confirm_password"
                                       placeholder="Confirm Password" required>
                                <div class="valid-feedback">

                                </div>
                            </div>
                            <button class="btn btn-primary" id="update-btn"
                                    type="submit">{{trans('messages.submit')}}
                            </button>

                            <a class="btn btn-dark"
                               href="{{route('admin.dashboard')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/password.js') }}?v={{ time() }}"></script>

@endsection

