@extends('admin.layouts.vertical', ['title' =>"Edit Tapasvi Sanman"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Tapasvi Sanman</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$tapasviSamman->id}}">
                            @csrf

                            <div class="form-group mb-3">
                                <label>Title</label>
                                <input type="text" class="form-control"
                                       name="title" value="{{$tapasviSamman->title}}"
                                       placeholder="Title" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Serial No</label>
                                <input type="text" class="form-control"
                                       name="order_by" value="{{$tapasviSamman->order_by}}"
                                       placeholder="Serial No" >
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Image</label>
                                <input type="file" class="form-control dropify"
                                       name="image" data-default-file="{{asset($tapasviSamman->image)}}"
                                >
                                <div class="valid-feedback"></div>
                            </div>

                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.tapasviSamman.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/tapasviSamman.js') }}?v={{ time() }}"></script>

@endsection

