@extends('admin.layouts.vertical', ['title' =>"Update Template"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Update Template</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate enctype="multipart/form-data">
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$template->id}}">
                            @csrf
                            <div class="row">
                                <div class="col-12 form-group">
                                    <label for="title">Title<span
                                                class="error">*</span></label>
                                    <input type="text" class="form-control" name="title"
                                           value="{{$template->title}}"
                                           id="title"
                                           placeholder="Name" required/>
                                    <div class="help-block with-errors error"></div>
                                </div>
                                @if($template->type=='email')
                                    <div class="col-12 form-group">
                                        <label for="subject">Subject<span
                                                    class="error">*</span></label>
                                        <input type="text" class="form-control"
                                               name="subject" id="subject" value="{{$template->subject}}"
                                               placeholder="Subject" required/>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                @endif
                                @if($template->type=='sms')
                                    <div class="col-12 form-group">
                                        <label for="api_key">Api Key<span
                                                    class="error">*</span></label>
                                        <input type="text" class="form-control"
                                               name="api_key" id="api_key" value="{{$template->api_key}}"
                                               placeholder="Subject" required/>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                @endif
                                @if(count($tags)>0)
                                    <div class="col-12 form-group">
                                        <label>Tags:

                                            @foreach($tags as $tag)
                                                {{$tag}}
                                            @endforeach

                                        </label>
                                    </div>
                                @endif
                                @if($template->type!='email')
                                    <div class="col-12 form-group">
                                        <label for="title">Description<span
                                                    class="error">*</span></label>
                                        <input type="text" class="form-control"
                                               name="description" value="{{$template->description}}"
                                               id="description"
                                               placeholder="Description" required/>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                @else
                                    <div class="col-12 form-group">
                                        <label for="title">Description<span
                                                    class="error">*</span></label>
                                        <textarea class="form-control description" name="description" id="description"
                                                  placeholder="Description"
                                                  required>{{$template->description}}</textarea>
                                        <div class="help-block with-errors error"></div>
                                    </div>
                                @endif
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}</button>
                            <a class="btn btn-dark"
                               href="{{route('admin.template.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection
@section('script')
    <script src="{{ asset('admin/assets/js/custom/template.js') }}?v={{ time() }}"></script>
@endsection

