@extends('admin.layouts.vertical', ['title' =>"108 Parshwanath"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <a href="{{route('admin.parshwanath.create')}}" class="btn btn-primary">Add 108 Parshwanath</a>
                    </div>
                    <h4 class="page-title">108 Parshwanath</h4>
                </div>
            </div>
        </div>
        <!-- Add rows table -->
        <section id="add-row">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table add-rows" id="data-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tfoot class="thead-dark">
                                        <tr>
                                            <th>{{trans('messages.id')}}</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>{{trans('messages.action')}}</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
    <script>
        const status_message = '{{trans('messages.status_message')}}'
        const delete_message = '{{trans('messages.delete_message')}}'
    </script>
    <script src="{{ asset('admin/assets/js/custom/parshwanath.js') }}?v={{ time() }}"></script>
@endsection
