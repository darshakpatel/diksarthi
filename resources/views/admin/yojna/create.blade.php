@extends('admin.layouts.vertical', ['title' => "Add Yojna"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Add Yojna</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="add">
                            <input type="hidden" name="edit_value" value="0">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Name</label>
                                <input type="text" class="form-control"
                                       name="name"
                                       placeholder="Name" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Start Date</label>
                                <input type="text" class="form-control today_datepicker"
                                       name="start_date"
                                       placeholder="Date" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>End Date</label>
                                <input type="text" class="form-control today_datepicker"
                                       name="end_date"
                                       placeholder="Title" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Image</label>
                                <input type="file" class="form-control dropify"
                                       name="image"
                                >
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Description</label>
                                <textarea name="description" id="description" class="form-control"></textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Benefit</label>
                                <textarea name="benefit" id="benefit" class="form-control"></textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Video Link</label>
                                <input type="text" class="form-control"
                                       name="video_link"
                                       placeholder="Video Link">
                                <div class="valid-feedback"></div>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.yojna.index')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/yojna.js') }}?v={{ time() }}"></script>
@endsection

