@extends('admin.layouts.vertical', ['title' =>"Edit Yojna"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Yojna</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$yojna->id}}">
                            @csrf

                            <div class="form-group mb-3">
                                <label>Start Date</label>
                                <input type="text" class="form-control"
                                       name="start_date" value="{{$yojna->start_date}}"
                                       placeholder="Start Date" required>
                                <div class="valid-feedback"></div>
                            </div>

                            <div class="form-group mb-3">
                                <label>End Date</label>
                                <input type="text" class="form-control"
                                       name="end_date" value="{{$yojna->end_date}}"
                                       placeholder="End Date" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Name</label>
                                <input type="text" class="form-control"
                                       name="name" value="{{$yojna->name}}"
                                       placeholder="Name" required>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Image</label>
                                <input type="file" class="form-control dropify"
                                       name="image" data-default-file="{{asset($yojna->image)}}"
                                >
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Description</label>
                                <textarea name="description" id="description"
                                          class="form-control">{{$yojna->description}}</textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <label>Benefit</label>
                                <textarea name="benefit" id="benefit"
                                          class="form-control">{{$yojna->benefit}}</textarea>
                                <div class="valid-feedback"></div>
                            </div>
                            <div class="form-group mb-3">
                                <label>Video Link</label>
                                <input type="text" class="form-control"
                                       name="video_link" value="{{$yojna->video_link}}"
                                       placeholder="Video Link">
                                <div class="valid-feedback"></div>
                            </div>

                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.yojna.index')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/yojna.js') }}?v={{ time() }}"></script>

@endsection

