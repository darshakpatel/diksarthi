@extends('admin.layouts.vertical', ['title' =>"Diksarthi List"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Diksarthi List</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            @csrf
                            <div class="form-group mb-3">
                                <label>Image</label>
                                <input type="file" class="dropify"
                                       name="photo">
                            </div>
                            <div class="form-group mb-3">
                                <label>PDF File</label>
                                <input type="file" class="dropify"
                                       name="image"
                                >
                            </div>
                            <div class="form-group ">
                                <a href="{{asset($diksarthi->image)}}" target="_blank" class="btn btn-info">Download
                                    PDF</a>
                            </div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.dashboard')}}">{{trans('messages.cancel')}}</a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/diksarthi.js') }}?v={{ time() }}"></script>

@endsection

