@extends('admin.layouts.vertical', ['title' => "Edit Diksarthi"])

@section('content')
    <!-- Start Content-->
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">

                    <h4 class="page-title">Edit Diksarthi</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                        <form role="form" data-toggle="validator" id="addEditForm" autocomplete="off"
                              method="post" class="needs-validation" novalidate>
                            <input type="hidden" id="form-method" value="edit">
                            <input type="hidden" name="edit_value" value="{{$diksarthi->id}}">
                            @csrf
                            <div class="row">

                                <div class="col-12 form-group mb-3">
                                    <label>Title</label>
                                    <input type="text" class="form-control"
                                           name="title" value="{{$diksarthi->title}}"
                                           placeholder="Title" required>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Village</label>
                                    <select name="village_id" id="village_id" class="form-control select2">
                                        <option value="">Select Village</option>
                                        @foreach($villages as $village)
                                            <option value="{{$village->id}}"
                                                    @if($diksarthi->village_id==$village->id) selected @endif >{{$village->name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Description</label>
                                    <textarea name="description" id="description" class="form-control">{!! $diksarthi->description !!}</textarea>
                                    <div class="valid-feedback"></div>
                                </div>
                                <div class="col-12 form-group mb-3">
                                    <label>Image</label>
                                    <input type="file" class="dropify"
                                           data-default-file="{{asset($diksarthi->image)}}"
                                           name="image">
                                </div>
                            </div>
                            <div id="gallery" style="width: 100%"></div>
                            <button class="btn btn-primary"
                                    type="submit">{{trans('messages.submit')}}
                            </button>
                            <a class="btn btn-dark"
                               href="{{route('admin.diksarthi.index')}}">{{trans('messages.cancel')}}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- container -->
@endsection

@section('script')
    <script src="{{ asset('admin/assets/js/custom/diksarthi.js') }}?v={{ time() }}"></script>
@endsection

